#if defined _BOSSFIGHTFORTRESS_BASE_INCLUDED
  #endinput
#endif
#define _BOSSFIGHTFORTRESS_BASE_INCLUDED

#include <definitions>
#include <gamma>
#include <tf2>
#include <tf2_stocks>

#define BFF_CONFIG_DIRECTORY "configs/boss-fight-fortress"

#define BFF_GAMEMODE_BASE "BossFightFortress-Base"

#define BFF_BOSS_BASE "BFFBossBase"
#define BFF_BOSS_GENERIC "BFFBossGeneric"

#define BFF_NORMAL_CHARGE_ABILITY "BFFNormalChargeAbility"
#define BFF_CONTINUOUS_CHARGE_ABILITY "BFFContinuousChargeAbility"
#define BFF_SPECIAL_ABILITY "BFFSpecialAbility"
#define BFF_PHASE_ABILITY "BFFPhaseAbility"

// State of an ability
enum AbilityState
{
	AbilityState_None,
	AbilityState_Ready,
	AbilityState_Charging,
	AbilityState_OnCooldown,
}

// Mode of the charge! Rawr
// Normal charges up from 0% to 100% when activating, while Continuous uses up charge from 100% to 0% when activating
enum ChargeMode
{
	ChargeMode_Invalid,    // Primarily for internal use, please don't use.
	ChargeMode_Normal,     // Normal charge mode, once used starts recharging from 0%, can only start charging once 100% recharged
	ChargeMode_Continuous, // Continuous charge mode, goes from 100% to 0%, it's "always" ready and it's depleted at 0%, when you let go it'll recharge from the percentage you left it at
}

// How the different special cooldowns are taken into account
enum SpecialChargeMode
{
	SpecialChargeMode_Assisting, // Assisting means all helps each other towards the goal
	SpecialChargeMode_Competing, // Competing means the highest cooldown rules them all!
}

// How values in PhaseAt are interpreted 
enum PhaseMode
{
	PhaseMode_FixedHealth,      // Values in PhasesAt represents exact health figures for a phase
	PhaseMode_PercentageHealth, // Values in PhasesAt represents percentage of max health for a phase
	PhaseMode_DamageTaken,      // Values in PhasesAt represents how much damage should be taken to enter next phase
}

// How reliable phases are
enum PhaseReliability
{
	PhaseReliability_AllowSkip, // Allows skipping a phase if damaged enough
	PhaseReliability_NeverSkip, // Does not allow skipping a phase, damage taken is capped to start of a new phase
}

// The considered round states of Boss Fight Fortress
enum BffRoundState
{
	BffRoundState_Invalid,
	BffRoundState_Preround,
	BffRoundState_Active,
	BffRoundState_Finished
}

// Ability slot of the charge abilities
enum AbilitySlot
{
	AbilitySlot_Primary,  // Is used with Attack2
	AbilitySlot_Secondary // Is used with Duck
}

// TF2 Weapon qualities, shamelessly ripped from VSH
enum TFQuality
{
	TFQuality_None = -1,       // Probably should never actually set an item's quality to this
	TFQuality_Normal = 0,
	TFQuality_NoInspect = 0,   // Players cannot see your attributes - NO LONGER WORKS due to Gunmettle update.
	TFQuality_Rarity1,
	TFQuality_Genuine = 1,
	TFQuality_Rarity2,
	TFQuality_Level = 2,       //  Same color as "Level # Weapon" text in description
	TFQuality_Vintage,
	TFQuality_Rarity3,         //  Is actually 4 - sort of brownish
	TFQuality_Rarity4,
	TFQuality_Unusual = 5,
	TFQuality_Unique,
	TFQuality_Community,
	TFQuality_Developer,
	TFQuality_Selfmade,
	TFQuality_Customized,
	TFQuality_Strange,
	TFQuality_Completed,
	TFQuality_Haunted,         //  13
	TFQuality_Collectors
}

#include <bossfightfortress-base/bffclient>
#include <bossfightfortress-base/boss-creator>
#include <bossfightfortress-base/game-mode-creator>
#include <bossfightfortress-base/charge-ability-creator>
#include <bossfightfortress-base/special-ability-creator>
#include <bossfightfortress-base/phase-ability-creator>
#include <bossfightfortress-stocks>

/**
 * Gets the current round state in a BFF game mode
 */
native BffRoundState BFF_GetRoundState();

/**
 * Sets the round active for the current BFF game mode
 *
 * @error If not called from the current game mode plugin
 */
native void BFF_SetRoundActive();

/**
 * Sets the round finished for the current BFF game mode
 *
 * @error If not called from the current game mode plugin
 */
native void BFF_SetRoundFinished();


/**
 * Adds a boss definitions model/sound/material files to downloads table
 */
native void BFF_AddBossFilesToDownloadsTable(Definition boss);

/**
 * Adds the model/sound/material files of the childrens to the boss definition to downloads table
 */
native void BFF_AddBossChildrensFilesToDownloadsTable(Definition ancestor);

/**
 * This is called when the round state changes in BFF
 */
forward void BFF_OnRoundStateChange(BffRoundState newState);

/* DO NOT EDIT BELOW THIS LINE */
public SharedPlugin __pl_bossfightfortress = 
{
	name = "bossfightfortress",
	file = "bossfightfortress-base.smx",
#if defined REQUIRE_PLUGIN
	required = 1,
#else
	required = 0,
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_bossfightfortress_SetNTVOptional()
{
	MarkNativeAsOptional("BFF_GetRoundState");
	MarkNativeAsOptional("BFF_SetRoundActive");
	MarkNativeAsOptional("BFF_SetRoundFinished");

	MarkNativeAsOptional("BFF_AddBossFilesToDownloadsTable");
	MarkNativeAsOptional("BFF_AddBossChildrensFilesToDownloadsTable");

	MarkNativeAsOptional("BffClient.IsBoss.get");
	MarkNativeAsOptional("BffClient.Boss.get");
	MarkNativeAsOptional("BffClient.Boss.set");
	MarkNativeAsOptional("BffClient.Phase.get");
	MarkNativeAsOptional("BffClient.ClearBoss");
	MarkNativeAsOptional("BffClient.BossClass.get");
	MarkNativeAsOptional("BffClient.IsEndangered.get");
	MarkNativeAsOptional("BffClient.PrimaryChargePercent.get");
	MarkNativeAsOptional("BffClient.SecondaryChargePercent.get");
	MarkNativeAsOptional("BffClient.SpecialChargePercent.get");
	MarkNativeAsOptional("BffClient.CustomSpecialChargePercent.get");
	MarkNativeAsOptional("BffClient.CustomSpecialChargePercent.set");

	MarkNativeAsOptional("BffClient.SetHealthMultiplier");
	MarkNativeAsOptional("BffClient.UpdateHud");
	MarkNativeAsOptional("BffClient.SetPrimaryChargeTime");
	MarkNativeAsOptional("BffClient.SetPrimaryCooldownTime");
	MarkNativeAsOptional("BffClient.SetPrimaryCooldownPercentage");
	MarkNativeAsOptional("BffClient.SetSecondaryChargeTime");
	MarkNativeAsOptional("BffClient.SetSecondaryCooldownTime");
	MarkNativeAsOptional("BffClient.SetSecondaryCooldownPercentage");
	MarkNativeAsOptional("BffClient.SetSpecialCooldown");
	MarkNativeAsOptional("BffClient.RemoveAllWearables");
	MarkNativeAsOptional("BffClient.PlaySound");
}
#endif