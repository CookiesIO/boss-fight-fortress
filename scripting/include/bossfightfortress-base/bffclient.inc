#if defined _BOSSFIGHTFORTRESS_BFF_CLIENT_INCLUDED
  #endinput
#endif
#define _BOSSFIGHTFORTRESS_BFF_CLIENT_INCLUDED

methodmap BffClient
{
	public BffClient(int client, bool userid=false)
	{
		if (userid)
		{
			client = GetClientOfUserId(client);
		}
		return view_as<BffClient>(client);
	}

	/******************************************
	 *
	 *  Wrapper/helper properties
	 *
	 ******************************************/

	property int Index
	{
		public get()
		{
			return view_as<int>(this);
		}
	}

	property int UserId
	{
		public get()
		{
			return GetClientUserId(this.Index);
		}
	}

	property bool IsValid
	{
		public get()
		{
			int client = this.Index;
			return (client > 0 && client <= MaxClients && IsClientInGame(client));
		}
	}

	property bool IsFakeClient
	{
		public get()
		{
			return IsFakeClient(this.Index);
		}
	}

	property TFClassType Class
	{
		public get()
		{
			return TF2_GetPlayerClass(this.Index);
		}
		public set(TFClassType tfclass)
		{
			TF2_SetPlayerClass(this.Index, tfclass);
		}
	}

	property TFTeam Team
	{
		public get()
		{
			return TF2_GetClientTeam(this.Index);
		}
		public set(TFTeam team)
		{
			TF2_ChangeClientTeam(this.Index, team);
		}
	}

	property bool IsDueling
	{
		public get()
		{
			return TF2_IsPlayerInDuel(this.Index);
		}
	}

	property bool IsPlaying
	{
		public get()
		{
			TFTeam team = this.Team;
			return (team == TFTeam_Blue || team == TFTeam_Red);
		}
	}

	property bool IsAlive
	{
		public get()
		{
			return IsPlayerAlive(this.Index);
		}
	}

	property int Health
	{
		public get()
		{
			return GetEntProp(this.Index, Prop_Send, "m_iHealth");
		}
		public set(int health)
		{
			SetEntProp(this.Index, Prop_Send, "m_iHealth", health);
		}
	}

	property int EntityFlags
	{
		public get()
		{
			return GetEntityFlags(this.Index);
		}
		public set(int value)
		{
			SetEntityFlags(this.Index, value);
		}
	}

	/******************************************
	 *
	 *  Native properties
	 *
	 ******************************************/

	property bool IsBoss
	{
		public native get();
	}

	property Definition Boss
	{
		public native get();
		public native set(Definition boss);
	}

	property TFClassType BossClass
	{
		public native get();
	}

	property int Phase
	{
		public native get();
	}

	property bool IsEndangered
	{
		public native get();
	}

	property int BossMaxHealth
	{
		public native get();
	}

	property float PrimaryChargePercent
	{
		public native get();
	}

	property float SecondaryChargePercent
	{
		public native get();
	}

	property float SpecialChargePercent
	{
		public native get();
	}

	property float CustomSpecialChargePercent
	{
		public native get();
		public native set(float chargePercent);
	}

	/******************************************
	 *
	 *  Wrapper functions
	 *
	 ******************************************/

	public int GetEntProp(PropType propType, const char[] prop, int size=4, int element=0)
	{
		return GetEntProp(this.Index, propType, prop, size, element);
	}

	public void SetEntProp(PropType propType, const char[] prop, int value, int size=4, int element=0)
	{
		SetEntProp(this.Index, propType, prop, value, size, element);
	}

	public float GetEntPropFloat(PropType propType, const char[] prop, int element=0)
	{
		return GetEntPropFloat(this.Index, propType, prop, element);
	}

	public void SetEntPropFloat(PropType propType, const char[] prop, float value, int element=0)
	{
		SetEntPropFloat(this.Index, propType, prop, value, element);
	}

	public int GetEntPropEnt(PropType propType, const char[] prop, int element=0)
	{
		return GetEntPropEnt(this.Index, propType, prop, element);
	}

	public void SetEntPropEnt(PropType propType, const char[] prop, int other, int element=0)
	{
		SetEntPropEnt(this.Index, propType, prop, other, element);
	}

	public void GetEntPropVector(PropType propType, const char[] prop, float vec[3], int element=0)
	{
		GetEntPropVector(this.Index, propType, prop, vec, element);
	}

	public void SetEntPropVector(PropType propType, const char[] prop, const float vec[3], int element=0)
	{
		SetEntPropVector(this.Index, propType, prop, vec, element);
	}

	public int GetEntPropString(PropType propType, const char[] prop, char[] buffer, int maxlen, int element=0)
	{
		return GetEntPropString(this.Index, propType, prop, buffer, maxlen, element);
	}

	public int SetEntPropString(PropType propType, const char[] prop, const char[] buffer, int element=0)
	{
		return SetEntPropString(this.Index, propType, prop, buffer, element);
	}

	public int Teleport(const float origin[3] = NULL_VECTOR, const float angles[3] = NULL_VECTOR, const float velocity[3] = NULL_VECTOR)
	{
		TeleportEntity(this.Index, origin, angles, velocity);
	}

	public bool TeleportTo(float pos[3], int traceFlags = MASK_PLAYERSOLID, TraceEntityFilter filter = INVALID_FUNCTION, any data = 0)
	{
		if (filter == INVALID_FUNCTION)
		{
			return BFF_TeleportTo(this.Index, pos, traceFlags);
		}
		return BFF_TeleportToFilter(this.Index, pos, traceFlags, filter, data);
	}

	public bool Blink(float maxDistance=-1.0, int traceFlags = MASK_PLAYERSOLID, TraceEntityFilter filter = INVALID_FUNCTION, any data = 0)
	{
		if (filter == INVALID_FUNCTION)
		{
			return BFF_Blink(this.Index, maxDistance, traceFlags);
		}
		return BFF_BlinkFilter(this.Index, maxDistance, traceFlags, filter, data);
	}

	/******************************************
	 *
	 *  TF2 wrapper functions
	 *
	 ******************************************/

	public void Ignite(int attacker)
	{
		TF2_IgnitePlayer(this.Index, attacker);
	}

	public void Respawn()
	{
		TF2_RespawnPlayer(this.Index);
	}

	public void Regenerate()
	{
		TF2_RegeneratePlayer(this.Index);
	}

	public void AddCondition(TFCond condition, float duration=TFCondDuration_Infinite, int inflictor=0)
	{
		TF2_AddCondition(this.Index, condition, duration, inflictor);
	}

	public void RemoveCondition(TFCond condition)
	{
		TF2_RemoveCondition(this.Index, condition);
	}

	public bool IsInCondition(TFCond condition)
	{
		return TF2_IsPlayerInCondition(this.Index, condition);
	}

	public void SetPowerPlay(bool enabled)
	{
		TF2_SetPlayerPowerPlay(this.Index, enabled);
	}

	public void DisguisePlayer(TFTeam team, TFClassType classType, int target=0)
	{
		TF2_DisguisePlayer(this.Index, team, classType, target);
	}

	public void RemoveDisguise()
	{
		TF2_RemovePlayerDisguise(this.Index);
	}

	public void Stun(float duration, float slowdown, int stunflags, int attacker=0)
	{
		TF2_StunPlayer(this.Index, duration, slowdown, stunflags, attacker);
	}

	public void MakeBleed(int attacker, float duration)
	{
		TF2_MakeBleed(this.Index, attacker, duration);
	}

	public void SetPlayerClass(TFClassType classType, bool persistent = true)
	{
		TF2_SetPlayerClass(this.Index, classType, _, persistent);
	}

	public void RemoveWeaponSlot(int slot)
	{
		TF2_RemoveWeaponSlot(this.Index, slot);
	}

	public void RemoveAllWeapons()
	{
		TF2_RemoveAllWeapons(this.Index);
	}

	public void TF2_RemoveWearable(int wearable)
	{
		TF2_RemoveWearable(this.Index, wearable);
	}

	/******************************************
	 *
	 *  Native functions
	 *
	 ******************************************/

	public native void ClearBoss();

	public native void SetHealthMultiplier(float multiplier);

	public native void UpdateHud();

	public native void SetPrimaryChargeTime(float newTime, bool adjustPercentage=true);
	public native void SetPrimaryCooldownTime(float newTime, bool adjustPercentage=true);
	public native void SetPrimaryCooldownPercentage(float percentage);

	public native void SetSecondaryChargeTime(float newTime, bool adjustPercentage=true);
	public native void SetSecondaryCooldownTime(float newTime, bool adjustPercentage=true);
	public native void SetSecondaryCooldownPercentage(float percentage);

	public native void SetSpecialCooldown(int damage=-1, float time=-1.0, bool recallCustom=true, bool useCustom=false);

	public native void RemoveAllWearables();

	public native bool PlaySound(char[] sound, bool isGroup=true, bool emitGlobally=false);

	/******************************************
	 *
	 *  Helper functions
	 *
	 ******************************************/

	public void RemoveAllWeaponsAndWearables()
	{
		this.RemoveAllWeapons();
		this.RemoveAllWearables();
	}

	public void SetTemporaryClass(TFClassType classType)
	{
		this.RemoveAllWeaponsAndWearables();
		this.SetPlayerClass(classType, false);
		this.Regenerate();
	}

	public void DeathlessChangeTeam(TFTeam team)
	{
		int lifeState = this.GetEntProp(Prop_Send, "m_lifeState");
		if (lifeState == 0)
		{
			this.SetEntProp(Prop_Send, "m_lifeState", 2);
		}
		this.Team = team;
		if (lifeState == 0)
		{
			this.SetEntProp(Prop_Send, "m_lifeState", 0);
		}
	}

	public void SetModel(const char[] model)
	{
		SetVariantString(model);
		AcceptEntityInput(this.Index, "SetCustomModel");
		this.SetEntProp(Prop_Send, "m_bUseClassAnimations", 1);
	}

	#if defined _tf2items_included
	public int GiveWeapon(char[] classname, int itemIndex, const char[] attributes, TFQuality quality=TFQuality_Normal, int level=42, bool autoSwitch=true, bool visible=true)
	{
		Handle item = BFF_CreateItem(classname, itemIndex, attributes, quality, level, true);

		int itemEntity = TF2Items_GiveNamedItem(this.Index, item);
		EquipPlayerWeapon(this.Index, itemEntity);
		delete item;

		if (autoSwitch)
		{
			this.SetEntPropEnt(Prop_Send, "m_hActiveWeapon", itemEntity);
		}

		if (!visible)
		{
			SetEntPropFloat(itemEntity, Prop_Send, "m_flModelScale", 0.0001);
		}

		return itemEntity;
	}
	#endif
}