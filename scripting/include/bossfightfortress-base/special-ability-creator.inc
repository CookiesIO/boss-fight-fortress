#if defined _BOSSFIGHTFORTRESS_SPECIAL_ABILITY_CREATOR_INCLUDED
	#endinput
#endif
#define _BOSSFIGHTFORTRESS_SPECIAL_ABILITY_CREATOR_INCLUDED

typeset BFF_OnSpecialAbilityUse
{
	function void(int client, float chargePercent, Definition ability);
	function void(BffClient client, float chargePercent, Definition ability);
}

methodmap SpecialAbilityCreator < DefinitionCreator
{
	public SpecialAbilityCreator(const char[] name, BFF_OnSpecialAbilityUse onUse, Definition parentAbility=null)
	{    
		Definition abilityDefinition = FindDefinition(BFF_SPECIAL_ABILITY);
		Definition ability = (parentAbility.IsValid() ? parentAbility : abilityDefinition);
		if (abilityDefinition.IsValid() && (ability == abilityDefinition || ability.IsValidEx(true, abilityDefinition)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, ability);
			creator.SetFunction("OnUse", onUse);
			return view_as<SpecialAbilityCreator>(creator);
		}
		ThrowError("Make sure the "...BFF_SPECIAL_ABILITY..." definition exists before attempting to create new special ability!");
		return null;
	}
}