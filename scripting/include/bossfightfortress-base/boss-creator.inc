#if defined _BOSSFIGHTFORTRESS_BOSS_CREATOR_INCLUDED
  #endinput
#endif
#define _BOSSFIGHTFORTRESS_BOSS_CREATOR_INCLUDED

typeset BFF_NameFormatter
{
	function void(int client, char[] buffer, int bufferSize);
	function void(BffClient client, char[] buffer, int bufferSize);
}

typedef BFF_GetMaxHealth = function int(float multiplier);

typeset BFF_EquipBoss
{
	function void(int client);
	function void(BffClient client);
}

typeset BFF_OnContinuousChargeStart
{
	function bool(int client, float cooldownPercent);
	function bool(BffClient client, float cooldownPercent);

	function bool(int client, float cooldownPercent, float cooldownDifference);
	function bool(BffClient client, float cooldownPercent, float cooldownDifference);
}

typeset BFF_OnContinuousChargeReleased
{
	function void(int client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);

	function void(int client, float cooldownPercent, float cooldownDifference);
	function void(BffClient client, float cooldownPercent, float cooldownDifference);
}

typeset BFF_OnNormalChargeStart
{
	function void(int client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);

	function void(int client, float cooldownPercent, float cooldownDifference);
	function void(BffClient client, float cooldownPercent, float cooldownDifference);
}

typeset BFF_OnNormalChargeReleased
{
	function bool(int client, float cooldownPercent);
	function bool(BffClient client, float cooldownPercent);

	function void(int client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);
}

typeset BFF_ChargeFormatter
{
	function void(int client, char[] buffer, int bufferSize, AbilityState abilityState, int percentage);
	function void(BffClient client, char[] buffer, int bufferSize, AbilityState abilityState, int percentage);
}

typeset BFF_OnUseSpecial
{
	function bool(int client, float charge);
	function bool(BffClient client, float charge);
}

typeset BFF_SpecialFormatter
{
	function void(int client, char[] buffer, int bufferSize, AbilityState abilityState, int percentage);
	function void(BffClient client, char[] buffer, int bufferSize, AbilityState abilityState, int percentage);
}

typeset BFF_BossOnPlayerRunCmd
{
	function Action(int client, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon);
	function Action(BffClient client, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon);
}

typeset BFF_ChargeOnPlayerRunCmd
{
	function Action(int client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon);
	function Action(BffClient client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon);
}

typeset BFF_SoundHook
{
	function Action(int client, int clients[MAXPLAYERS], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags);
	function Action(BffClient client, int clients[MAXPLAYERS], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags);
}

methodmap BffBossCreator < DefinitionCreator
{
	public BffBossCreator(const char[] name, TFClassType classType, BFF_GetMaxHealth getMaxHealth, BFF_EquipBoss equipBoss=INVALID_FUNCTION, Definition parentBoss=null)
	{
		Definition bffBoss = FindDefinition(BFF_BOSS_BASE);
		Definition bffGenericBoss = FindDefinition(BFF_BOSS_GENERIC);
		Definition boss = (parentBoss.IsValid() ? parentBoss : bffGenericBoss);
		if (bffBoss.IsValid() && bffGenericBoss.IsValid() && (boss == bffGenericBoss || boss.IsValidEx(true, bffBoss)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, boss);
			creator.SetString("DisplayName", name);
			creator.SetCell("Class", classType);
			creator.SetFunction("GetMaxHealth", getMaxHealth);
			creator.SetFunction("EquipBoss", equipBoss);
			return view_as<BffBossCreator>(creator);
		}
		ThrowError("Make sure that both the "...BFF_BOSS_BASE..." and the "...BFF_BOSS_GENERIC..." definitions exists before attempting to create a new boss!");
		return null;
	}

	public void SetDisplayName(const char[] name)
	{
		this.SetString("DisplayName", name);
	}

	public void SetNameFormatter(BFF_NameFormatter nameFormatter)
	{
		this.SetFunction("FormatName", nameFormatter);
	}

	public void SetCustomModel(const char[] model)
	{
		this.SetString("CustomModel", model);
	}

	public void AddSoundGroup(const char[] group)
	{
		this.DefineStringArray(group, PLATFORM_MAX_PATH);
		this.PushStringToArray("SoundGroups", group);
	}

	public void SetPhaseMode(PhaseMode phaseMode)
	{
		this.SetCell("PhaseMode", phaseMode);
	}

	public void SetPhaseReliability(PhaseReliability phaseReliability)
	{
		this.SetCell("PhaseReliability", phaseReliability);
	}

	public void AddPhaseAt(float phaseAt)
	{
		this.PushFloatToArray("PhasesAt", phaseAt);
	}

	public void SetPhases(PhaseMode phaseMode, PhaseReliability phaseReliability, float[] phasesAt, int phaseCount)
	{
		this.SetPhaseMode(phaseMode);
		this.SetPhaseReliability(phaseReliability);
		this.ClearFloatArray("PhasesAt");
		for (int i = 0; i < phaseCount; i++)
		{
			this.PushFloatToArray("PhasesAt", phasesAt[i]);
		}
	}

	public void AddPhaseAbility(int phase, char[] ability)
	{
		char property[64];
		Format(property, sizeof(property), "PhaseAbilities<%d>", phase);
		this.PushDefinitionToArray(property, ability);
	}

	public void SetPhaseAbility(int phase, char[][] abilities, int abilityCount)
	{
		char property[64];
		Format(property, sizeof(property), "PhaseAbilities<%d>", phase);
		this.ClearDefinitionArray(property);
		for (int i = 0; i < abilityCount; i++)
		{
			this.PushDefinitionToArray(property, abilities[i]);
		}
	}

	public void SetOnPlayerRunCmd(BFF_BossOnPlayerRunCmd onPlayerRunCmd)
	{
		this.SetFunction("OnPlayerRunCmd", onPlayerRunCmd);
	}

	public void SetSoundHook(BFF_SoundHook soundHook)
	{
		this.SetFunction("SoundHook", soundHook);
	}

	/**
	 * Charge ability stuff
	 **/

	public void SetNormalAbility(AbilitySlot slot, int phase, const char[] name, BFF_OnNormalChargeReleased onChargeReleased, float chargeTime=-1.0, float cooldownTime=-1.0, BFF_OnNormalChargeStart onChargeStart=INVALID_FUNCTION, BFF_ChargeOnPlayerRunCmd onPlayerRunCmd=INVALID_FUNCTION)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "%sAbilityName<%d>", type, phase);
		this.SetString(property, name);

		if (chargeTime > 0.0)
		{
			Format(property, sizeof(property), "%sChargeTime<%d>", type, phase);
			this.SetFloat(property, chargeTime);
		}

		if (cooldownTime > 0.0)
		{
			Format(property, sizeof(property), "%sCooldownTime<%d>", type, phase);
			this.SetFloat(property, cooldownTime);
		}

		if (onChargeStart != INVALID_FUNCTION)
		{
			Format(property, sizeof(property), "On%sStart<%d>", type, phase);
			this.SetFunction(property, onChargeStart);
		}

		if (onPlayerRunCmd != INVALID_FUNCTION)
		{
			Format(property, sizeof(property), "On%sPlayerRunCmd<%d>", type, phase);
			this.SetFunction(property, onPlayerRunCmd);
		}

		Format(property, sizeof(property), "On%sReleased<%d>", type, phase);
		this.SetFunction(property, onChargeReleased);

		Format(property, sizeof(property), "%sChargeMode<%d>", type, phase);
		this.SetCell(property, ChargeMode_Normal);
	}

	public void SetContinuousAbility(AbilitySlot slot, int phase, const char[] name, BFF_OnContinuousChargeStart onChargeStart, BFF_OnContinuousChargeReleased onChargeReleased, float chargeTime=-1.0, float cooldownTime=-1.0, BFF_ChargeOnPlayerRunCmd onPlayerRunCmd=INVALID_FUNCTION)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "%sAbilityName<%d>", type, phase);
		this.SetString(property, name);

		if (chargeTime > 0.0)
		{
			Format(property, sizeof(property), "%sChargeTime<%d>", type, phase);
			this.SetFloat(property, chargeTime);
		}

		if (cooldownTime > 0.0)
		{
			Format(property, sizeof(property), "%sCooldownTime<%d>", type, phase);
			this.SetFloat(property, cooldownTime);
		}

		if (onPlayerRunCmd != INVALID_FUNCTION)
		{
			Format(property, sizeof(property), "On%sPlayerRunCmd<%d>", type, phase);
			this.SetFunction(property, onPlayerRunCmd);
		}

		Format(property, sizeof(property), "On%sStart<%d>", type, phase);
		this.SetFunction(property, onChargeStart);

		Format(property, sizeof(property), "On%sReleased<%d>", type, phase);
		this.SetFunction(property, onChargeReleased);

		Format(property, sizeof(property), "%sChargeMode<%d>", type, phase);
		this.SetCell(property, ChargeMode_Continuous);
	}

	public void SetAbilityFormatter(AbilitySlot slot, int phase, BFF_ChargeFormatter chargeFormatter)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "%sFormatHud<%d>", type, phase);
		this.SetFunction(property, chargeFormatter);
	}

	public void SetChargeTimes(AbilitySlot slot, int phase, float chargeTime, float cooldownTime)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		if (chargeTime > 0.0)
		{
			Format(property, sizeof(property), "%sChargeTime<%d>", type, phase);
			this.SetFloat(property, chargeTime);
		}

		if (cooldownTime > 0.0)
		{
			Format(property, sizeof(property), "%sCooldownTime<%d>", type, phase);
			this.SetFloat(property, cooldownTime);
		}
	}

	public void SetAbility(AbilitySlot slot, int phase, const char[] ability, const char[] name, float chargeTime=-1.0, float cooldownTime=-1.0)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "%sAbility<%d>", type, phase);
		this.SetDefinition(property, ability);

		Format(property, sizeof(property), "%sAbilityName<%d>", type, phase);
		this.SetString(property, name);

		if (chargeTime > 0.0)
		{
			Format(property, sizeof(property), "%sChargeTime<%d>", type, phase);
			this.SetFloat(property, chargeTime);
		}

		if (cooldownTime > 0.0)
		{
			Format(property, sizeof(property), "%sCooldownTime<%d>", type, phase);
			this.SetFloat(property, cooldownTime);
		}
	}

	public void EnableAbilityAt(AbilitySlot slot, int phase)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "Has%sAbility<%d>", type, phase);
		this.SetBool(property, true);
	}

	public void DisableAbilityAt(AbilitySlot slot, int phase)
	{
		char type[16];
		FormatAbilitySlot(type, sizeof(type), slot);

		char property[64];
		Format(property, sizeof(property), "Has%sAbility<%d>", type, phase);
		this.SetBool(property, false);
	}

	/**
	 * Special ability stuff
	 **/

	public void SetSpecialAbility(int phase, const char[] name, SpecialChargeMode mode, int cooldownDamage, float cooldownTime, bool cooldownCustom, BFF_OnUseSpecial onUseSpecial=INVALID_FUNCTION)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialAbilityName<%d>", phase);
		this.SetString(property, name);

		Format(property, sizeof(property), "SpecialUseCustomCharge<%d>", phase);
		this.SetBool(property, cooldownCustom);

		Format(property, sizeof(property), "SpecialChargeDamage<%d>", phase);
		this.SetInt(property, cooldownDamage);

		Format(property, sizeof(property), "SpecialChargeTime<%d>", phase);
		this.SetFloat(property, cooldownTime);

		Format(property, sizeof(property), "SpecialChargeMode<%d>", phase);
		this.SetCell(property, mode);

		if (onUseSpecial != INVALID_FUNCTION)
		{
			Format(property, sizeof(property), "OnUseSpecialAbility<%d>", phase);
			this.SetFunction(property, onUseSpecial);
		}
	}

	public void SetSpecialAbilityName(int phase, const char[] name)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialAbilityName<%d>", phase);
		this.SetString(property, name);
	}

	public void SetSpecialFormatter(int phase, BFF_SpecialFormatter specialFormatter)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialFormatHud<%d>", phase);
		this.SetFunction(property, specialFormatter);
	}

	public void SetOnUseSpecialAbility(int phase, BFF_OnUseSpecial onUseSpecial)
	{
		char property[64];
		Format(property, sizeof(property), "OnUseSpecialAbility<%d>", phase);
		this.SetFunction(property, onUseSpecial);
	}

	public void SetSpecialCooldown(int phase, int damage, float time, bool recallCustom, bool useCustom=false)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialChargeTime<%d>", phase);
		this.SetFloat(property, time);

		Format(property, sizeof(property), "SpecialChargeDamage<%d>", phase);
		this.SetInt(property, damage);

		if (recallCustom)
		{
			Format(property, sizeof(property), "SpecialUseCustomCharge<%d>", phase);
			this.SetBool(property, useCustom);
		}
	}

	public void AddSpecialAbility(int phase, const char[] ability)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialAbilities<%d>", phase);
		this.PushDefinitionToArray(property, ability);
	}

	public void SetSpecialAbilities(int phase, const char[][] abilities, int abilityCount)
	{
		char property[64];
		Format(property, sizeof(property), "SpecialAbilities<%d>", phase);
		this.ClearDefinitionArray(property);
		for (int i = 0; i < abilityCount; i++)
		{
			this.PushDefinitionToArray(property, abilities[i]);
		}
	}

	public void SetSpecialAbilityMinimumCharge(int phase, float charge)
	{
		char property[64];
		Format(property, sizeof(property), "DefaultOnUseSpecialAbilityMinimumCharge<%d>", phase);
		this.SetFloat(property, charge);
	}

	public void EnableSpecialAbilityAt(int phase)
	{
		char property[64];
		Format(property, sizeof(property), "HasSpecialAbility<%d>", phase);
		this.SetBool(property, true);
	}

	public void DisableSpecialAbilityAt(int phase)
	{
		char property[64];
		Format(property, sizeof(property), "HasSpecialAbility<%d>", phase);
		this.SetBool(property, false);
	}
}

static stock void FormatAbilitySlot(char[] type, int length, AbilitySlot slot)
{
	if (slot == AbilitySlot_Primary)
	{
		Format(type, length, "Primary");
	}
	else
	{
		Format(type, length, "Secondary");
	}
}