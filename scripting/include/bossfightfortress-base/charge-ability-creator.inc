#if defined _BOSSFIGHTFORTRESS_CHARGE_ABILITY_CREATOR_INCLUDED
  #endinput
#endif
#define _BOSSFIGHTFORTRESS_CHARGE_ABILITY_CREATOR_INCLUDED

typeset BFF_OnContinuousChargeAbilityStart
{
	function bool(int client, float cooldownPercent);
	function void(int client, float cooldownPercent);
	function bool(BffClient client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);

	function bool(int client, float cooldownPercent, float cooldownDifference);
	function void(int client, float cooldownPercent, float cooldownDifference);
	function bool(BffClient client, float cooldownPercent, float cooldownDifference);
	function void(BffClient client, float cooldownPercent, float cooldownDifference);

	function bool(int client, float cooldownPercent, float cooldownDifference, Definition ability);
	function void(int client, float cooldownPercent, float cooldownDifference, Definition ability);
	function bool(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability);
	function void(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability);
}

typeset BFF_OnContinuousChargeAbilityReleased
{
	function void(int client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);

	function void(int client, float cooldownPercent, float cooldownDifference);
	function void(BffClient client, float cooldownPercent, float cooldownDifference);

	function void(int client, float cooldownPercent, float cooldownDifference, Definition ability);
	function void(BffClient client, float cooldownPercent, float cooldownDifference, Definition ability);
}

typeset BFF_OnNormalChargeAbilityStart
{
	function void(int client, float cooldownPercent, Definition ability);
	function void(BffClient client, float cooldownPercent);

	function void(int client, float cooldownPercent, Definition ability);
	function void(BffClient client, float cooldownPercent, Definition ability);
}

typeset BFF_OnNormalChargeAbilityReleased
{
	function bool(int client, float cooldownPercent);
	function void(int client, float cooldownPercent);
	function bool(BffClient client, float cooldownPercent);
	function void(BffClient client, float cooldownPercent);

	function bool(int client, float cooldownPercent, Definition ability);
	function void(int client, float cooldownPercent, Definition ability);
	function bool(BffClient client, float cooldownPercent, Definition ability);
	function void(BffClient client, float cooldownPercent, Definition ability);
}

typeset BFF_AbilityOnPlayerRunCmd
{
	function Action(int client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon, Definition ability);
	function Action(BffClient client, float chargePercent, int &buttons, int &impulse, float vel[3], float ang[3], int &weapon, Definition ability);
}

// Normal and Continuous abilities share some, but not all
methodmap ChargeAbilityCreator < DefinitionCreator
{
	public ChargeAbilityCreator()
	{
		ThrowError("No can do making just a ChargeAbility, use either 'NormalChargeAbilityCreator' or 'ContinuousChargeAbilityCreator'");
		return null;
	}

	public void SetCanSaveFromDanger(bool value=true)
	{
		this.SetBool("CanSaveFromDanger", value);
	}

	public void SetIsAerialEscape(bool value=true)
	{
		this.SetBool("IsAerialEscape", value);
	}

	public void SetOnPlayerRunCmd(BFF_AbilityOnPlayerRunCmd onPlayerRunCmd)
	{
		this.SetFunction("OnPlayerRunCmd", onPlayerRunCmd);
	}
}

methodmap NormalChargeAbilityCreator < ChargeAbilityCreator
{
	public NormalChargeAbilityCreator(const char[] name, BFF_OnNormalChargeAbilityReleased onChargeReleased, Definition parentAbility=null)
	{    
		Definition abilityDefinition = FindDefinition(BFF_NORMAL_CHARGE_ABILITY);
		Definition ability = (parentAbility.IsValid() ? parentAbility : abilityDefinition);
		if (abilityDefinition.IsValid() && (ability == abilityDefinition || ability.IsValidEx(true, abilityDefinition)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, ability);
			creator.SetFunction("OnChargeReleased", onChargeReleased);
			return view_as<NormalChargeAbilityCreator>(creator);
		}
		ThrowError("Make sure the "...BFF_NORMAL_CHARGE_ABILITY..." definition exists before attempting to create new normal charge ability!");
		return null;
	}

	public void SetOnChargeStart(BFF_OnNormalChargeAbilityStart onChargeStart)
	{
		this.SetFunction("OnChargeStart", onChargeStart);
	}
}

methodmap ContinuousChargeAbilityCreator < ChargeAbilityCreator
{
	public ContinuousChargeAbilityCreator(const char[] name, BFF_OnContinuousChargeAbilityStart onChargeStart, BFF_OnContinuousChargeAbilityReleased onChargeReleased, Definition parentAbility=null)
	{    
		Definition abilityDefinition = FindDefinition(BFF_CONTINUOUS_CHARGE_ABILITY);
		Definition ability = (parentAbility.IsValid() ? parentAbility : abilityDefinition);
		if (abilityDefinition.IsValid() && (ability == abilityDefinition || ability.IsValidEx(true, abilityDefinition)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, ability);
			creator.SetFunction("OnChargeStart", onChargeStart);
			creator.SetFunction("OnChargeReleased", onChargeReleased);
			return view_as<ContinuousChargeAbilityCreator>(creator);
		}
		ThrowError("Make sure the "...BFF_CONTINUOUS_CHARGE_ABILITY..." definition exists before attempting to create new continuous charge ability!");
		return null;
	}
}