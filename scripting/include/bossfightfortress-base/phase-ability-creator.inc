#if defined _BOSSFIGHTFORTRESS_PHASE_ABILITY_CREATOR_INCLUDED
	#endinput
#endif
#define _BOSSFIGHTFORTRESS_PHASE_ABILITY_CREATOR_INCLUDED

typeset BFF_OnPhaseAbilityEnter
{
	function void(int client, Definition ability);
	function void(BffClient client, Definition ability);
}

typeset BFF_OnPhaseAbilityExit
{
	function void(int client, Definition ability);
	function void(BffClient client, Definition ability);
}

methodmap PhaseAbilityCreator < DefinitionCreator
{
	public PhaseAbilityCreator(const char[] name, BFF_OnPhaseAbilityEnter onEnter, BFF_OnPhaseAbilityExit onExit, Definition parentAbility=null)
	{    
		Definition abilityDefinition = FindDefinition(BFF_PHASE_ABILITY);
		Definition ability = (parentAbility.IsValid() ? parentAbility : abilityDefinition);
		if (abilityDefinition.IsValid() && (ability == abilityDefinition || ability.IsValidEx(true, abilityDefinition)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, ability);
			creator.SetFunction("OnPhaseEnter", onEnter);
			creator.SetFunction("OnPhaseExit", onExit);
			return view_as<PhaseAbilityCreator>(creator);
		}
		ThrowError("Make sure the "...BFF_PHASE_ABILITY..." definition exists before attempting to create new phase ability!");
		return null;
	}
}