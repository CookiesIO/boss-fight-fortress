#if defined _BOSSFIGHTFORTRESS_GAME_MODE_CREATOR_INCLUDED
  #endinput
#endif
#define _BOSSFIGHTFORTRESS_GAME_MODE_CREATOR_INCLUDED

typedef BFF_OnRoundActive = function void();

methodmap BffGameModeCreator < GameModeCreator
{
	public BffGameModeCreator(const char[] name, GameModeStart start, GameModeEnded ended, BFF_OnRoundActive onRoundActive, Definition parentGameMode=null)
	{
		Definition bffGameMode = FindDefinition(BFF_GAMEMODE_BASE);
		Definition gameMode = (parentGameMode.IsValid() ? parentGameMode : bffGameMode);
		if (bffGameMode.IsValid() && (gameMode == bffGameMode || gameMode.IsValidEx(true, bffGameMode)))
		{
			DefinitionCreator creator = new DefinitionCreator(name, gameMode);
			creator.SetFunction("BffStart", start);
			creator.SetFunction("BffEnded", ended);
			creator.SetFunction("OnRoundActive", onRoundActive);
			return view_as<BffGameModeCreator>(creator);
		}
		ThrowError("Make sure the "...BFF_GAMEMODE_BASE..." definition exists before attempting to create new game modes!");
		// Statisfy the compiler.. :/
		return null;
	}

	public void SetAllowChangeClass(bool value)
	{
		this.SetBool("AllowChangeClass", value);
	}
}