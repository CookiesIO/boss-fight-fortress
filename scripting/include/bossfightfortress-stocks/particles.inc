/*
	Chdata's reworked attachparticle, now in new syntax!
	Also decided to rename things to my own preference
*/
stock int BFF_SpawnParticleNear(int target, const char[] particleEffect, float timeToLive = -1.0, const float offset[3] = {0.0,...}, bool attach = false, float startDelay = -1.0)
{
	float pos[3];
	GetEntPropVector(target, Prop_Send, "m_vecOrigin", pos);
	AddVectors(pos, offset, pos);
	return BFF_SpawnParticleAt(pos, particleEffect, timeToLive, attach ? target : -1, startDelay);
}

stock int BFF_SpawnParticleAt(const float[3] pos, const char[] particleEffect, float timeToLive = -1.0, int parent = -1, float startDelay = -1.0)
{
	int particle = CreateEntityByName("info_particle_system");
	if (IsValidEntity(particle))
	{
		TeleportEntity(particle, pos, NULL_VECTOR, NULL_VECTOR);

		DispatchKeyValue(particle, "effect_name", particleEffect);
		DispatchSpawn(particle);

		if (parent != -1)
		{
			BFF_SetParent(particle, parent);
			SetEntPropEnt(particle, Prop_Send, "m_hOwnerEntity", parent);
		}

		ActivateEntity(particle);

		if (startDelay > 0.0)
		{
			char output[32];
			Format(output, sizeof(output), "OnUser1 !self,Start,,%0.2f,1", startDelay);
			SetVariantString(output);
			AcceptEntityInput(particle, "AddOutput");
			AcceptEntityInput(particle, "FireUser1");

			if (timeToLive > 0.0)
			{
				timeToLive += startDelay;
			}
		}
		else
		{
			AcceptEntityInput(particle, "Start");
		}

		if (timeToLive > 0.0) 
		{
			// Original comment:
			// Interestingly, OnUser1 can be used multiple times, as the code above won't conflict with this.
			// 
			// My comment: The above output will fire once, and will then be deleted, it can only be used one,
			// as defined in the last option. Format from https://developer.valvesoftware.com/wiki/AddOutput
			// <targetname>:<inputname>:<parameter>:<delay>:<max times to fire, -1 means infinite>
			// following thisforamt , the above start delay code has the following settings:
			// target name: !self, inputname: Start, parameter: {nothing}, delay: %0.2f, max time to fire: 1
			// so it can only be used once.
			// Besides, even if it was fired twice, it's just calling start twice, so the second time it'd likely
			// be ignored, or just be restarted in the /same/ frame, however it works.
			BFF_KillEntityIn(particle, timeToLive); 
		}

		return particle;
	}
	return -1;
}