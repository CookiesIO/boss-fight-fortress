//#define TELEPORT_DEBUG

// The ratio between standing and ducking, used to determine ducking Maxs height
// 62.0 / 82.0 = 0.75609756097
static const float DuckStandingRatio = 0.75609756097;

// Horizontally radial resolution used to adjust teleport destination if required
// To visualize it, it's just evenly spaced bars going out from the center of a circle
// It should be kept acceptable, as multiple adjusts might be performed per teleport
static const int TeleportAdjustResolution = 16;

// Maximum steps to check in any direction to see if we fits
static const int VerticalTestSteps = 4;

// Horizontal test steps > vertical ones!
static const int HorizontalTestSteps = 8;

// Options variables
static int g_iTraceFlags = 0;
static TraceEntityFilter g_traceFilter = INVALID_FUNCTION;
static any g_traceData = 0;

stock bool BFF_Blink(int client, float maxDistance=-1.0, int traceFlags = MASK_PLAYERSOLID)
{
	return BFF_BlinkFilter(client, maxDistance, traceFlags, DefaultAdjustTeleportTraceFilter, client);
}

stock bool BFF_BlinkFilter(int client, float maxDistance, int traceFlags, TraceEntityFilter filter, any data = 0)
{
	if (client <= 0 || client > MaxClients)
	{
		ThrowError("Invalid client index (%d) provided", client);
		return false;
	}
	if (IsClientInGame(client) == false)
	{
		ThrowError("Client (%d) not in game", client);
		return false;
	}
	if (IsPlayerAlive(client) == false)
	{
		// Don't think an error is needed, just return false instead
		//ThrowError("Client (%d) is not alive", client);
		return false;
	}

	SetupTeleportParameters(filter, data, traceFlags);

	float targetPos[3];
	float eyeAngles[3];
	float eyePos[3];
	float fwd[3];
	float maxs[3];

	GetClientEyeAngles(client, eyeAngles);
	GetClientEyePosition(client, eyePos);
	GetEntPropVector(client, Prop_Send, "m_vecMaxs", maxs);

	GetAngleVectors(eyeAngles, fwd, NULL_VECTOR, NULL_VECTOR);
	
	if (maxDistance <= 0.0)
	{
		TraceRay(eyePos, eyeAngles, RayType_Infinite);
		TR_GetEndPosition(targetPos);
	}
	else
	{
		CopyVector(eyePos, targetPos);
		ScaleVector(fwd, maxDistance);
		AddVectors(targetPos, fwd, targetPos);

		TraceRay(eyePos, targetPos, RayType_EndPoint);
		TR_GetEndPosition(targetPos);

		NormalizeVector(fwd, fwd);
	}

	// Move this a little close to us, to not teleport through walls, etc :P
	ScaleVector(fwd, maxs[0]);
	SubtractVectors(targetPos, fwd, targetPos);

	return BFF_TeleportToFilter(client, targetPos, traceFlags, filter, data);
}

stock bool BFF_TeleportTo(int entity, float pos[3], int traceFlags = MASK_PLAYERSOLID)
{
	return BFF_TeleportToFilter(entity, pos, traceFlags, DefaultAdjustTeleportTraceFilter, entity);
}

stock bool BFF_TeleportToFilter(int entity, float pos[3], int traceFlags, TraceEntityFilter filter, any data = 0)
{
	if (IsValidEntity(entity) == false)
	{
		ThrowError("Entity (%d) is not valid");
		return false;
	}

	bool isClient = entity >= 1 && entity <= MaxClients;
	if (isClient && (IsClientInGame(entity) == false || IsPlayerAlive(entity) == false))
	{
		return false;
	}

	SetupTeleportParameters(filter, data, traceFlags);
	
	float mins[3];
	float maxs[3];
	GetEntPropVector(entity, Prop_Send, "m_vecMins", mins);
	GetEntPropVector(entity, Prop_Send, "m_vecMaxs", maxs);

	bool ducking = isClient == false || GetEntProp(entity, Prop_Send, "m_bDucked") != 0;
	bool newDucking = ducking; 

	if (AdjustTeleportParameters(pos, mins, maxs, newDucking))
	{
		if (isClient && newDucking && !ducking)
		{
			maxs[2] *= DuckStandingRatio;
			SetEntProp(entity, Prop_Send, "m_bDucked", 1);
			SetEntPropVector(entity, Prop_Send, "m_vecMaxs", maxs);
			SetEntityFlags(entity, GetEntityFlags(entity) | FL_DUCKING);
		}

		TeleportEntity(entity, pos, NULL_VECTOR, NULL_VECTOR);
		return true;
	}
	return false;
}

static stock void SetupTeleportParameters(TraceEntityFilter filter, any data, int traceFlags)
{
	g_iTraceFlags = traceFlags;
	g_traceFilter = filter;
	g_traceData = data;
}

static stock bool AdjustTeleportParameters(float pos[3], const float mins[3], const float maxs[3], bool &ducking)
{
	bool hitGround = false;
	bool hitCeiling = false;
	float newPos[3];
	CopyVector(pos, newPos);

	float downTraceMaxs[3];
	CopyVector(maxs, downTraceMaxs);
	downTraceMaxs[2] = 1.0;

	// Check if we're near the ground
	newPos[2] -= maxs[2];
	TraceRay(pos, newPos, RayType_EndPoint);
	hitGround = TR_DidHit();
	newPos[2] = pos[2];

	// Shouldn't do anything if we hit the ground, but if we didn't, we might have to account for the ceiling above
	if (hitGround == false)
	{
		newPos[2] += maxs[2];
		TraceRay(pos, newPos, RayType_EndPoint);
		if (TR_DidHit())
		{
			hitCeiling = true;

			float endPos[3];
			TR_GetEndPosition(endPos);

			// Adjust destination for the ceiling
			newPos[2] -= ((newPos[2] + maxs[2]) - endPos[2]) + 5.0;
			CopyVector(newPos, pos);
		}
		else
		{
			newPos[2] = pos[2];
		}
	}

	// We'll use the bounding box to define the vertical step, helps adjust for larger stuffs
	float verticalStep = (maxs[2] - mins[2]) / VerticalTestSteps;
	verticalStep = hitCeiling ? -verticalStep : verticalStep;

	for (int i = 0; i < VerticalTestSteps; i++)
	{
		newPos[2] = pos[2] + (verticalStep * i);

		TraceHull(newPos, newPos, mins, maxs);
		if (TR_DidHit())
		{
			// If it did hit, and we're not ducking, we can try again with the ducking maxs
			if (!ducking)
			{
				float newMaxs[3];
				CopyVector(maxs, newMaxs);
				newMaxs[2] *= DuckStandingRatio;
				TraceHull(newPos, newPos, mins, newMaxs);
				if (!TR_DidHit())
				{
					ducking = true;
					return true;
				}
			}
		}

		// So, either we were ducking, or we just didn't fit, so we gotta try in some more creative ways
		bool newDucking = ducking;
		if (AdjustTeleportHorizontally(newPos, mins, maxs, newDucking))
		{
			float newMaxs[3];
			CopyVector(maxs, newMaxs);
			if (!ducking && newDucking)
			{
				newMaxs[2] *= DuckStandingRatio;
			}

			// Trace towards the original target position, we might be able to get to hug it
			TraceHull(newPos, pos, mins, newMaxs);
			TR_GetEndPosition(pos);

			ducking = newDucking;
			return true;
		}
	}
	return false;
}

static stock bool AdjustTeleportHorizontally(float pos[3], const float mins[3], const float maxs[3], bool &ducking)
{
	// Mins/maxs [0] and [1] are the same for players, so just assume that, it makes everything simpler
	// Don't think this should be used for anything else but players either
	// We'll also adjust the wanted distance to be a little bigger, helps with fitting one in
	float wantedDistance = (maxs[0] - mins[0]) * 1.25;
	float fwdScale = (wantedDistance * 1.2) / HorizontalTestSteps;

	float fwd[3];
	float stepPos[3];
	float angles[3] = { 0.0, ... };
	CopyVector(pos, stepPos);
	AdjustByHorizontalSampling(stepPos, wantedDistance, angles[1]);
	GetAngleVectors(angles, fwd, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(fwd, fwdScale);
	
	float duckingMaxs[3];
	CopyVector(maxs, duckingMaxs);
	duckingMaxs[2] *= (ducking ? 1.0 : DuckStandingRatio);

	float testPos[3];
	float downTraceMaxs[3];
	CopyVector(maxs, downTraceMaxs);
	downTraceMaxs[2] = 1.0;

	for (int i = 0; i < HorizontalTestSteps; i++)
	{
		CopyVector(stepPos, testPos);
		testPos[2] -= (ducking ? maxs[2] / DuckStandingRatio : maxs[2]);

		DrawMarker(stepPos, { 127, 127, 0, 255 });
		DrawBBoxBottom(testPos, mins, maxs, { 127, 0, 0, 255 });

		// Trace down with a thin plate of our maxs, to get to the ground if possible
		TraceHull(stepPos, testPos, mins, downTraceMaxs);
		if (TR_DidHit())
		{
			TR_GetEndPosition(testPos);

			// Hull traces are ... unreliable when scanning distances, followup with a normal trace
			TraceRay(stepPos, testPos, RayType_EndPoint);
			TR_GetEndPosition(testPos);

			DrawLine(testPos, pos, {255, 0, 0, 255});
			DrawLine(testPos, stepPos, {0, 0, 255, 255});

			// Only use the testPos if a ray from there doesn't hit anything
			TraceRay(pos, testPos, RayType_EndPoint);
			if (TR_DidHit())
			{
				CopyVector(stepPos, testPos);
			}
		}

		DrawMarker(testPos, { 255, 0, 255, 255 });
		DrawBBoxBottom(testPos, mins, maxs, { 255, 0, 255, 255 });

		// Test without ducking
		TraceHull(testPos, testPos, mins, maxs);
		if (TR_DidHit() == false)
		{
			// Success, enough space!
			CopyVector(testPos, pos);
			return true;
		}
		else if (!ducking)
		{
			// Test with ducking
			TraceHull(testPos, testPos, mins, duckingMaxs);
			if (TR_DidHit() == false)
			{
				// Success, space if we duck!
				CopyVector(testPos, pos);
				ducking = true;
				return true;
			}
		}

		// We should only adjust after the first iteration, so the pos has been moved
		if (i != 0)
		{
			AdjustByHorizontalSampling(stepPos, wantedDistance, angles[1]);
			GetAngleVectors(angles, fwd, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(fwd, fwdScale);
		}
		AddVectors(stepPos, fwd, stepPos);
	}
	return false;
}

static stock void AdjustByHorizontalSampling(float pos[3], float wantedDistance, float &newAngle)
{
	float angle = 360.0 / TeleportAdjustResolution;

	// Take a few looksies in all directions, get a few measurements of distance from it
	// We'll make some approximations for this, and hope we can fit ourselves in somewhere
	float[] distances = new float[TeleportAdjustResolution];
	float[] weightedSamples = new float[TeleportAdjustResolution];

	// Perform the sampling and select the best sample, we'll try and move in that direction
	SampleDistances(pos, wantedDistance, distances, weightedSamples, TeleportAdjustResolution);
	int bestSampleIndex = FindPreferredSample(wantedDistance, weightedSamples, TeleportAdjustResolution);

	int worstCombinedDistanceIndex = WrapAround(bestSampleIndex + (TeleportAdjustResolution >> 2), 0, TeleportAdjustResolution - 1);
	float worstCombinedDistanceValue = CombineWithOppositeSample(distances, TeleportAdjustResolution, worstCombinedDistanceIndex);
	float worstDistanceValue = distances[worstCombinedDistanceIndex];

	DrawLaserFrom(pos, angle * bestSampleIndex, { 255, 0, 0, 255 }, 50.0);
	DrawLaserFrom(pos, angle * WrapAround(bestSampleIndex + (TeleportAdjustResolution >> 2), 0, TeleportAdjustResolution - 1), { 255, 0, 0, 255 }, 50.0);
	DrawLaserFrom(pos, angle * WrapAround(bestSampleIndex - (TeleportAdjustResolution >> 2), 0, TeleportAdjustResolution - 1), { 255, 255, 0, 255 }, 50.0);

	// Try a more delicate approach if the worst combined sample is too small
	if (worstCombinedDistanceValue < (wantedDistance * 2.0))
	{
		float adjustment[3];
		float angles[3] = { 0.0, ... };

		float oppositeSample = worstCombinedDistanceValue - worstDistanceValue;
		float scale = (worstDistanceValue - oppositeSample) * 0.5;

		angles[1] = worstCombinedDistanceIndex * angle;
		GetAngleVectors(angles, adjustment, NULL_VECTOR, NULL_VECTOR);
		ScaleVector(adjustment, scale);
		AddVectors(pos, adjustment, pos);

		// Since we moved a bit, our preferred direction could have changed (even a lot), so get the new sampling results
		SampleDistances(pos, wantedDistance, distances, weightedSamples, TeleportAdjustResolution);
		bestSampleIndex = FindPreferredSample(wantedDistance, weightedSamples, TeleportAdjustResolution);

		DrawLaserFrom(pos, angle * bestSampleIndex, { 0, 0, 255, 255 });
		DrawLaserFrom(pos, angle * WrapAround(bestSampleIndex + (TeleportAdjustResolution >> 2), 0, TeleportAdjustResolution - 1), { 0, 0, 255, 255 });
		DrawLaserFrom(pos, angle * WrapAround(bestSampleIndex - (TeleportAdjustResolution >> 2), 0, TeleportAdjustResolution - 1), { 0, 255, 255, 255 });
	}

	newAngle = angle * bestSampleIndex;
}

static stock void SampleDistances(const float pos[3], float wantedDistance, float[] distances, float[] weightedSamples, int sampleCount)
{
	float angle = 360.0 / sampleCount;
	float angles[3] = { 0.0, 0.0, 0.0 };
	float temp[3];

	// First we should get distances in all directions
	for (int i = 0; i < sampleCount; i++)
	{
		angles[1] = angle * i;
		TraceRay(pos, angles, RayType_Infinite);
		TR_GetEndPosition(temp);

		distances[i] = GetVectorDistance(pos, temp, false);
	}

	// Samples should be taken across a roughly 90 degree angle (a 4th of the distances),
	// in order to make sure there's space in that direction
	int sampleSize = sampleCount >> 2 + 1;
	int halfSampleSize = sampleSize >> 1;

	for (int i = 0; i < sampleCount; i++)
	{
		int weight = 0;
		float sum = 0.0;
		for (int j = -halfSampleSize; j <= halfSampleSize; j++)
		{
			float distance = distances[WrapAround(i + j, 0, sampleCount - 1)];
			weight += WeightDistance(distance, wantedDistance) * ((halfSampleSize - IntAbs(j)) + 1);
			sum += distance;
		}
		weightedSamples[i] = sum / weight;
	}
}

// The preferred sample is not towards the way with seemingly most space
// It's towards the direction with acceptable space, also including the neighboring directions
// The "best" sample can be next to a terrible sample, blocking the teleport
static stock int FindPreferredSample(float wantedDistance, const float[] samples, int sampleCount)
{
	// This is used for backup, if there are not enough acceptable samples
	int bestSampleIndex = 0;
	float bestSampleValue = samples[0];

	bool[] acceptable = new bool[sampleCount];
	for (int i = 0; i < sampleCount; i++)
	{
		float sample = samples[i];
		if (sample >= wantedDistance)
		{
			acceptable[i] = true;
		}
		if (sample > bestSampleValue)
		{
			bestSampleIndex = i;
			bestSampleValue = sample;
		}
	}

	int maxIndex = sampleCount - 1;
	int bestStartIndex = -1;
	int bestLength = 0;

	int nextIndex = 0;
	int assessedCount = 0;

	while (assessedCount < sampleCount)
	{
		assessedCount++;
		if (acceptable[nextIndex])
		{
			int testIndex = nextIndex;
			int length = 1;

			if (nextIndex == 0)
			{
				while (assessedCount < sampleCount && acceptable[WrapAround(testIndex - 1, 0, maxIndex)])
				{
					length++;
					testIndex--;
					assessedCount++;
				}
			}

			while (assessedCount < sampleCount && acceptable[WrapAround(testIndex + length, 0, maxIndex)])
			{
				length++;
				assessedCount++;
			}

			// If nextIndex == 0, we've tested 2 indices
			// otherwise, 1 extra (or we've looped all'round, so no matter)
			assessedCount += (nextIndex == 0 ? 2 : 1);
			if (length > bestLength)
			{
				bestStartIndex = testIndex;
				bestLength = length;
			}

			nextIndex = WrapAround(testIndex + length + 1, 0, maxIndex);
		}
		else
		{
			nextIndex++;
		}
	}

	// If the length is less than 3, you can hardly say there's a preferred direction, so just pick the best sample
	if (bestLength < 3)
	{
		return bestSampleIndex;
	}

	return WrapAround(bestStartIndex + ((bestLength - 1) >> 1), 0, maxIndex);
}

static stock int IntAbs(int value)
{
	return value < 0 ? value * -1 : value;
}

static stock int WrapAround(int value, int min, int max)
{
	int range = (max - min) + 1;
	value = (value - min) % range;
	if (value < 0)
	{
		value += range;
	}
	value += min;
	return value;
}

static stock float CombineWithOppositeSample(const float[] samples, int length, int index)
{
	int half = length >> 1;

	float total = samples[index];
	total += samples[WrapAround(index + half, 0, length - 1)];

	return total;
}

static stock int WeightDistance(float distance, float wantedDistance)
{
	int weight = 1;
	if (wantedDistance > 0 && distance < wantedDistance)
	{
		// Just some rough weighting, if we're too close too our min wanted distance
		// We should it weight ti way higher than if we're further away
		float value = ((wantedDistance - distance) / wantedDistance) * 100.0;
		if (value > 30.0)
		{
			weight = RoundToCeil(value * 2);
		}
		else
		{
			weight = RoundToCeil(value);
		}
	}
	return weight < 1 ? 1 : weight;
}

static stock bool DefaultAdjustTeleportTraceFilter(int entity, int contentsMask, any data)
{
	#pragma unused contentsMask
	if (data != -1 && entity == data)
	{
		if (entity == data ||
			(entity > 0 && entity <= MaxClients &&
			data > 0 && data <= MaxClients &&
			GetClientTeam(entity) == GetClientTeam(data))
		)
		{
			return false;
		}
	}
	return true;
}

static stock void CopyVector(const float vec1[3], float vec2[3])
{
	vec2[0] = vec1[0];
	vec2[1] = vec1[1];
	vec2[2] = vec1[2];
}

static stock void TraceRay(const float pos[3], const float vec[3], RayType rtype)
{
	if (g_traceFilter == INVALID_FUNCTION)
	{
		TR_TraceRay(pos, vec, g_iTraceFlags, rtype);
	}
	else
	{
		TR_TraceRayFilter(pos, vec, g_iTraceFlags, rtype, g_traceFilter, g_traceData);
	}
}

static stock void TraceHull(const float pos[3], const float vec[3], const float mins[3], const float maxs[3])
{
	if (g_traceFilter == INVALID_FUNCTION)
	{
		TR_TraceHull(pos, vec, mins, maxs, g_iTraceFlags);
	}
	else
	{
		TR_TraceHullFilter(pos, vec, mins, maxs, g_iTraceFlags, g_traceFilter, g_traceData);
	}
}

#if defined TELEPORT_DEBUG
static stock void DrawLine(const float pos1[3], const float pos2[3], const int color[4])
{
	static int BeamSprite = -1;
	if (BeamSprite == -1) BeamSprite = PrecacheModel("materials/sprites/laser.vmt");

	TE_SetupBeamPoints(pos1, pos2, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0); TE_SendToAll();
}

static stock void DrawBBoxBottom(const float pos[3], const float mins[3], const float maxs[3], const int color[4])
{
	static int BeamSprite = -1;
	if (BeamSprite == -1) BeamSprite = PrecacheModel("materials/sprites/laser.vmt");

	float tl[3];
	float tr[3];
	float bl[3];
	float br[3];

	tl[0] = pos[0] + mins[0];
	tl[1] = pos[1] + mins[1];
	tl[2] = pos[2];
	
	tr[0] = pos[0] + maxs[0];
	tr[1] = pos[1] + mins[1];
	tr[2] = pos[2];
	
	bl[0] = pos[0] + mins[0];
	bl[1] = pos[1] + maxs[1];
	bl[2] = pos[2];
	
	br[0] = pos[0] + maxs[0];
	br[1] = pos[1] + maxs[1];
	br[2] = pos[2];

	TE_SetupBeamPoints(tl, tr, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0); TE_SendToAll();
	TE_SetupBeamPoints(tr, br, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0); TE_SendToAll();
	TE_SetupBeamPoints(br, bl, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0); TE_SendToAll();
	TE_SetupBeamPoints(bl, tl, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0); TE_SendToAll();
}

static stock void DrawMarker(const float pos[3], const int color[4], float distance = 50.0)
{
	static int BeamSprite = -1;
	if (BeamSprite == -1) BeamSprite = PrecacheModel("materials/sprites/laser.vmt");

	float up[3];
	CopyVector(pos, up);
	up[2] += distance;

	TE_SetupBeamPoints(pos, up, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0);
	TE_SendToAll();
}

static stock void DrawLaserFrom(const float pos[3], float angle, const int color[4], float distance = 100.0)
{
	static int BeamSprite = -1;
	if (BeamSprite == -1) BeamSprite = PrecacheModel("materials/sprites/laser.vmt");

	float fwd[3];
	float angles[3] = {0.0, ...};
	angles[1] = angle < 360.0 ? angle + 360.0 : angle > 360.0 ? angle + 360.0 : angle;
	GetAngleVectors(angles, fwd, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(fwd, distance);
	AddVectors(pos, fwd, fwd);

	TE_SetupBeamPoints(pos, fwd, BeamSprite, 0, 0, 0, 10.0, 5.0, 5.0, 1, 0.0, color, 0);
	TE_SendToAll();
}
#else
// provide these when not debugging, similar, but different
static stock void DrawLine(const float pos1[3], const float pos2[3], const int color[4])
{
}

static stock void DrawBBoxBottom(const float pos[3], const float mins[3], const float maxs[3], const int color[4])
{
}

static stock void DrawMarker(const float pos[3], const int color[4], float distance = 50.0)
{
}

static stock void DrawLaserFrom(const float pos[3], float angle, const int color[4], float distance = 100.0)
{
}
#endif