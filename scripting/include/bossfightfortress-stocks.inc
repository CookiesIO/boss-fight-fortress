#if defined BOSSFIGHTFORTRESS_STOCKS
	#endinput
#endif
#define BOSSFIGHTFORTRESS_STOCKS

#include <bossfightfortress-stocks/teleport>
#include <bossfightfortress-stocks/particles>

stock void BFF_SetParent(int entity, int parent)
{
	SetVariantString("!activator");
	AcceptEntityInput(entity, "SetParent", parent, entity);
}

stock void BFF_KillEntityIn(int entity, float seconds)
{
	char output[32];
	Format(output, sizeof(output), "OnUser1 !self,Kill,,%0.2f,1", seconds);
	SetVariantString(output);
	AcceptEntityInput(entity, "AddOutput");
	AcceptEntityInput(entity, "FireUser1");
}

#if defined _tf2items_included
stock Handle BFF_CreateItem(char[] classname, int itemIndex, const char[] attributes, TFQuality quality=TFQuality_Normal, int level=42, bool forceGeneration=false)
{
	Handle item = TF2Items_CreateItem(OVERRIDE_ALL | (forceGeneration ? FORCE_GENERATION : 0));

	TF2Items_SetClassname(item, classname);
	TF2Items_SetItemIndex(item, itemIndex);
	TF2Items_SetQuality(item, view_as<int>(quality));
	TF2Items_SetLevel(item, level);

	char attributeParts[32][16];
	int attributeCount = ExplodeString(attributes, ";", attributeParts, sizeof(attributeParts), sizeof(attributeParts[]));
	attributeCount = (attributeCount & ~1) / 2;

	TF2Items_SetNumAttributes(item, attributeCount);
	for (int i = 0; i < attributeCount; i++)
	{
		int attribute = StringToInt(attributeParts[i * 2]);
		if (attribute <= 0)
		{
			ThrowError("Invalid attribute index (%s)", attributeParts[i * 2]);
			return null;
		}
		TF2Items_SetAttribute(item, i, attribute, StringToFloat(attributeParts[i * 2 + 1]));
	}

	return item;
}
#endif