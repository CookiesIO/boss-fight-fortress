#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <tf2_stocks>
#include <gamma>
#include <definitions>
#include <bossfightfortress-base>

/*********************************************************************
 *
 *  This version of Boss Fight Fortress is a remake of my old version,
 *  which was made for the old of Gamma. It's being upgraded to the
 *  new version of Gamma and will also be based on Definitions.
 *
 *  The new version of Gamma allows a use-case I highly wanted for the
 *  old version, which was specialization. The core-game mode already
 *  supports multiple bosses, so why limit to a single boss?
 *  By taking advantage of Definitions, the base game mode can now be
 *  a seperate plugin, and 2 more plugins can be made, one for the
 *  classic Boss vs All, and one for Bosses vs Bosses, or any other
 *  variation desired, that'll be using bosses.
 *
 *  On top of that, each game mode can, while still using the same
 *  base definition for bosses, use bosses only made available for
 *  their game mode. This is due to the reason that, when making
 *  a new variation, you can also make a new boss definition that
 *  should be used with your new game mode specifically. This
 *  prevents unexpected issues, by explicitly requiring support from
 *  the sub-plugin itself.
 *
 *  The old Gamma had something called "Behaviors", which are not
 *  present in the new one. The reason is, that if such functionality
 *  is required, it can easily be made as a third-party plugin.
 *  Also, because of Definitions, the need for having it in Gamma
 *  itself was drastically reduced, Behaviors was a simple way for
 *  game modes to define player altering effects, based on a type.
 *  In essense, it was like definitions, but more crude due to the
 *  need of Behavior Types.
 *  Definitions is much more flexible in that regard, and also more
 *  streamlined, as well as being more generalized, so you aren't
 *  limited to do only one specific thing.
 *
 * *******************************************************************
 *
 *  I hope that people will embrace the way of thinking poured into
 *  this project, to stay modular and flexible, so that all servers
 *  can offer an as unique experience as they want.
 *
 *  Nothing prevents anyone from coding in restrictions directly, but
 *  for the sake of everyones best interests, provide it in external
 *  plugins, in such a way that it could work for any game mode, a
 *  good example: Weapon Restriction. 
 *
 *  I've seen these hardcoded into VSH/FF2 and it makes me cringe,
 *  these are not neccesarily required by every server and would
 *  benefit a lot if they were moved to another plugin, and made
 *  config based. Nothing stops you from distributing default configs,
 *  but at least this way people can change them as they wish.
 *  And it could even offer per game mode restrictions.
 *
 * *** **** Definitions ***** Gamma ***** Boss Fight Fortress **** ***
 *
 *********************************************************************/

// Our definitions, the game mode and the boss base
#pragma unused g_hBffGameMode
GameMode g_hBffGameMode;
Definition g_hBossDefinition;

GameMode g_hCurrentBossGameMode;

BffRoundState g_eRoundState;
Handle g_hStatusHudSynchronizer;

Handle g_hOnRoundStateChange;

// Accuracy of charge percentage to be shown in hud, should be one of the following:
// 1, 2, 5, 10, 20, 25, 50
#define CHARGE_ACCURACY 5

#include "bossfightfortress/definitions/special-ability-definition.sp"
#include "bossfightfortress/definitions/charge-ability-definition.sp"
#include "bossfightfortress/definitions/phase-ability-definition.sp"

#include "bossfightfortress/bffbaseclient.sp"

#include "bossfightfortress/definitions/boss-definition.sp"
#include "bossfightfortress/entity-management.sp"
#include "bossfightfortress/file-management.sp"
#include "bossfightfortress/endangerment.sp"
#include "bossfightfortress/soundhook.sp"

#define PLUGIN_VERSION "2.0"

public Plugin myinfo = 
{
	name = "Boss Fight Fortress - Base",
	author = "Cookies.io",
	description = "The base game mode for boss-style game modes, doesn't host any version of the game mode itself.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public APLRes AskPluginLoad2()
{
	CreateNative("BFF_GetRoundState", Native_GetRoundState);
	CreateNative("BFF_SetRoundActive", Native_SetRoundActive);
	CreateNative("BFF_SetRoundFinished", Native_SetRoundFinished);

	CreateNative("BFF_AddBossFilesToDownloadsTable", Native_AddBossFilesToDownloadsTable);
	CreateNative("BFF_AddBossChildrensFilesToDownloadsTable", Native_AddBossChildrensFilesToDownloadsTable);

	CreateNative("BffClient.IsBoss.get", Native_GetClientIsBoss);
	CreateNative("BffClient.Boss.get", Native_GetClientBoss);
	CreateNative("BffClient.Boss.set", Native_SetClientBoss);
	CreateNative("BffClient.Phase.get", Native_GetClientPhase);
	CreateNative("BffClient.ClearBoss", Native_ClearClientBoss);
	CreateNative("BffClient.BossClass.get", Native_GetClientBossClass);
	CreateNative("BffClient.IsEndangered.get", Native_GetClientIsEndangered);
	CreateNative("BffClient.PrimaryChargePercent.get", Native_GetClientPrimaryChargePercent);
	CreateNative("BffClient.SecondaryChargePercent.get", Native_GetClientSecondaryChargePercent);
	CreateNative("BffClient.SpecialChargePercent.get", Native_GetClientSpecialChargePercent);
	CreateNative("BffClient.CustomSpecialChargePercent.get", Native_GetCustomSpecialCharge);
	CreateNative("BffClient.CustomSpecialChargePercent.set", Native_SetCustomSpecialCharge);

	CreateNative("BffClient.SetHealthMultiplier", Native_ClientSetHealthMultiplier);
	CreateNative("BffClient.UpdateHud", Native_ClientUpdateHud);
	CreateNative("BffClient.SetPrimaryChargeTime", Native_ClientSetPrimaryChargeTime);
	CreateNative("BffClient.SetPrimaryCooldownTime", Native_ClientSetPrimaryCooldownTime);
	CreateNative("BffClient.SetPrimaryCooldownPercentage", Native_ClientSetPrimaryCooldownPercentage);
	CreateNative("BffClient.SetSecondaryChargeTime", Native_ClientSetSecondaryChargeTime);
	CreateNative("BffClient.SetSecondaryCooldownTime", Native_ClientSetSecondaryCooldownTime);
	CreateNative("BffClient.SetSecondaryCooldownPercentage", Native_ClientSetSecondaryCooldownPercentage);
	CreateNative("BffClient.SetSpecialCooldown", Native_ClientSetSpecialCooldown);
	CreateNative("BffClient.RemoveAllWearables", Native_ClientRemoveAllWearables);
	CreateNative("BffClient.PlaySound", Native_ClientPlaySound);

	g_hOnRoundStateChange = CreateGlobalForward("BFF_OnRoundStateChange", ET_Ignore, Param_Cell);

	RegPluginLibrary("bossfightfortress");
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_base_version", PLUGIN_VERSION, "Boss Fight Fortress - Base Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);

	RegAdminCmd("bossfightfortress_base_reload_configs", Cmd_ReloadConfigs, ADMFLAG_ROOT, "Reloads all Boss Fight Fortress - Base configs");

	g_hStatusHudSynchronizer = CreateHudSynchronizer();
	LoadConfigs();
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void OnAllPluginsLoaded()
{
	CreateSpecialAbilityDefinition();
	CreateChargeAbilityDefinition();
	CreatePhaseAbilityDefinition();
	CreateBossDefinition();

	AddDefinitionCreatedListener(GAMMA_GAMEMODE_DEFINITION, OnGameModeDefinitionCreated);
	AddDefinitionDestroyedListener(BFF_GAMEMODE_BASE, OnBFFGameModeDestroyed);
}

void OnGameModeDefinitionCreated()
{
	GameModeCreator creator = new GameModeCreator(BFF_GAMEMODE_BASE, BffStart, BffEnded);

	creator.DefineFunction("BffStart");
	creator.DefineFunction("BffEnded");
	creator.DefineFunction("OnRoundActive");
	creator.SetBool("AllowChangeClass", false);
	creator.SetBool("AllowGlobalSoundEmit", false);
	creator.SetFunction("TeleportAwayFromDanger", TeleportAwayFromDanger);

	// The following are optionally set, and are not provided a default
	// I write them with creator.Define*(); to show how they would've been defined
	// creator.DefineFunction("PlayingSound"); 

	g_hBffGameMode = creator.Finalize();
}

void OnBFFGameModeDestroyed()
{
	g_hBffGameMode = null;
}

public void OnMapStart()
{
	EndangermentMapChange();
}

public Action Cmd_ReloadConfigs(int client, int args)
{
	LoadConfigs();
	ReplyToCommand(client, "Configs reloaded");
}

void LoadConfigs()
{
	LoadEndangermentConfig();
}

/******************************************
 *
 *  Default boss-base definition functions
 *  Placed here, because of methodmaps they don't work in boss-definition.sp
 *  That's what happens when things gets more complicated than something top-down can handle :(
 *
 ******************************************/
public void DefaultFormatName(BffBaseClient client, char[] buffer, int bufferSize)
{
	client.Boss.GetString("DisplayName", buffer, bufferSize);
}

public void DefaultFormatSpecialHud(BffBaseClient client, char[] buffer, int bufferSize, AbilityState state, int charge)
{
	// well, poop ... This is the price we have to pay for carry-over phases
	char property[64];
	for (int i = client.Phase; i >= 0; i--)
	{
		Format(property, sizeof(property), "SpecialAbilityName<%d>", i);
		if (i == 0) 
		{
			client.Boss.GetStringEx(property, buffer, bufferSize, "Rage");
		}
		else if (client.Boss.GetString(property, buffer, bufferSize))
		{
			break;
		}
	}
	switch (state)
	{
		case AbilityState_Ready:
		{
			Format(buffer, bufferSize, "%s is ready", buffer);
		}
		case AbilityState_Charging:
		{
			Format(buffer, bufferSize, "%s is %d%% charged", buffer, charge);
		}
	}
}

static void FormatChargeHud(BffBaseClient client, char[] buffer, int bufferSize, const char[] type, ChargeAbility ability, AbilityState state, int charge)
{
	// well, poop ... This is the price we have to pay for carry-over phases
	char property[64];
	for (int i = client.Phase; i >= 0; i--)
	{
		Format(property, sizeof(property), "%sAbilityName<%d>", type, i);
		if (i == 0) 
		{
			client.Boss.GetStringEx(property, buffer, bufferSize, "Charge ability");
		}
		else if (client.Boss.GetString(property, buffer, bufferSize))
		{
			break;
		}
	}
	if (ability.ChargeMode == ChargeMode_Normal)
	{
		switch (state)
		{
			case AbilityState_Ready:
			{
				Format(buffer, bufferSize, "%s is ready", buffer);
			}
			case AbilityState_Charging:
			{
				Format(buffer, bufferSize, "%s is %d%% charged", buffer, charge);
			}
			case AbilityState_OnCooldown:
			{
				Format(buffer, bufferSize, "%s is %d%% recharged", buffer, charge);
			}
		}
	}
	else
	{
		// I guess there's not really much of a concept of recharge and ready for Continuous abilities
		Format(buffer, bufferSize, "%s has %d%% charge", buffer, charge);
	}
}

public void DefaultPrimaryFormatChargeHud(BffBaseClient client, char[] buffer, int bufferSize, AbilityState state, int charge)
{
	FormatChargeHud(client, buffer, bufferSize, "Primary", client.PrimaryAbility, state, charge);
}

public void DefaultSecondaryFormatChargeHud(BffBaseClient client, char[] buffer, int bufferSize, AbilityState state, int charge)
{
	FormatChargeHud(client, buffer, bufferSize, "Secondary", client.SecondaryAbility, state, charge);
}

public bool DefaultOnUseSpecialAbility(BffBaseClient client, float chargePercentage)
{
	// well, poop ... This is the price we have to pay for carry-over phases
	char property[64];
	float requiredCharge = 1.0;
	for (int i = client.Phase; i >= 0; i--)
	{
		Format(property, sizeof(property), "DefaultOnUseSpecialAbilityMinimumCharge<%d>", i);
		if (i == 0) 
		{
			requiredCharge = client.Boss.GetFloatEx(property, 1.0);
		}
		else if (client.Boss.GetFloat(property, requiredCharge))
		{
			break;
		}
	}
	return chargePercentage >= requiredCharge;
}

/******************************************
 *
 *  Game mode start/ended listeners
 *
 ******************************************/

void SetRoundState(BffRoundState state)
{
	g_eRoundState = state;

	Call_StartForward(g_hOnRoundStateChange);
	Call_PushCell(state);
	Call_Finish();
}

void SetRoundActive()
{
	if (g_eRoundState == BffRoundState_Preround)
	{
		g_hCurrentBossGameMode.StartFunction("OnRoundActive");
		Call_Finish();

		SetRoundState(BffRoundState_Active);

		for (int client = 1; client <= MaxClients; client++)
		{
			BffBaseClient bffClient = BffBaseClient(client);
			bffClient.RoundActive();
		}
	}
}

void SetRoundFinished()
{
	if (g_eRoundState == BffRoundState_Active)
	{
		SetRoundState(BffRoundState_Finished);
	}
}

public void BffStart()
{
	g_hCurrentBossGameMode = Gamma_GetCurrentGameMode();

	EnableSoundHook();
	HookEvent("post_inventory_application", Event_PostInventoryApplication);
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_hurt", Event_PlayerHurt);
	HookEvent("object_destroyed", Event_ObjectDestroyed);
	HookEvent("teamplay_round_win", Event_RoundWin, EventHookMode_Pre);
	HookMiscEntities();

	if (g_hCurrentBossGameMode.GetBoolEx("AllowChangeClass") == false)
	{
		AddCommandListener(Command_ChangeClass, "joinclass");
	}
	AddCommandListener(Command_VoiceMenu, "voicemenu");

	g_hCurrentBossGameMode.StartFunction("BffStart");
	Call_Finish();

	SetRoundState(BffRoundState_Preround);
}

public void BffEnded(GameModeEndReason reason)
{
	DisableSoundHook();
	UnhookEvent("post_inventory_application", Event_PostInventoryApplication);
	UnhookEvent("player_spawn", Event_PlayerSpawn);
	UnhookEvent("player_death", Event_PlayerDeath);
	UnhookEvent("player_hurt", Event_PlayerHurt);
	UnhookEvent("object_destroyed", Event_ObjectDestroyed);
	UnhookEvent("teamplay_round_win", Event_RoundWin, EventHookMode_Pre);
	UnhookMiscEntities();

	if (g_hCurrentBossGameMode.GetBoolEx("AllowChangeClass") == false)
	{
		RemoveCommandListener(Command_ChangeClass, "joinclass");
	}
	RemoveCommandListener(Command_VoiceMenu, "voicemenu");

	for (int client = 1; client <= MaxClients; client++)
	{
		BffBaseClient bffClient = BffBaseClient(client);
		bffClient.ClearBoss();
	}

	g_hCurrentBossGameMode.StartFunction("BffEnded");
	Call_PushCell(reason);
	Call_Finish();

	SetRoundState(BffRoundState_Invalid);
	g_hCurrentBossGameMode = null;
}

public void TeleportAwayFromDanger(BffBaseClient client)
{
	bool onlyTeamSpawn = g_hCurrentBossGameMode.GetBoolEx("OnlyTeleportToTeamSpawn");
	int team = view_as<int>(client.Team);

	int spawnPoint = -1;
	ArrayList spawnPoints = new ArrayList();
	while ((spawnPoint = FindEntityByClassname(spawnPoint, "info_player_teamspawn")) != -1)
	{
		if (onlyTeamSpawn)
		{
			if (GetEntProp(spawnPoint, Prop_Send, "m_iTeamNum") == team)
			{
				spawnPoints.Push(spawnPoint);
			}
		}
		else
		{
			spawnPoints.Push(spawnPoint);
		}
	}
	
	if (spawnPoints.Length == 0)
	{
		LogMessage("No spawnpoints found.");
	}
	else
	{
		float pos[3];
		float rot[3];	
		spawnPoint = spawnPoints.Get(GetRandomInt(0, spawnPoints.Length - 1));

		GetEntPropVector(spawnPoint, Prop_Send, "m_vecOrigin", pos);
		GetEntPropVector(spawnPoint, Prop_Send, "m_angRotation", rot);
		TeleportEntity(client.ClientIndex, pos, rot, NULL_VECTOR);
	}
	delete spawnPoints;
}

/******************************************
 *
 *  Event/command listeners
 *
 ******************************************/

public void Event_PostInventoryApplication(Handle event, const char[] name, bool dontBroadcast)
{
	BffBaseClient client = BffBaseClient(GetEventInt(event, "userid"), true);
	client.Regenerated();
}

public void Event_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	// The following comment is from my old BFF, which is why I'll keep the check here!
	// Did you know: When a player connects they "spawn" in team 0? I tell you what, I didn't! (at first)
	if (GetEventInt(event, "team") >= 2)
	{
		BffBaseClient client = BffBaseClient(GetEventInt(event, "userid"), true);
		client.Spawned();
	}
}

public void Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	if (g_eRoundState != BffRoundState_Preround)
	{
		BffBaseClient client = BffBaseClient(GetEventInt(event, "userid"), true);
		BffBaseClient attacker = BffBaseClient(GetEventInt(event, "attacker"), true);
		bool feignedDeath = (GetEventInt(event, "death_flags") & TF_DEATHFLAG_DEADRINGER) == TF_DEATHFLAG_DEADRINGER;

		client.Died(attacker, feignedDeath);
		if (attacker.IsValid)
		{
			attacker.Killed(client, feignedDeath);
		}
	}
}

public void Event_PlayerHurt(Handle event, const char[] name, bool dontBroadcast)
{
	BffBaseClient client = BffBaseClient(GetEventInt(event, "userid"), true);
	client.Hurt(GetEventInt(event, "damageamount"), GetEventInt(event, "custom"));
}

public void Event_ObjectDestroyed(Handle event, const char[] name, bool dontBroadcast)
{
	BffBaseClient attacker = BffBaseClient(GetEventInt(event, "attacker"), true);
	if (attacker.IsValid)
	{
		attacker.DestroyedBuilding();
	}
}

public void Event_RoundWin(Handle event, const char[] name, bool dontBroadcast)
{
	TFTeam team = view_as<TFTeam>(GetEventInt(event, "team"));
	for (int i = 1; i <= MaxClients; i++)
	{
		BffBaseClient client = BffBaseClient(i);
		if (client.IsValid)
		{
			if (client.Team == team)
			{
				client.Won();
			}
			else
			{
				client.Lost();
			}
		}
	}
}

public Action Command_ChangeClass(int clientIndex, const char[] command, int argc)
{
	BffBaseClient client = BffBaseClient(clientIndex);
	if (argc >= 1 && client.IsBoss && g_eRoundState != BffRoundState_Finished)
	{
		char classname[16];
		GetCmdArg(1, classname, sizeof(classname));

		TFClassType desiredClass = TF2_GetClass(classname);
		if (desiredClass != TFClass_Unknown)
		{
			SetEntProp(clientIndex, Prop_Send, "m_iDesiredPlayerClass", view_as<int>(desiredClass));
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action Command_VoiceMenu(int clientIndex, const char[] command, int argc)
{
	BffBaseClient client = BffBaseClient(clientIndex);
	if (client.IsBoss == false || argc < 2 || client.IsSpecialAbilityUsable == false)
	{
		return Plugin_Continue;
	}

	char arg1[8];
	char arg2[8];

	GetCmdArg(1, arg1, sizeof(arg1));
	GetCmdArg(2, arg2, sizeof(arg2));

	if (StringToInt(arg1) == 0 && StringToInt(arg2) == 0)
	{
		client.SpecialAbility.Use();
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

/******************************************
 *
 *  Delayed actions
 *
 ******************************************/

public void DelayedRevertClass(any userid)
{
	BffBaseClient client = BffBaseClient(userid, true);
	if (client.IsValid)
	{
		client.BffClient.SetTemporaryClass(client.BossClass);
	}
}

/******************************************
 *
 *  Natives
 *
 ******************************************/

bool CheckForNativeErrors(Handle plugin=null, const char[] message="")
{
	if (g_hCurrentBossGameMode.IsValid() == false)
	{
		ThrowNativeError(SP_ERROR_NATIVE, "BFF is currently not active.");
		return false;
	}

	if (plugin != null && g_hCurrentBossGameMode.IsFromPlugin(plugin) == false)
	{
		ThrowNativeError(SP_ERROR_NATIVE, message);
		return false;
	}
	return true;
}


public int Native_GetRoundState(Handle plugin, int numParams)
{
	return view_as<int>(g_eRoundState);
}

public int Native_SetRoundActive(Handle plugin, int numParams)
{
	if (CheckForNativeErrors(plugin, "Only the current game mode can set round the round active."))
	{
		SetRoundActive();
	}
	return 0;
}

public int Native_SetRoundFinished(Handle plugin, int numParams)
{
	if (CheckForNativeErrors(plugin, "Only the current game mode can set round the round finished."))
	{
		SetRoundFinished();
	}
	return 0;
}

// might have to do something to add error checking here, wouldn't want any plugin to just add a ton'o'bosses to the downloads table
public int Native_AddBossFilesToDownloadsTable(Handle plugin, int numParams)
{
	Definition boss = GetNativeCell(1);
	AddBossFilesToDownloadsTable(boss);
	return 0;
}

public int Native_AddBossChildrensFilesToDownloadsTable(Handle plugin, int numParams)
{
	Definition boss = GetNativeCell(1);
	ArrayList children = boss.GetChildren();
	int length = children.Length;
	for (int i = 0; i < length; i++)
	{
		Definition child = children.Get(i);
		AddBossFilesToDownloadsTable(child);
	}
	return 0;
}


public int Native_GetClientIsBoss(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}

		return view_as<int>(client.IsBoss);
	}
	return 0;
}

public int Native_GetClientBoss(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}

		return view_as<int>(client.Boss);
	}
	return 0;
}

public int Native_SetClientBoss(Handle plugin, int numParams)
{
	if (CheckForNativeErrors(plugin, "Only the current game mode can set the boss of a client."))
	{
		BffBaseClient client = GetNativeCell(1);
		Definition definition = GetNativeCell(2);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (definition.IsValidEx(false, g_hBossDefinition) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Not a valid, non-abstract boss definition.");
			return 0;
		}

		client.SetBoss(definition);
	}
	return 0;
}

public int Native_GetClientPhase(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}

		return client.Phase;
	}
	return 0;
}

public int Native_ClearClientBoss(Handle plugin, int numParams)
{
	if (CheckForNativeErrors(plugin, "Only the current game mode can set the boss of a client."))
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client (%d) is not a boss.", client.ClientIndex);
			return 0;
		}

		client.ClearBoss();
	}
	return 0;
}

public int Native_GetClientIsEndangered(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client (%d) is not a boss.", client.ClientIndex);
			return 0;
		}

		return view_as<int>(client.IsEndangered);
	}
	return 0;
}

public int Native_GetClientBossClass(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		return view_as<int>(client.BossClass);
	}
	return 0;
}

public int Native_GetClientPrimaryChargePercent(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		if (client.IsPrimaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable primary ability.");
			return 0;
		}

		return view_as<int>(client.PrimaryAbility.GetChargePercent());
	}
	return 0;
}

public int Native_GetClientSecondaryChargePercent(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		if (client.IsSecondaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable secondary ability.");
			return 0;
		}

		return view_as<int>(client.SecondaryAbility.GetChargePercent());
	}
	return 0;
}

public int Native_GetClientSpecialChargePercent(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		if (client.IsSpecialAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable special ability.");
			return 0;
		}

		return view_as<int>(client.SpecialAbility.GetChargePercent());
	}
	return 0;
}

public int Native_GetCustomSpecialCharge(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		if (client.IsSpecialAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable special ability.");
			return 0;
		}

		if (client.SpecialAbility.UseCustomCharge)
		{
			return view_as<int>(client.SpecialAbility.CustomChargePercent);
		}
	}
	return view_as<int>(0.0);
}

public int Native_SetCustomSpecialCharge(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.SpecialAbility.UseCustomCharge == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "The boss does not use Custom Special Charge.");
			return 0;
		}

		client.SpecialAbility.CustomChargePercent = GetNativeCell(2);
	}
	return 0;
}

public int Native_ClientSetHealthMultiplier(Handle plugin, int numParams)
{
	if (CheckForNativeErrors(plugin, "Only the owning game mode set the health multiplier."))
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss.");
			return 0;
		}

		client.SetHealthMultiplier(GetNativeCell(2));
	}
	return 0;
}

public int Native_ClientUpdateHud(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		client.QueueUpdateHud();
	}
	return 0;
}

public int Native_ClientSetPrimaryChargeTime(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsPrimaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable primary ability.");
			return 0;
		}

		client.PrimaryAbility.SetChargeTime(GetNativeCell(2), GetNativeCell(3));
	}
	return 0;
}

public int Native_ClientSetPrimaryCooldownTime(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsPrimaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable primary ability.");
			return 0;
		}

		client.PrimaryAbility.SetChargeCooldownTime(GetNativeCell(2), GetNativeCell(3));
	}
	return 0;
}

public int Native_ClientSetPrimaryCooldownPercentage(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsPrimaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "The boss does not have a usable primary ability.");
			return 0;
		}

		client.PrimaryAbility.SetChargeCooldownPercentage(GetNativeCell(2));
	}
	return 0;
}

public int Native_ClientSetSecondaryChargeTime(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsSecondaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable secondary ability.");
			return 0;
		}

		client.SecondaryAbility.SetChargeTime(GetNativeCell(2), GetNativeCell(3));
	}
	return 0;
}

public int Native_ClientSetSecondaryCooldownTime(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsSecondaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable secondary ability.");
			return 0;
		}

		client.SecondaryAbility.SetChargeCooldownTime(GetNativeCell(2), GetNativeCell(3));
	}
	return 0;
}

public int Native_ClientSetSecondaryCooldownPercentage(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsSecondaryAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "The boss does not have a usable secondary ability.");
			return 0;
		}

		client.SecondaryAbility.SetChargeCooldownPercentage(GetNativeCell(2));
	}
	return 0;
}

public int Native_ClientSetSpecialCooldown(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}
		if (client.IsBoss == false || client.Boss.IsFromPlugin(plugin) == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client is not a boss, or the calling plugin is not the owning plugin.");
			return 0;
		}

		if (client.IsSpecialAbilityUsable == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Client does not have a usable special ability.");
			return 0;
		}

		int damage = GetNativeCell(2);
		float time = GetNativeCell(3);
		bool recall = GetNativeCell(4);
		bool custom = GetNativeCell(5);

		client.SpecialAbility.SetSpecialCooldown(damage, time, recall, custom);
	}
	return 0;
}

public int Native_ClientRemoveAllWearables(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}

		client.RemoveAllWearables();
	}
	return 0;
}

public int Native_ClientPlaySound(Handle plugin, int numParams)
{
	if (CheckForNativeErrors())
	{
		BffBaseClient client = GetNativeCell(1);

		if (client.IsValid == false)
		{
			ThrowNativeError(SP_ERROR_PARAM, "Invalid client.");
			return 0;
		}

		int soundLength;
		GetNativeStringLength(2, soundLength);
		char[] sound = new char[soundLength + 1];
		GetNativeString(2, sound, soundLength + 1);

		bool isGroup = GetNativeCell(3);
		bool emitGlobally = GetNativeCell(3);

		return client.PlaySound(sound, isGroup, emitGlobally);
	}
	return 0;
}