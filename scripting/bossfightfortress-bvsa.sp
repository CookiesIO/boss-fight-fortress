#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <tf2>
#include <tf2_stocks>
#include <gamma>
#include <definitions>
#include <bossfightfortress-base>

bool g_bIsActive;
BffClient g_iCurrentBoss;

Definition g_hGenericBoss;
ArrayList g_hAvailableBosses;

int g_iPreviousArenaUseQueue;
int g_iPreviousArenaFirstBlood;
int g_iPreviousTeamsUnbalanceLimit;
int g_iPreviousForceCamera;

#define PLUGIN_VERSION "2.0"

public Plugin myinfo = 
{
	name = "Boss Fight Fortress - BvsA",
	author = "Cookies.io",
	description = "Boss vs All game mode for Boss Fight Fortress.",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net"
};

public void OnPluginStart()
{
	CreateConVar("bossfightfortress_bvsa_version", PLUGIN_VERSION, "Boss Fight Fortress - BvsA Version", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_CHEAT);
}

public void OnAllPluginsLoaded()
{
	AddDefinitionCreatedListener(BFF_BOSS_GENERIC, OnGenericBossCreated);
	AddDefinitionCreatedListener(BFF_GAMEMODE_BASE, OnGameModeBaseCreated);
}

public void OnPluginEnd()
{
	DestroyMyDefinitions();
}

public void OnGameModeBaseCreated()
{
	BffGameModeCreator creator = new BffGameModeCreator("BFF-BvsA", BffStart, BffEnded, BffOnRoundActive);

	creator.SetBool("AllowGlobalSoundEmit", true);
	creator.SetWorksInMap(WorksInMap);
	creator.SetSelectedForMap(SelectedForMap);
	creator.SetCanStart(CanStart);

	creator.Finalize();
}

public void OnGenericBossCreated(Definition definition)
{
	g_hGenericBoss = definition;
}

public void OnDefinitionDestroyed(Definition definition)
{
	if (definition.IsDefinition(BFF_BOSS_GENERIC))
	{
		g_hGenericBoss = null;
	}

	// gotta remove a boss from the available bosses if it's removed
	// wouldn't want to start a boss that we don't even know is ready, file wise, or not
	if (g_hAvailableBosses != null)
	{
		int index = g_hAvailableBosses.FindValue(definition);
		if (index != -1)
		{
			g_hAvailableBosses.Erase(index);
		}
	}

	if (g_bIsActive)
	{
		// Not good ...
		if (g_iCurrentBoss.Boss == definition)
		{
			switch (BFF_GetRoundState())
			{
				case BffRoundState_Preround:
				{
					if (GiveRandomBoss(g_iCurrentBoss) == false)
					{
						Gamma_ForceEndGameMode();
					}
				}
				case BffRoundState_Active:
				{
					Gamma_ForceEndGameMode();
				}
				default:
				{
					g_iCurrentBoss.ClearBoss();
				}
			}
		}
	}
}

public void OnClientDisconnect(int clientIndex)
{
	if (g_bIsActive)
	{
		BffClient client = BffClient(clientIndex);
		if (client.IsBoss)
		{
			// Well, poop. Should probably do something here. If applicable.
			client.ClearBoss();
			Gamma_ForceEndGameMode();
		}
	}
}

public bool WorksInMap(const char[] map)
{
	if (FindEntityByClassname(-1, "tf_logic_arena") != -1)
	{
		if (g_hGenericBoss.IsValid() == false)
		{
			LogError("[BFF BVSA] "...BFF_BOSS_GENERIC..." is somehow not valid. Game mode not available for this map, even though it should've been.");
			return false;
		}
		return true;
	}
	return false;
}

public void SelectedForMap()
{
	// Save the available bosses here, since these are the only ones we can be sure have their custom files downloaded
	if (g_hAvailableBosses != null)
	{
		delete g_hAvailableBosses;
	}
	g_hAvailableBosses = g_hGenericBoss.GetChildren();

	ConVar arenaUseQueue = FindConVar("tf_arena_use_queue");
	ConVar arenaFirstBlood = FindConVar("tf_arena_first_blood");
	ConVar teamsUnbalanceLimit = FindConVar("mp_teams_unbalance_limit");
	ConVar forceCamera = FindConVar("mp_forcecamera");

	g_iPreviousArenaUseQueue = arenaUseQueue.IntValue;
	g_iPreviousArenaFirstBlood = arenaFirstBlood.IntValue;
	g_iPreviousTeamsUnbalanceLimit = teamsUnbalanceLimit.IntValue;
	g_iPreviousForceCamera = forceCamera.IntValue;
	
	arenaUseQueue.IntValue = 0;
	arenaFirstBlood.IntValue = 0;
	teamsUnbalanceLimit.IntValue = 0;
	forceCamera.IntValue = 0;

	BFF_AddBossChildrensFilesToDownloadsTable(g_hGenericBoss);
}

public void OnMapEnd()
{
	ConVar arenaUseQueue = FindConVar("tf_arena_use_queue");
	ConVar arenaFirstBlood = FindConVar("tf_arena_first_blood");
	ConVar teamsUnbalanceLimit = FindConVar("mp_teams_unbalance_limit");
	ConVar forceCamera = FindConVar("mp_forcecamera");

	arenaUseQueue.IntValue = g_iPreviousArenaUseQueue;
	arenaFirstBlood.IntValue = g_iPreviousArenaFirstBlood;
	teamsUnbalanceLimit.IntValue = g_iPreviousTeamsUnbalanceLimit;
	forceCamera.IntValue = g_iPreviousForceCamera;
}

public bool CanStart()
{
	bool result = g_hGenericBoss.IsValid();
	if (result)
	{
		result = g_hAvailableBosses.Length > 0;
	}

	if (result)
	{
		int playerCount = GetTeamClientCount(2) + GetTeamClientCount(3);
		result = playerCount >= 2;
	}

	if (result)
	{
		int nextBoss = GetNextInQueue();
		result = nextBoss != -1;
	}
	return result;
}

public void BffStart()
{
	HookEvent("arena_round_start", Event_ArenaRoundStart);
	HookEvent("teamplay_round_win", Event_RoundWin);
	HookEvent("player_spawn", Event_PlayerSpawn);

	AddCommandListener(Command_Kill, "kill");
	AddCommandListener(Command_Kill, "explode");
	AddCommandListener(Command_JoinTeam, "jointeam");

	int nextBoss = GetNextInQueue();
	g_iCurrentBoss = BffClient(nextBoss);
	GiveRandomBoss(g_iCurrentBoss);

	for (int i = 1; i <= MaxClients; i++)
	{
		BffClient client = BffClient(i);
		if (client.IsValid && client.Team >= view_as<TFTeam>(2))
		{
			if (client.IsBoss)
			{
				client.DeathlessChangeTeam(TFTeam_Blue);
				client.Respawn();
			}
			else if (client.Team == TFTeam_Blue)
			{
				client.DeathlessChangeTeam(TFTeam_Red);
				client.Respawn();
			}
		}
	}

	UpdateBossHealthMultiplier();
	g_bIsActive = true;
}

public void BffOnRoundActive()
{
	UpdateBossHealthMultiplier();
}

public void BffEnded(GameModeEndReason reason)
{
	UnhookEvent("arena_round_start", Event_ArenaRoundStart);
	UnhookEvent("teamplay_round_win", Event_RoundWin);
	UnhookEvent("player_spawn", Event_PlayerSpawn);

	RemoveCommandListener(Command_Kill, "kill");
	RemoveCommandListener(Command_Kill, "explode");
	RemoveCommandListener(Command_JoinTeam, "jointeam");
	
	g_bIsActive = false;
}

public void Event_ArenaRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	BFF_SetRoundActive();
}

public void Event_RoundWin(Event event, const char[] name, bool dontBroadcast)
{
	BFF_SetRoundFinished();
}

public void Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	BffClient client = BffClient(event.GetInt("userid"), true);
	if (client.IsBoss == false && client.Team == TFTeam_Blue)
	{
		client.DeathlessChangeTeam(TFTeam_Red);
		client.Respawn();
	}
}

public Action Command_Kill(int client, const char[] command, int argc)
{
	if (BFF_GetRoundState() != BffRoundState_Finished)
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action Command_JoinTeam(int clientIndex, const char[] command, int argc)
{
	if (BFF_GetRoundState() != BffRoundState_Finished)
	{
		char team[16];
		GetCmdArg(1, team, sizeof(team));
		BffClient client = BffClient(clientIndex);
		bool handled = false;
		if (StrEqual(team, "spectator", false))
		{
			if (client.IsBoss == false)
			{
				return Plugin_Continue;
			}
		}
		else if (client.Team == TFTeam_Spectator)
		{
			client.Team = TFTeam_Red;
			handled = true;
		}
		if (!handled)
		{
			ReplyToCommand(clientIndex, "You cannot change team");
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

void UpdateBossHealthMultiplier()
{
	float multiplier = float(GetTeamClientCount(view_as<int>(TFTeam_Red)));

	for (int i = 1; i <= MaxClients; i++)
	{
		BffClient client = BffClient(i);
		if (client.IsValid && client.IsBoss)
		{
			client.SetHealthMultiplier(multiplier);
		}
	}
}


int GetNextInQueue()
{
	// It needs a proper implementation at one point
	// Likely through Definitions, because, you knew, we love, /love/, definitions.
	// ... I wonder when I'll be hated for pushing definitions so much!?
	for (int i = g_iCurrentBoss.Index + 1; i <= MaxClients; i++)
	{
		BffClient client = BffClient(i);
		if (client.IsValid && client.IsFakeClient == false && client.Team >= view_as<TFTeam>(2))
		{
			return i;
		}
	}
	for (int i = 1; i <= g_iCurrentBoss.Index; i++)
	{
		BffClient client = BffClient(i);
		if (client.IsValid && client.IsFakeClient == false && client.Team >= view_as<TFTeam>(2))
		{
			return i;
		}
	}
	return -1;
}

bool GiveRandomBoss(BffClient client)
{
	if (g_hAvailableBosses.Length > 0)
	{
		int definition = GetRandomInt(0, g_hAvailableBosses.Length - 1);
		client.Boss = view_as<Definition>(g_hAvailableBosses.Get(definition));
		return true;
	}
	return false;
}