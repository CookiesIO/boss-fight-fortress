#if defined _ENDANGERMENT_INCLUDED
  #endinput
#endif
#define _ENDANGERMENT_INCLUDED

static const float DefaultEndangerDamage = 150.0;
static const int DefaultForceTeleport = 1;

static KeyValues g_hConfig;
static bool g_bHasEntitiesConfig;
static float g_fDefaultEndangerDamage;
static int g_iDefaultForceTeleport;
static float g_fCurrentEndangerDamage;
static int g_iCurrentForceTeleport;

static char g_sCurrentMap[128];

void LoadEndangermentConfig()
{
	delete g_hConfig;
	g_hConfig = new KeyValues("Endangerment");

	char config[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, config, sizeof(config), "%s/endangerment.cfg", BFF_CONFIG_DIRECTORY);
	g_hConfig.ImportFromFile(config);

	LoadDefaults();
	EndangermentMapChange();
}

void EndangermentMapChange()
{
	g_hConfig.Rewind();
	GetCurrentMap(g_sCurrentMap, sizeof(g_sCurrentMap));
	if (g_hConfig.JumpToKey("maps") && g_hConfig.JumpToKey(g_sCurrentMap))
	{
		g_fCurrentEndangerDamage = g_hConfig.GetFloat("damage", g_fDefaultEndangerDamage);
		g_iCurrentForceTeleport = g_hConfig.GetNum("force teleport", g_iDefaultForceTeleport);

		g_bHasEntitiesConfig = g_hConfig.JumpToKey("entities");
	}
	else
	{
		g_bHasEntitiesConfig = false;
	}
}

bool IsTriggerDangerous(int trigger, float damage, bool &forceTeleport)
{
	float endangerDamage = g_fCurrentEndangerDamage;
	int endangerForceTeleport = g_iCurrentForceTeleport;
	
	if (g_bHasEntitiesConfig)
	{
		char entityName[64];
		GetEntPropString(trigger, Prop_Data, "m_iName", entityName, sizeof(entityName));
		if (g_hConfig.JumpToKey(entityName))
		{
			endangerDamage = g_hConfig.GetFloat("damage", endangerDamage);
			endangerForceTeleport = g_hConfig.GetNum("force teleport", endangerForceTeleport);

			g_hConfig.GoBack();
		}
	}

	if (damage >= endangerDamage)
	{
		forceTeleport = endangerForceTeleport != 0;
		return true;
	}
	return false;
}

static void LoadDefaults()
{
	if (g_hConfig.JumpToKey("defaults"))
	{
		g_fDefaultEndangerDamage = g_hConfig.GetFloat("damage", DefaultEndangerDamage);
		g_iDefaultForceTeleport = g_hConfig.GetNum("force teleport", DefaultForceTeleport);
	}
	else
	{
		g_fDefaultEndangerDamage = DefaultEndangerDamage;
		g_iDefaultForceTeleport = DefaultForceTeleport;
	}
}