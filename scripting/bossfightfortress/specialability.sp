#if defined _SPECIALABILITY_INCLUDED
  #endinput
#endif
#define _SPECIALABILITY_INCLUDED

// Special ability cooldown method, there's a few options!
enum ChargeMethod
{
	ChargeMethod_None    = 0x0,
	ChargeMethod_Timed   = 0x1,
	ChargeMethod_Damage  = 0x2,
	ChargeMethod_Custom  = 0x4
}

enum SpecialAbility
{
	InvalidSpecialAbility = 0
}

// Special ability stuff, charged overtime and activated by specialing.
static bool g_bHasSpecialAbility[MAXPLAYERS+1];

static int        g_iSpecialCurrentPhase[MAXPLAYERS+1];
static Definition g_hSpecialBossDefinition[MAXPLAYERS+1];

static ChargeMethod      g_eSpecialChargeMethod[MAXPLAYERS+1];
static AbilityState      g_eSpecialAbilityState[MAXPLAYERS+1];
static SpecialChargeMode g_eSpecialChargeMode[MAXPLAYERS+1];

static bool  g_bSpecialUseCustomCharge[MAXPLAYERS+1];
static float g_fSpecialCustomChargePercent[MAXPLAYERS+1];
 
static float g_fSpecialChargeStartTime[MAXPLAYERS+1];
static float g_fSpecialChargeTime[MAXPLAYERS+1];

static int g_iSpecialChargeDamage[MAXPLAYERS+1];
static int g_iSpecialChargeDamageTaken[MAXPLAYERS+1];

static bool   g_nHasUseSpecialAbility[MAXPLAYERS+1];
static Handle g_hOnUseSpecialAbility[MAXPLAYERS+1];
static Handle g_hFormatSpecialAbility[MAXPLAYERS+1];

static ArrayList g_hSpecialAbilities[MAXPLAYERS+1];

methodmap SpecialAbility
{
	public SpecialAbility(int client)
	{
		return view_as<SpecialAbility>(client);
	}

	property int Owner
	{
		public get()
		{
			return view_as<int>(this);
		}
	}

	property int Index
	{
		public get()
		{
			return view_as<int>(this);
		}
	}

	property bool IsUsable
	{
		public get()
		{
			return g_bHasSpecialAbility[this.Index];
		}
		public set(bool value)
		{
			g_bHasSpecialAbility[this.Index] = value;
		}
	}

	/******************************************
	 *
	 *  Special ability properties
	 *
	 ******************************************/

	property int CurrentPhase
	{
		public get()
		{
			return g_iSpecialCurrentPhase[this.Index];
		}
		public set(int value)
		{
			g_iSpecialCurrentPhase[this.Index] = value;
		}
	}

	property Definition Boss
	{
		public get()
		{
			return g_hSpecialBossDefinition[this.Index];
		}
		public set(Definition value)
		{
			g_hSpecialBossDefinition[this.Index] = value;
		}
	}

	property Handle FormatHudFwd
	{
		public get()
		{
			return g_hFormatSpecialAbility[this.Index];
		}
		public set(Handle value)
		{
			g_hFormatSpecialAbility[this.Index] = value;
		}
	}

	// needed to avoid confusion with default
	property bool HasUseAbilityFwd
	{
		public get()
		{
			return g_nHasUseSpecialAbility[this.Index];
		}
		public set(bool value)
		{
			g_nHasUseSpecialAbility[this.Index] = value;
		}
	}

	property Handle OnUseAbilityFwd
	{
		public get()
		{
			return g_hOnUseSpecialAbility[this.Index];
		}
		public set(Handle value)
		{
			g_hOnUseSpecialAbility[this.Index] = value;
		}
	}

	property ArrayList SpecialAbilities
	{
		public get()
		{
			return g_hSpecialAbilities[this.Index];
		}
		public set(ArrayList value)
		{
			g_hSpecialAbilities[this.Index] = value;
		}
	}

	property AbilityState AbilityState
	{
		public get()
		{
			return g_eSpecialAbilityState[this.Index];
		}
		public set(AbilityState state)
		{
			g_eSpecialAbilityState[this.Index] = state;
		}
	}

	property ChargeMethod ChargeMethod
	{
		public get()
		{
			return g_eSpecialChargeMethod[this.Index];
		}
		public set(ChargeMethod method)
		{
			g_eSpecialChargeMethod[this.Index] = method;
		}
	}

	property SpecialChargeMode ChargeMode
	{
		public get()
		{
			return g_eSpecialChargeMode[this.Index];
		}
		public set(SpecialChargeMode mode)
		{
			g_eSpecialChargeMode[this.Index] = mode;
		}
	}

	property bool UseCustomCharge
	{
		public get()
		{
			return g_bSpecialUseCustomCharge[this.Index];
		}
		public set(bool value)
		{
			g_bSpecialUseCustomCharge[this.Index] = value;
		}
	}

	property float CustomChargePercent
	{
		public get()
		{
			return g_fSpecialCustomChargePercent[this.Index];
		}
		public set(float percentage)
		{
			g_fSpecialCustomChargePercent[this.Index] = percentage;
		}
	}

	property float ChargeTime
	{
		public get()
		{
			return g_fSpecialChargeTime[this.Index];
		}
		public set(float time)
		{
			if (time < 0)
			{
				time = 0.0;
			}
			g_fSpecialChargeTime[this.Index] = time;
		}
	}

	property float ChargeStartTime
	{
		public get()
		{
			return g_fSpecialChargeStartTime[this.Index];
		}
		public set(float time)
		{
			g_fSpecialChargeStartTime[this.Index] = time;
		}
	}

	property float SpecialCooldownTimePercentage
	{
		public get()
		{
			float startTime = this.ChargeStartTime;
			float cooldownTime = this.ChargeTime;

			return GetTimePassedPercentage(startTime, cooldownTime);
		}
	}

	property int ChargeDamage
	{
		public get()
		{
			return g_iSpecialChargeDamage[this.Index];
		}
		public set(int damage)
		{
			if (damage < 0)
			{
				damage = 0;
			}
			g_iSpecialChargeDamage[this.Index] = damage;
		}
	}

	property int ChargeDamageTaken
	{
		public get()
		{
			return g_iSpecialChargeDamageTaken[this.Index];
		}
		public set(int damage)
		{
			if (damage < 0)
			{
				damage = 0;
			}
			g_iSpecialChargeDamageTaken[this.Index] = damage;
		}
	}

	property float ChargeDamagePercentage
	{
		public get()
		{
			int damageTaken = this.ChargeDamageTaken;
			int damageCooldown = this.ChargeDamage;

			return (float(damageTaken) / damageCooldown);
		}
	}

	/******************************************
	 *
	 *  Special ability function
	 *
	 ******************************************/

	public float ProcessSpecialChargePercentage(float currentPercentage, float newPercentage)
	{
		if (this.ChargeMode == SpecialChargeMode_Assisting)
		{
			return currentPercentage + newPercentage;
		}
		else if (currentPercentage < newPercentage)
		{
			return newPercentage;
		}
		return currentPercentage;
	}

	public float GetChargePercent()
	{
		// If we're not playing yet, it's not charged
		if (g_eRoundState == BffRoundState_Preround)
		{
			return 0.0;
		}
		// If we're ready it's 100%
		if (this.AbilityState == AbilityState_Ready)
		{
			return 1.0;
		}

		float percentage = 0.0;

		ChargeMethod method = this.ChargeMethod;
		if ((method & ChargeMethod_Damage) == ChargeMethod_Damage)
		{
			float temporaryPercentage = this.ChargeDamagePercentage;
			percentage = this.ProcessSpecialChargePercentage(percentage, temporaryPercentage);
		}

		if ((method & ChargeMethod_Timed) == ChargeMethod_Timed)
		{
			float temporaryPercentage = this.SpecialCooldownTimePercentage;
			percentage = this.ProcessSpecialChargePercentage(percentage, temporaryPercentage);
		}

		if ((method & ChargeMethod_Custom) == ChargeMethod_Custom)
		{
			float temporaryPercentage = this.CustomChargePercent;
			percentage = this.ProcessSpecialChargePercentage(percentage, temporaryPercentage);
		}

		if (percentage > 1.0)
		{
			percentage = 1.0;
		}

		return percentage;
	}

	public void SetSpecialCustomCharge(float percentage)
	{
		if ((this.ChargeMethod & ChargeMethod_Custom) == ChargeMethod_Custom)
		{
			this.CustomChargePercent = percentage;
		}
	}

	public void SetSpecialCooldown(int damage, float time, bool recallCustom, bool useCustom)
	{
		ChargeMethod method = ChargeMethod_None;
		ChargeMethod current = this.ChargeMethod;
		bool charging = this.AbilityState == AbilityState_Charging;

		if (damage < 0)
		{
			damage = this.ChargeDamage;
		}
		if (damage >= 0)
		{
			this.ChargeDamage = damage;
			if (damage > 0)
			{
				method |= ChargeMethod_Damage;
				if (charging && (current & ChargeMethod_Damage) != ChargeMethod_Damage)
				{
					this.ChargeDamageTaken = 0;
				}
			}
		}

		if (time < 0.0)
		{
			time = this.ChargeTime;
		}
		if (time >= 0.0)
		{
			this.ChargeTime = time;
			if (time > 0.0)
			{
				method |= ChargeMethod_Timed;
				if (charging && (current & ChargeMethod_Timed) != ChargeMethod_Timed)
				{
					this.ChargeStartTime = GetGameTime();
				}
			}
		}

		if (recallCustom == true)
		{
			useCustom = this.UseCustomCharge;
		}
		if (useCustom == true)
		{
			method |= ChargeMethod_Custom;
			if (charging && (current & ChargeMethod_Custom) != ChargeMethod_Custom)
			{
				this.CustomChargePercent = 0.0;
			}
		}
		
		this.ChargeMethod = method;
	}

	public void StartSpecialCooldown(int damage=-1, float time=-1.0, bool recallCustom=true, bool useCustom=false)
	{
		this.SetSpecialCooldown(damage, time, recallCustom, useCustom);

		this.ChargeDamageTaken = 0;
		this.CustomChargePercent = 0.0;
		this.ChargeStartTime = GetGameTime();

		this.AbilityState = AbilityState_Charging;
		if (this.GetChargePercent() == 1.0)
		{
			this.AbilityState = AbilityState_Ready;
		}
	}

	public bool UpdateCharge()
	{
		if (this.ChargeMethod != ChargeMethod_None)
		{
			if (this.AbilityState != AbilityState_Ready && this.GetChargePercent() == 1.0)
			{
				this.AbilityState = AbilityState_Ready;
			}
			return true;
		}
		return false;
	}

	public bool Use()
	{
		if (g_eRoundState != BffRoundState_Preround)
		{
			bool result = true;

			float chargePercent = this.GetChargePercent();

			Call_StartForward(this.OnUseAbilityFwd);
			Call_PushCell(this.Owner);
			Call_PushFloat(chargePercent);
			Call_Finish(result);

			if (result == true)
			{
				// We have to call them individually manually, however much i hate it
				// due to needing to pass the definition itself to the callback
				// for options in case parameters are overriden in a child definition
				ArrayList abilities = this.SpecialAbilities;
				int length = abilities.Length;
				for (int i = 0; i < length; i++)
				{
					Definition ability = abilities.Get(i);
					ability.StartFunction("OnUse");
					Call_PushCell(this.Owner);
					Call_PushCell(chargePercent);
					Call_PushCell(ability);
					Call_Finish();
				}

				this.StartSpecialCooldown();
			}
			return result;
		}
		return false;
	}

	public void CreateOnUseAbilityForward()
	{
		if (this.OnUseAbilityFwd != null)
		{
			delete this.OnUseAbilityFwd;
		}
		this.OnUseAbilityFwd = CreateForward(ET_Single, Param_Cell, Param_Float);
	}

	public void CreateFormatHudForward()
	{
		if (this.FormatHudFwd != null)
		{
			delete this.FormatHudFwd;
		}
		this.FormatHudFwd = CreateForward(ET_Ignore, Param_Cell, Param_String, Param_Cell, Param_Cell, Param_Cell);
	}

	public void AdvanceToPhase(int phase)
	{
		// We may first get a special ability at a later phase
		// But after we've received a special ability, it's always automatically inherited
		bool usable = this.IsUsable;

		char property[64];
		Definition boss = this.Boss;
		for (int i = this.CurrentPhase + 1; i <= phase; i++)
		{
			int length = 0;
			Format(property, sizeof(property), "SpecialAbilities<%d>", i);
			if (boss.GetDefinitionArrayLength(property, length))
			{
				// always clear if we have special abilities set
				this.SpecialAbilities.Clear();

				if (length != 0)
				{
					// only setting usable to true if we have any abilities in array
					usable = true;

					Definition[] abilities = new Definition[length];
					boss.GetDefinitionArray(property, abilities, length);
					for (int j = 0; j < length; j++)
					{
						this.SpecialAbilities.Push(abilities[j]);
					}
				}
			}
			
			Format(property, sizeof(property), "OnUseSpecialAbility<%d>", i);
			if (boss.HasFunction(property))
			{
				usable = true;
				this.HasUseAbilityFwd = true;
				this.CreateOnUseAbilityForward();
				boss.AddToForward(property, this.OnUseAbilityFwd);
			}
			
			Format(property, sizeof(property), "SpecialFormatHud<%d>", i);
			if (boss.HasFunction(property))
			{
				this.CreateFormatHudForward();
				boss.AddToForward(property, this.FormatHudFwd);
			}

			Format(property, sizeof(property), "HasSpecialAbility<%d>", i);
			bool newUsable = boss.GetBoolEx(property, true);
			if (usable)
			{
				// if our ability is already usable, it's pretty simple
				usable = newUsable;
			}
			else if (newUsable)
			{
				// however, if not ... we need to look at our abilities
				usable = (this.HasUseAbilityFwd || this.SpecialAbilities.Length != 0);
			}

			Format(property, sizeof(property), "SpecialChargeMode<%d>", i);
			this.ChargeMode = boss.GetCellEx(property, this.ChargeMode);

			// should recall UseCustomCharge if the property is not found
			Format(property, sizeof(property), "SpecialUseCustomCharge<%d>", i);
			bool useCustomCharge = false;
			bool recallCustomCharge = (boss.GetBool(property, useCustomCharge) == false);

			Format(property, sizeof(property), "SpecialChargeDamage<%d>", i);
			int chargeDamage = boss.GetIntEx(property, this.ChargeDamage);

			Format(property, sizeof(property), "SpecialChargeTime<%d>", i);
			float chargeTime = boss.GetFloatEx(property, this.ChargeTime);

			this.SetSpecialCooldown(chargeDamage, chargeTime, recallCustomCharge, useCustomCharge);
		}

		this.CurrentPhase = phase;
		this.IsUsable = usable;
	}

	public void Setup(Definition boss)
	{
		this.IsUsable = false;
		this.CurrentPhase = -1;
		this.Boss = boss;

		/**
		 * Default values
		 **/
		this.ChargeMode = SpecialChargeMode_Assisting;
		this.StartSpecialCooldown(1200, 45.0, false, false);

		this.CreateOnUseAbilityForward();
		AddToForward(this.OnUseAbilityFwd, null, DefaultOnUseSpecialAbility);

		// this is to be false, since it's for custom useability
		// the DefaultOnUseSpecialAbility is only there to simplify things somewhere else
		this.HasUseAbilityFwd = false;

		this.CreateFormatHudForward();
		AddToForward(this.FormatHudFwd, null, DefaultFormatSpecialHud);

		this.SpecialAbilities = new ArrayList();
	}

	public void Destroy()
	{
		g_bHasSpecialAbility[this.Index] = false;

		this.ChargeDamage = 0;
		this.ChargeTime = 0.0;
		this.CustomChargePercent = 0.0;
		this.ChargeMethod = ChargeMethod_None;
		this.AbilityState = AbilityState_None;

		delete this.OnUseAbilityFwd;
		this.OnUseAbilityFwd = null;

		delete this.FormatHudFwd;
		this.FormatHudFwd = null;

		delete this.SpecialAbilities;
		this.SpecialAbilities = null;
	}

	public void FormatAbility(char[] buffer, int bufferSize)
	{
		Call_StartForward(this.FormatHudFwd);
		Call_PushCell(this);
		Call_PushStringEx(buffer, bufferSize, SM_PARAM_STRING_UTF8|SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
		Call_PushCell(bufferSize);
		Call_PushCell(this.AbilityState);
		Call_PushCell(PercentageToInt(this.GetChargePercent(), false, true));
		Call_Finish();
	}
}