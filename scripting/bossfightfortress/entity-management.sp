public void OnEntityCreated(int entity, const char[] classname)
{
	if (g_eRoundState != BffRoundState_Invalid)
	{
		if (IsBlockedItem(classname))
		{
			SDKHook(entity, SDKHook_Touch, OnItemTouch);
		}
	}
}

public void HookMiscEntities()
{
	static const char classnames[][] = 
	{
		"item_healthkit*",
		"item_ammopack*",
		"tf_ammo_pack"
	};

	for (int i = 0; i < sizeof(classnames); i++)
	{
		int entity = -1;
		while ((entity = FindEntityByClassname(entity, classnames[i])) != -1)
		{
			SDKHook(entity, SDKHook_Touch, OnItemTouch);
		}
	}
}

public void UnhookMiscEntities()
{
	static const char classnames[][] = 
	{
		"item_healthkit*",
		"item_ammopack*",
		"tf_ammo_pack"
	};

	for (int i = 0; i < sizeof(classnames); i++)
	{
		int entity = -1;
		while ((entity = FindEntityByClassname(entity, classnames[i])) != -1)
		{
			SDKUnhook(entity, SDKHook_Touch, OnItemTouch);
		}
	}
}

static Action OnItemTouch(int entity, int other)
{
	#pragma unused entity
	BffBaseClient client = BffBaseClient(other);
	if (client.IsValid && client.IsBoss)
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

static bool IsBlockedItem(const char[] classname)
{
	return StrContains(classname, "item_healthkit") != -1 ||
	       StrContains(classname, "item_ammopack") != -1 ||
	       StrEqual(classname, "tf_ammo_pack");
}