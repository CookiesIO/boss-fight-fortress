#if defined _BFFUTILS_INCLUDED
  #endinput
#endif
#define _BFFUTILS_INCLUDED

float GetTimePassedPercentage(float startTime, float duration)
{
	// No duration, instant charge! Yarr! Pirates! .. what?
	if (duration == 0.0)
	{
		return 1.0;
	}

	float currentGameTime = GetGameTime();

	// If startTime is lower than (current time - duration), we're at full charge
	if (startTime < (currentGameTime - duration))
	{
		return 1.0;
	}

	// Anyways, other than that it's simple math! timeSpan / duration
	//                                            timeSpan = (current - start) by the by.
	return (currentGameTime - startTime) / duration;
}

int PercentageToInt(float percentage, bool ceil = false, bool adjust = false)
{
	if (adjust)
	{
		// Example: Accuracy 5, percentage 0.52
		// Accuracy Float = 5 / 100 = 0.05
		// ceil(0.52 * (1 / 0.05)) * 5 = ??
		// Step-by-step
		// 1 / 0.05 = 20
		// 0.52 * 20 = 10.4
		// ceil(10.4) = 11
		// 11 * 5 = 55
		// 0.52 = 52%, round up to nearest 5 = 55%!
		// Just added it to those wondering
		if (ceil)
		{
			return RoundToCeil(percentage * (1 / CHARGE_ACCURACY_FLOAT)) * CHARGE_ACCURACY;
		}
		return RoundToFloor(percentage * (1 / CHARGE_ACCURACY_FLOAT)) * CHARGE_ACCURACY;
	}
	if (ceil)
	{
		return RoundToCeil(percentage * 100);
	}
	return RoundToFloor(percentage * 100);
}