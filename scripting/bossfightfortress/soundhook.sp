static const float CatchPhraseChance = 0.1;
static bool g_bExpectingSound = false;

void EnableSoundHook()
{
	AddNormalSoundHook(SoundHook);
	g_bExpectingSound = false;
}

void DisableSoundHook()
{
	RemoveNormalSoundHook(SoundHook);
}

void SoundHookShouldExpectSound()
{
	g_bExpectingSound = true;
}

void SoundHookShouldExpectSoundNoMore()
{
	g_bExpectingSound = false;
}

static Action SoundHook(int clients[MAXPLAYERS], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags, char soundEntry[PLATFORM_MAX_PATH], int &seed)
{
	#pragma unused soundEntry, seed
	Action result = Plugin_Continue;
	if (g_bExpectingSound == false && entity > 0 && entity <= MaxClients)
	{
		BffBaseClient client = BffBaseClient(entity);
		if (client.IsBoss)
		{
			if (client.Boss.StartFunction("SoundHook"))
			{
				int newClients[MAXPLAYERS];
				int newNumClients = numClients;
				char newSample[PLATFORM_MAX_PATH];
				int newEntity = entity;
				int newChannel = channel;
				float newVolume = volume;
				int newLevel = level;
				int newPitch = pitch;
				int newFlags = flags;

				for (int i = 0; i < numClients; i++)
				{
					newClients[i] = clients[i];
				}
				strcopy(newSample, sizeof(newSample), sample);

				Call_PushCell(client);
				Call_PushArrayEx(newClients, MAXPLAYERS, SM_PARAM_COPYBACK);
				Call_PushCellRef(newNumClients);
				Call_PushStringEx(newSample, sizeof(newSample), SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
				Call_PushCellRef(newEntity);
				Call_PushCellRef(newChannel);
				Call_PushFloatRef(newVolume);
				Call_PushCellRef(newLevel);
				Call_PushCellRef(newPitch);
				Call_PushCellRef(newFlags);
				Call_Finish(result);

				if (result == Plugin_Changed)
				{
					numClients = newNumClients;
					entity = newEntity;
					channel = newChannel;
					volume = newVolume;
					level = newLevel;
					pitch = newPitch;
					flags = newFlags;

					for (int i = 0; i < newNumClients; i++)
					{
						clients[i] = newClients[i];
					}
					strcopy(sample, sizeof(sample), newSample);
				}
			}

			if (result == Plugin_Continue && channel == SNDCHAN_VOICE)
			{
				char catchPhrase[PLATFORM_MAX_PATH];
				if (GetRandomFloat(0.0, 1.0) < CatchPhraseChance && client.FindSound("SoundsCatchPhrase", catchPhrase))
				{
					strcopy(sample, sizeof(sample), catchPhrase);
					result = Plugin_Changed;
				}
				else if (client.Boss.GetBoolEx("MuteVoice"))
				{
					result = Plugin_Stop;
				}
			}
			else if (result == Plugin_Handled)
			{
				result = Plugin_Continue;
			}
		}
	}
	return result;
}