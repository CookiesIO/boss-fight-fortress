#if defined _CHARGEABILITY_INCLUDED
  #endinput
#endif
#define _CHARGEABILITY_INCLUDED

enum ChargeAbility
{
	InvalidChargeAbility = 0
}

static const float AerialEscapeActivateTime = 2.5;

// Charge Ability is used for both Primary and Secondary abilities, so make room for it
// i <= MAXPLAYERS == Primary, i > MAXPLAYERS == Secondary, client = (i <= MAXPLAYERS ? i : i - MAXPLAYERS)
// Should maybe make this backed by a StringMap instead.
// Or maybe make an ArrayList with a blocksize that encompasses all the values below
#define MaxChargeAbilities (MAXPLAYERS * 2) + 1

// Charge ability stuff. There is a lot.
static bool g_bHasChargeAbility[MaxChargeAbilities];

static int        g_iChargeCurrentPhase[MaxChargeAbilities];
static Definition g_hClientBossDefinition[MaxChargeAbilities];
static bool       g_bCanSaveFromDanger[MaxChargeAbilities];
static bool       g_bIsAerialEscape[MaxChargeAbilities];
static bool       g_bIsChargeAbilityActive[MaxChargeAbilities];

static float g_fChargeStartTime[MaxChargeAbilities];
static float g_fChargeTime[MaxChargeAbilities];
static float g_fChargeCooldownTime[MaxChargeAbilities];
static float g_fChargePercentAtLastActivation[MaxChargeAbilities];

static Definition   g_hAbilityDefinition[MaxChargeAbilities];
static ChargeMode   g_eChargeMode[MaxChargeAbilities];
static AbilityState g_eChargeAbilityState[MaxChargeAbilities];

static Handle g_hOnChargeStart[MaxChargeAbilities];
static Handle g_hOnChargeReleased[MaxChargeAbilities];
static Handle g_hFormatChargeAbility[MaxChargeAbilities];
static Handle g_hOnPlayerRunCmd[MaxChargeAbilities];
static bool g_bHasOnPlayerRunCmd[MaxChargeAbilities];

// Initial setup for possibly providing drop-in replacements or alternatives
methodmap ChargeAbility
{
	public ChargeAbility(int client, bool isSecondary)
	{
		if (isSecondary)
		{
			return view_as<ChargeAbility>(client + MAXPLAYERS);
		}
		return view_as<ChargeAbility>(client);
	}

	property bool IsSecondary
	{
		public get()
		{
			return view_as<int>(this) > MAXPLAYERS;
		}
	}

	property int ChargeButton
	{
		public get()
		{
			if (this.IsSecondary)
			{
				return IN_DUCK;
			}
			return IN_ATTACK2;
		}
	}

	property int Owner
	{
		public get()
		{
			// cheapo impl, should probably do something better
			int result = view_as<int>(this);
			while (result > MAXPLAYERS)
			{
				result -= MAXPLAYERS;
			}
			return result;
		}
	}

	property int Index
	{
		public get()
		{
			return view_as<int>(this);
		}
	}

	property bool IsUsable
	{
		public get()
		{
			return g_bHasChargeAbility[this.Index];
		}
		public set(bool value)
		{
			g_bHasChargeAbility[this.Index] = value;
		}
	}

	/******************************************
	 *
	 *  Charge ability properties
	 *
	 ******************************************/

	property bool IsAerialEscape
	{
		public get()
		{
			return g_bIsAerialEscape[this.Index];
		}
		public set(bool value)
		{
			g_bIsAerialEscape[this.Index] = value;
		}
	}

	property bool IsActive
	{
		public get()
		{
			// always active if not aerial escape
			if (this.IsAerialEscape == false)
			{
				return true;
			}
			return g_bIsChargeAbilityActive[this.Index];
		}
		public set(bool value)
		{
			g_bIsChargeAbilityActive[this.Index] = value;
		}
	}

	property int CurrentPhase
	{
		public get()
		{
			return g_iChargeCurrentPhase[this.Index];
		}
		public set(int value)
		{
			g_iChargeCurrentPhase[this.Index] = value;
		}
	}

	property bool CanSaveFromDanger
	{
		public get()
		{
			return g_bCanSaveFromDanger[this.Index];
		}
		public set(bool value)
		{
			g_bCanSaveFromDanger[this.Index] = value;
		}
	}

	property Definition Boss
	{
		public get()
		{
			return g_hClientBossDefinition[this.Index];
		}
		public set(Definition value)
		{
			g_hClientBossDefinition[this.Index] = value;
		}
	}

	property Handle FormatHudFwd
	{
		public get()
		{
			return g_hFormatChargeAbility[this.Index];
		}
		public set(Handle value)
		{
			g_hFormatChargeAbility[this.Index] = value;
		}
	}

	property Handle OnChargeStartFwd
	{
		public get()
		{
			return g_hOnChargeStart[this.Index];
		}
		public set(Handle value)
		{
			g_hOnChargeStart[this.Index] = value;
		}
	}

	property Handle OnChargeReleasedFwd
	{
		public get()
		{
			return g_hOnChargeReleased[this.Index];
		}
		public set(Handle value)
		{
			g_hOnChargeReleased[this.Index] = value;
		}
	}

	property bool HasOnPlayerRunCmdFwd
	{
		public get()
		{
			return g_bHasOnPlayerRunCmd[this.Index];
		}
		public set(bool value)
		{
			g_bHasOnPlayerRunCmd[this.Index] = value;
		}
	}

	property Handle OnPlayerRunCmdFwd
	{
		public get()
		{
			return g_hOnPlayerRunCmd[this.Index];
		}
		public set(Handle value)
		{
			g_hOnPlayerRunCmd[this.Index] = value;
		}
	}

	property AbilityState AbilityState
	{
		public get()
		{
			return g_eChargeAbilityState[this.Index];
		}
		public set(AbilityState state)
		{
			g_eChargeAbilityState[this.Index] = state;
		}
	}

	property ChargeMode ChargeMode
	{
		public get()
		{
			return g_eChargeMode[this.Index];
		}
		public set(ChargeMode mode)
		{
			g_eChargeMode[this.Index] = mode;
		}
	}

	property float ChargeStartTime
	{
		public get()
		{
			return g_fChargeStartTime[this.Index];
		}
		public set(float time)
		{
			g_fChargeStartTime[this.Index] = time;
		}
	}

	property float ChargeTime
	{
		public get()
		{
			return g_fChargeTime[this.Index];
		}
		public set(float time)
		{
			if (time < 0)
			{
				time = 0.0;
			}
			g_fChargeTime[this.Index] = time;
		}
	}

	property float ChargeCooldownTime
	{
		public get()
		{
			return g_fChargeCooldownTime[this.Index];
		}
		public set(float time)
		{
			if (time < 0)
			{
				time = 0.0;
			}
			g_fChargeCooldownTime[this.Index] = time;
		}
	}

	property float ChargePercentAtLastActivation
	{
		public get()
		{
			return g_fChargePercentAtLastActivation[this.Index];
		}
		public set(float value)
		{
			g_fChargePercentAtLastActivation[this.Index] = value;
		}
	}

	property Definition AbilityDefinition
	{
		public get()
		{
			return g_hAbilityDefinition[this.Index];
		}
		public set(Definition value)
		{
			g_hAbilityDefinition[this.Index] = value;
		}
	}

	/******************************************
	 *
	 *  Charge ability functions
	 *
	 ******************************************/

	public float GetChargePercent(bool adjusted=true)
	{
		switch (this.AbilityState)
		{
			case AbilityState_Ready:
			{
				// Continuous is ready when it's at 100%, Normal is ready at 0%
				if (adjusted && this.ChargeMode == ChargeMode_Continuous)
				{
					return 1.0;
				}
				return 0.0;
			}
			case AbilityState_Charging:
			{
				// Continuous descreases from 100% to 0%, whereas normal increases from 0% to 100%
				float percentage = GetTimePassedPercentage(this.ChargeStartTime, this.ChargeTime);
				if (adjusted && this.ChargeMode == ChargeMode_Continuous)
				{
					return 1 - percentage;
				}
				return percentage;
			}
			case AbilityState_OnCooldown:
			{
				// No difference here, it'll count upwards for both.
				return GetTimePassedPercentage(this.ChargeStartTime, this.ChargeCooldownTime);
			}
		}
		return 0.0;
	}

	public void SetChargeCooldownPercentage(float percentage)
	{
		if (this.ChargeCooldownTime == 0.0)
		{
			return;
		}
		if (percentage < 0.0)
		{
			percentage = 0.0;
		}
		else if (percentage > 1.0)
		{
			percentage = 1.0;
		}

		this.ChargeStartTime = GetGameTime() - (this.ChargeCooldownTime * percentage);
		this.AbilityState = percentage == 1.0 ? AbilityState_Ready : AbilityState_OnCooldown;
	}

	public void SetChargePercentage(float percentage)
	{
		if (this.ChargeTime == 0.0)
		{
			return;
		}
		this.ChargeStartTime = GetGameTime() - (this.ChargeTime * percentage);
	}

	public void SetChargeCooldownTime(float time, bool adjust)
	{
		if (time == this.ChargeCooldownTime)
		{
			return;
		}
		if (time <= 0.0)
		{
			this.ChargeCooldownTime = 0.0;
			this.AbilityState = AbilityState_Ready;
			return;
		}

		if (adjust && this.AbilityState == AbilityState_OnCooldown)
		{
			float percentage = this.GetChargePercent();
			this.ChargeCooldownTime = time;
			this.SetChargeCooldownPercentage(percentage);
		}
		else
		{
			this.ChargeCooldownTime = time;
		}
	}

	public void SetChargeTime(float time, bool adjust)
	{
		if (time == this.ChargeTime)
		{
			return;
		}
		if (time <= 0.0)
		{
			this.ChargeTime = 0.0;
			return;
		}

		if (adjust && this.AbilityState == AbilityState_Charging)
		{
			float percentage = this.GetChargePercent(false);
			this.ChargeTime = time;
			this.SetChargePercentage(percentage);
		}
		else
		{
			this.ChargeTime = time;
		}
	}

	public Action OnPlayerRunCmd(int &buttons, int &impulse, float vel[3], float ang[3], int &weapon)
	{
		Action result = Plugin_Continue;

		if (this.HasOnPlayerRunCmdFwd && this.AbilityState == AbilityState_Charging)
		{
			float charge = this.GetChargePercent();

			Call_StartForward(this.OnPlayerRunCmdFwd);
			Call_PushCell(this.Owner);
			Call_PushFloat(charge);
			Call_PushCellRef(buttons);
			Call_PushCellRef(impulse);
			Call_PushArrayEx(vel, sizeof(vel), SM_PARAM_COPYBACK);
			Call_PushArrayEx(ang, sizeof(ang), SM_PARAM_COPYBACK);
			Call_PushCellRef(weapon);
			Call_PushCell(this.AbilityDefinition);
			Call_Finish(result);
		}

		return result;
	}

	public bool StartCharge()
	{
		float cooldownPercent = this.GetChargePercent();

		// Worst case, always!
		bool result = false;

		// For the Normal charge mode, we must be ready in order to start
		if (this.ChargeMode == ChargeMode_Continuous ||
			this.ChargeMode == ChargeMode_Normal && this.AbilityState == AbilityState_Ready)
		{
			Call_StartForward(this.OnChargeStartFwd);
			Call_PushCell(this.Owner);
			Call_PushFloat(cooldownPercent);

			// Only continuous abilities gets the cooldown difference
			if (this.ChargeMode == ChargeMode_Continuous)
			{
				Call_PushFloat(FloatAbs(cooldownPercent - this.ChargePercentAtLastActivation));
			}

			Call_PushCell(this.AbilityDefinition);
			Call_Finish(result);
		}

		if (this.ChargeMode == ChargeMode_Normal)
		{
			result = this.AbilityState == AbilityState_Ready;
		}
		return result;
	}

	public void ChargeReleased()
	{
		float chargePercent = this.GetChargePercent(true);
		bool result = true;

		Call_StartForward(this.OnChargeReleasedFwd);
		Call_PushCell(this.Owner);
		Call_PushFloat(chargePercent);

		// Only continuous charge mode benefits from the step since activation
		if (this.ChargeMode == ChargeMode_Continuous)
		{
			Call_PushFloat(FloatAbs(this.ChargePercentAtLastActivation - chargePercent));
		}

		Call_PushCell(this.AbilityDefinition);
		Call_Finish(result);

		if (result == false && this.ChargeMode != ChargeMode_Continuous)
		{
			this.AbilityState = AbilityState_Ready;
		}
		else
		{
			if (this.ChargeMode == ChargeMode_Continuous)
			{
				// In continuous mode, it should start where it was let off
				this.ChargeStartTime = (GetGameTime() - (this.ChargeCooldownTime * chargePercent));
			}
			else
			{
				this.ChargeStartTime = GetGameTime();
			}

			this.AbilityState = AbilityState_OnCooldown;
		}
		this.ChargePercentAtLastActivation = chargePercent;
	}

	public bool UpdateCharge(int buttons, int lastButtons)
	{
		static bool releasedButton[MaxChargeAbilities] = { true, ... };
		static bool wasOffGround[MaxChargeAbilities] = { true, ... };
		static float offGroundStartTime[MaxChargeAbilities] = { 0.0, ... };

		bool changed = false;

		// short out if ability is not usable
		if (this.IsUsable == false)
		{
			return false;
		}

		// If this ability is an aerial escape, we can only use it if we've been in the air for some time
		if (this.IsAerialEscape)
		{
			int entityFlags = GetEntityFlags(this.Owner);
			if (wasOffGround[this.Index])
			{
				if ((entityFlags & FL_ONGROUND) == FL_ONGROUND)
				{
					this.IsActive = false;
					wasOffGround[this.Index] = false;
					
					// if this is a continuous ability and is charging, stop the ability
					if (this.ChargeMode == ChargeMode_Continuous && this.AbilityState == AbilityState_Charging)
					{
						this.ChargeReleased();
					}
					changed = true;
				}
				else if (this.IsActive == false)
				{
					float timeDifference = GetGameTime() - offGroundStartTime[this.Index];
					if (timeDifference < AerialEscapeActivateTime)
					{
						return false;
					}
					this.SetChargeCooldownPercentage(0.0);
					this.IsActive = true;
					changed = true;
				}
			}
			else
			{
				if ((entityFlags & FL_ONGROUND) != FL_ONGROUND)
				{
					wasOffGround[this.Index] = true;
					offGroundStartTime[this.Index] = GetGameTime();
				}
				return false;
			}
		}

		int button = this.ChargeButton;

		if (this.AbilityState == AbilityState_OnCooldown)
		{
			if (this.GetChargePercent() == 1.0)
			{
				this.AbilityState = AbilityState_Ready;
			}

			// If we're using the normal charge mode, there's no need to do anything more when we're in cooldown mode
			// ... Other than keeping track of the button!
			if (this.ChargeMode == ChargeMode_Normal)
			{
				if (releasedButton[this.Index] == false)
				{
					releasedButton[this.Index] = ((buttons & button) != button);
				}
				return true;
			}

			changed = true;
		}

		if ((buttons & button) == button)
		{
			switch (this.AbilityState)
			{
				case AbilityState_Ready,
				     AbilityState_OnCooldown: // When using Continuous
				{
					if (releasedButton[this.Index])
					{
						if (this.StartCharge() == false)
						{
							return this.AbilityState == AbilityState_OnCooldown;
						}

						float cooldownPercent = this.GetChargePercent();
						float chargeStartTime = GetGameTime();
						if (this.ChargeMode == ChargeMode_Continuous)
						{
							// In continuous mode, it should start we're it was let off
							chargeStartTime = (chargeStartTime - (this.ChargeTime * (1 - cooldownPercent)));
						}

						this.ChargeStartTime = chargeStartTime;
						this.ChargePercentAtLastActivation = cooldownPercent;
						this.AbilityState = AbilityState_Charging;

						releasedButton[this.Index] = false;
						changed = true;
					}
				}
				case AbilityState_Charging:
				{
					if (this.ChargeMode == ChargeMode_Continuous)
					{
						if (this.GetChargePercent() == 0.0)
						{
							this.ChargeReleased();
						}
					}
					changed = true;
				}
			}
		}
		else if ((lastButtons & button) == button)
		{
			if (this.AbilityState == AbilityState_Charging)
			{
				// If we're charging and we get in here, then we'll need to release the charge!
				this.ChargeReleased();
				changed = true;
			}
			releasedButton[this.Index] = true;
		}
		return changed;
	}

	public void CreateOnChargeStartForward(Handle &fwd, ChargeMode chargeMode)
	{
		delete fwd;
		if (chargeMode == ChargeMode_Continuous)
		{
			fwd = CreateForward(ET_Single, Param_Cell, Param_Float, Param_Float, Param_Cell);
		}
		else
		{
			fwd = CreateForward(ET_Single, Param_Cell, Param_Float, Param_Cell);
		}
	}

	public void CreateOnChargeReleasedForward(Handle &fwd, ChargeMode chargeMode)
	{
		delete fwd;
		if (chargeMode == ChargeMode_Continuous)
		{
			fwd = CreateForward(ET_Single, Param_Cell, Param_Float, Param_Float, Param_Cell);
		}
		else
		{
			fwd = CreateForward(ET_Single, Param_Cell, Param_Float, Param_Cell);
		}
	}

	public void CreatePlayerRunCmdForward(Handle &fwd)
	{
		delete fwd; // client, chargePercent, &buttons, &impulse, vel[3], ang[3], &weapon
		fwd = CreateForward(ET_Single, Param_Cell, Param_Float, Param_CellByRef, Param_CellByRef, Param_Array, Param_Array, Param_CellByRef, Param_Cell);
	}

	public void CreateFormatHudForward()
	{
		delete this.FormatHudFwd;
		this.FormatHudFwd = CreateForward(ET_Ignore, Param_Cell, Param_String, Param_Cell, Param_Cell, Param_Cell);
	}

	public void AdvanceToPhase(int newPhase)
	{
		// Just to know which properties to grab
		char type[16];
		if (this.IsSecondary)
		{
			strcopy(type, sizeof(type), "Secondary");
		}
		else
		{
			strcopy(type, sizeof(type), "Primary");
		}

		// We may first get a special ability at a later phase
		// But after we've received a special ability, it's always automatically inherited
		bool usable = this.IsUsable;
		bool wasUsable = usable;
		bool canSaveFromDanger = this.CanSaveFromDanger;
		bool isAerialEscape = this.IsAerialEscape;
		ChargeMode chargeMode = this.ChargeMode;
		Definition abilityDefinition = this.AbilityDefinition;

		Handle newOnChargeStartFwd = null;
		Handle newOnChargeReleasedFwd = null;
		Handle newOnPlayerRunCmdFwd = null;
		bool hasNewOnPlayerRunCmdFwd = false;
		bool onChargeForwardsChanged = false;

		char property[64];
		Definition boss = this.Boss;
		for (int phase = this.CurrentPhase + 1; phase <= newPhase; phase++)
		{
			// does this phase have a charge ability of it's own?
			ChargeMode phaseChargeMode = ChargeMode_Invalid;

			// during boss definition validation, it's checked whether or not we have all of OnChargeStart, OnChargeReleased and ChargeMode
			// so it's safe here to just grab them all if we have ChargeMode set!
			Format(property, sizeof(property), "%sChargeMode<%d>", type, phase);
			if (boss.GetCell(property, phaseChargeMode) && phaseChargeMode != ChargeMode_Invalid)
			{
				// abilityDefinition should be set to null, since we have functions on the boss instead
				abilityDefinition = null;
				onChargeForwardsChanged = true;

				this.CreateOnChargeStartForward(newOnChargeStartFwd, phaseChargeMode);
				Format(property, sizeof(property), "On%sStart<%d>", type, phase);
				boss.AddToForward(property, newOnChargeStartFwd);

				this.CreateOnChargeReleasedForward(newOnChargeReleasedFwd, phaseChargeMode);
				Format(property, sizeof(property), "On%sReleased<%d>", type, phase);
				boss.AddToForward(property, newOnChargeReleasedFwd);

				this.CreatePlayerRunCmdForward(newOnPlayerRunCmdFwd);
				Format(property, sizeof(property), "On%sPlayerRunCmd<%d>", type, phase);
				hasNewOnPlayerRunCmdFwd = boss.AddToForward(property, newOnPlayerRunCmdFwd);

				Format(property, sizeof(property), "%sCanSaveFromDanger<%d>", type, phase);
				canSaveFromDanger = boss.GetBoolEx(property, false);

				Format(property, sizeof(property), "%sIsAerialEscape<%d>", type, phase);
				isAerialEscape = boss.GetBoolEx(property, false);
			}
			else
			{
				// If the boss doesn't have a it's functions set itself,
				// it might have an ability deifnition instead
				Format(property, sizeof(property), "%sAbility<%d>", type, phase);
				if (boss.GetDefinition(property, abilityDefinition))
				{
					onChargeForwardsChanged = true;

					if (abilityDefinition.IsDescendantOf(g_hNormalChargeAbilityDefinition))
					{
						phaseChargeMode = ChargeMode_Normal;
					}
					else
					{
						phaseChargeMode = ChargeMode_Continuous;
					}

					this.CreateOnChargeStartForward(newOnChargeStartFwd, phaseChargeMode);
					abilityDefinition.AddToForward("OnChargeStart", newOnChargeStartFwd);

					this.CreateOnChargeReleasedForward(newOnChargeReleasedFwd, phaseChargeMode);
					abilityDefinition.AddToForward("OnChargeReleased", newOnChargeReleasedFwd);

					this.CreatePlayerRunCmdForward(newOnPlayerRunCmdFwd);
					hasNewOnPlayerRunCmdFwd = boss.AddToForward("OnPlayerRunCmd", newOnPlayerRunCmdFwd);

					canSaveFromDanger = abilityDefinition.GetBoolEx("CanSaveFromDanger", false);
					isAerialEscape = abilityDefinition.GetBoolEx("IsAerialEscape", false);
				}
			}

			// We have a new charge mode (maybe, it's not invalid at least) so... Ability changed!
			if (phaseChargeMode != ChargeMode_Invalid)
			{
				chargeMode = phaseChargeMode;
			}

			Format(property, sizeof(property), "Has%sAbility<%d>", type, phase);
			bool newUsable = boss.GetBoolEx(property, true);
			if (usable)
			{
				// if our ability is already usable, it's pretty simple
				usable = newUsable;
			}
			else
			{
				// however, if not ... we need to look at our abilities
				usable = chargeMode != ChargeMode_Invalid && newUsable;
			}
			
			Format(property, sizeof(property), "%sFormatHud<%d>", type, phase);
			if (boss.HasFunction(property))
			{
				this.CreateFormatHudForward();
				boss.AddToForward(property, this.FormatHudFwd);
			}

			Format(property, sizeof(property), "%sChargeTime<%d>", type, phase);
			float chargeTime = boss.GetFloatEx(property, this.ChargeTime);
			this.SetChargeTime(chargeTime, true);

			Format(property, sizeof(property), "%sCooldownTime<%d>", type, phase);
			float chargeCooldownTime = boss.GetFloatEx(property, this.ChargeCooldownTime);
			this.SetChargeCooldownTime(chargeCooldownTime, true);
		} // end for (int i = this.CurrentPhase + 1; i <= newPhase; i++)

		if (wasUsable && (!usable || onChargeForwardsChanged))
		{
			// If we were using a continuous charge, and it's charging, release it
			if (this.ChargeMode == ChargeMode_Continuous && this.AbilityState == AbilityState_Charging)
			{
				this.ChargeReleased();
			}
			// If we were using a normal charge, and it's charging, cancel the charging
			else if (this.ChargeMode == ChargeMode_Normal && this.AbilityState == AbilityState_Charging)
			{
				this.SetChargePercentage(0.0);
				this.AbilityState = AbilityState_Ready;
			}
		}

		// When going from usable from unusable, start cooldown from 0%
		if (usable && !wasUsable)
		{
			this.SetChargeCooldownPercentage(0.0);
		}

		if (onChargeForwardsChanged)
		{
			delete this.OnChargeStartFwd;
			this.OnChargeStartFwd = newOnChargeStartFwd;

			delete this.OnChargeReleasedFwd;
			this.OnChargeReleasedFwd = newOnChargeReleasedFwd;

			delete this.OnPlayerRunCmdFwd;
			this.OnPlayerRunCmdFwd = newOnPlayerRunCmdFwd;
			this.HasOnPlayerRunCmdFwd = hasNewOnPlayerRunCmdFwd;
		}

		this.ChargeMode = chargeMode;
		this.CurrentPhase = newPhase;
		this.IsUsable = usable;
		this.AbilityDefinition = abilityDefinition;
		this.CanSaveFromDanger = canSaveFromDanger;
		// Keep IsActive, if we're active, was an Aerial Escape and turns into an Aerial Escape
		this.IsActive = (isAerialEscape && this.IsActive && this.IsAerialEscape);
		this.IsAerialEscape = isAerialEscape;
	}

	public void Setup(Definition boss)
	{
		this.IsUsable = false;
		this.AbilityDefinition = null;
		this.CanSaveFromDanger = false;
		this.IsAerialEscape = false;
		this.IsActive = false;
		this.CurrentPhase = -1;
		this.Boss = boss;

		this.ChargeTime = 5.0;
		this.ChargeCooldownTime = 10.0;
		this.ChargeStartTime = 0.0;

		this.ChargeMode = ChargeMode_Invalid;
		this.AbilityState = AbilityState_Ready;

		this.HasOnPlayerRunCmdFwd = false;
		this.OnPlayerRunCmdFwd = null;
		this.OnChargeStartFwd = null;
		this.OnChargeReleasedFwd = null;

		this.CreateFormatHudForward();
		AddToForward(this.FormatHudFwd, null, (this.IsSecondary ? DefaultSecondaryFormatChargeHud : DefaultPrimaryFormatChargeHud));
	}

	public void Destroy()
	{
		if (this.IsUsable && this.ChargeMode == ChargeMode_Continuous && this.AbilityState == AbilityState_Charging)
		{
			this.ChargeReleased();
		}
		
		this.IsUsable = false;
		this.AbilityDefinition = null;
		this.CanSaveFromDanger = false;
		this.IsAerialEscape = false;
		this.IsActive = false;
		this.CurrentPhase = -1;
		this.Boss = null;

		this.ChargeTime = 0.0;
		this.ChargeCooldownTime = 0.0;
		this.ChargeStartTime = 0.0;

		this.ChargeMode = ChargeMode_Invalid;
		this.AbilityState = AbilityState_None;

		this.HasOnPlayerRunCmdFwd = false;
		delete this.OnPlayerRunCmdFwd;
		delete this.OnChargeStartFwd;
		delete this.OnChargeReleasedFwd;
		delete this.FormatHudFwd;
	}

	public void FormatAbility(char[] buffer, int bufferSize)
	{
		bool roundUp = (this.ChargeMode == ChargeMode_Continuous && this.AbilityState != AbilityState_OnCooldown);

		Call_StartForward(this.FormatHudFwd);
		Call_PushCell(this.Owner);
		Call_PushStringEx(buffer, bufferSize, SM_PARAM_STRING_UTF8|SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
		Call_PushCell(bufferSize);
		Call_PushCell(this.AbilityState);
		Call_PushCell(PercentageToInt(this.GetChargePercent(), roundUp, true));
		Call_Finish();
	}
}

// We don't need this anywhere else, and shouldn't rely on it outside this file
#undef MaxChargeAbilities