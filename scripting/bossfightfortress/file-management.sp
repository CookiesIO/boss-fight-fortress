#if defined _FILE_MANAGEMENT_INCLUDED
  #endinput
#endif
#define _FILE_MANAGEMENT_INCLUDED

static const char CustomModPathId[] = "CUSTOM_MOD";

void AddBossFilesToDownloadsTable(Definition boss)
{
	int length;
	char file[PLATFORM_MAX_PATH];

	if (boss.GetString("CustomModel", file, sizeof(file)))
	{
		AddModelFilesToDownloadsTable(file);
	}

	boss.GetStringArrayLength("Models", length);
	for (int i = 0; i < length; i++)
	{
		boss.GetArrayString("Models", i, file, sizeof(file));
		AddModelFilesToDownloadsTable(file);
	}

	boss.GetStringArrayLength("Materials", length);
	for (int i = 0; i < length; i++)
	{
		boss.GetArrayString("Materials", i, file, sizeof(file));
		AddMaterialFilesToDownloadsTable(file);
	}

	AddSoundGroupsToDownloadsTable(boss);
}

void PrecacheBossFiles(Definition boss)
{
	int length;
	char file[PLATFORM_MAX_PATH];

	if (boss.GetString("CustomModel", file, sizeof(file)))
	{
		PrecacheModelFiles(file);
	}

	boss.GetStringArrayLength("Models", length);
	for (int i = 0; i < length; i++)
	{
		boss.GetArrayString("Models", i, file, sizeof(file));
		PrecacheModelFiles(file);
	}

	PrecacheSoundGroups(boss);
}

static void AddModelFilesToDownloadsTable(const char[] model)
{
	static const char modelExtensions[][] = {
		".mdl", ".dx80.vtx", ".dx90.vtx", ".sw.vtx", ".vvd", ".phy"
	};

	static const bool requiredExtensions[] = {
		true, true, true, true, true, false
	};

	char modelWithoutExtension[PLATFORM_MAX_PATH];
	StripExtension(modelWithoutExtension, model, ".mdl");

	char file[PLATFORM_MAX_PATH];
	Format(file, sizeof(file), "%s.mdl", modelWithoutExtension);
	if (FileExists(file, true))
	{
		if (FileExists(file, true, CustomModPathId))
		{
			for (int i = 0; i < sizeof(modelExtensions); i++)
			{
				Format(file, sizeof(file), "%s%s", modelWithoutExtension, modelExtensions[i]);
				if (FileExists(file, true, CustomModPathId))
				{
					AddFileToDownloadsTable(file);
				}
				else if (requiredExtensions[i])
				{
					LogError("[BFF] Model file \"%s\" was not found, but is required (remember that custom files must be in the /custom/ folder).", file);
				}
			}
		}
	}
	else
	{
		LogError("[BFF] Model file \"%s\" was not found.", file);
	}
}

static void AddMaterialFilesToDownloadsTable(const char[] material)
{
	static const char extensions[][] = {
		".vtf", ".vmt"
	};

	bool foundMaterial = false;
	char file[PLATFORM_MAX_PATH];
	for (int i = 0; i < sizeof(extensions); i++)
	{
		Format(file, sizeof(file), "%s%s", material, extensions[i]);
		if (FileExists(file, true, CustomModPathId))
		{
			AddFileToDownloadsTable(file);
			foundMaterial = true;
		}
	}

	if (foundMaterial == false)
	{
		LogError("[BFF] Material files (.vmt or .vtf) for \"%s\" was not found, but are required (remember that custom files must be in the /custom/ folder).", material);
	}
}

static void AddSoundGroupsToDownloadsTable(Definition boss)
{
	int soundGroupsLength;
	char soundGroupName[64];
	char file[PLATFORM_MAX_PATH];
	boss.GetStringArrayLength("SoundGroups", soundGroupsLength);
	for (int i = 0; i < soundGroupsLength; i++)
	{
		int length;
		boss.GetArrayString("SoundGroups", i, soundGroupName, sizeof(soundGroupName));
		boss.GetStringArrayLength(soundGroupName, length);
		for (int j = 0; j < length; j++)
		{
			boss.GetArrayString(soundGroupName, j, file, sizeof(file));
			AddSoundFileToDownloadsTable(file);
		}
	}
}

static void AddSoundFileToDownloadsTable(const char[] sound)
{
	char file[PLATFORM_MAX_PATH];
	Format(file, sizeof(file), "sound/%s", sound);
	if (FileExists(file, true))
	{
		if (FileExists(file, true, CustomModPathId))
		{
			AddFileToDownloadsTable(file);
		}
	}
	else
	{
		LogError("[BFF] Sound file \"%s\" was not found.", sound);
	}
}

static void PrecacheModelFiles(const char[] model)
{
	char modelWithoutExtension[PLATFORM_MAX_PATH];
	StripExtension(modelWithoutExtension, model, ".mdl");

	char file[PLATFORM_MAX_PATH];
	Format(file, sizeof(file), "%s.mdl", modelWithoutExtension);
	if (FileExists(file, true))
	{
		PrecacheModel(file);
	}
	else
	{
		LogError("[BFF] Model file \"%s\" was not found.", file);
	}
}

static void PrecacheSoundGroups(Definition boss)
{
	int soundGroupsLength;
	char soundGroupName[64];
	char file[PLATFORM_MAX_PATH];
	boss.GetStringArrayLength("SoundGroups", soundGroupsLength);
	for (int i = 0; i < soundGroupsLength; i++)
	{
		int length;
		boss.GetArrayString("SoundGroups", i, soundGroupName, sizeof(soundGroupName));
		boss.GetStringArrayLength(soundGroupName, length);
		for (int j = 0; j < length; j++)
		{
			boss.GetArrayString(soundGroupName, j, file, sizeof(file));
			PrecacheSoundFile(file);
		}
	}
}

static void PrecacheSoundFile(const char[] sound)
{
	char file[PLATFORM_MAX_PATH];
	Format(file, sizeof(file), "sound/%s", sound);
	if (FileExists(file, true))
	{
		PrecacheSound(sound);
	}
	else
	{
		LogError("[BFF] Sound file \"%s\" was not found.", sound);
	}
}

static void StripExtension(char buffer[PLATFORM_MAX_PATH], const char[] model, const char[] extension)
{
	strcopy(buffer, sizeof(buffer), model);
	int length = strlen(buffer);
	int extensionLength = strlen(extension);

	if (extensionLength < length && StrEqual(buffer[length - extensionLength], extension))
	{
		buffer[length - extensionLength] = '\0';
	}
}