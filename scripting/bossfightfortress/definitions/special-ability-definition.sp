#if defined _SPECIAL_ABILITY_DEFINITION_INCLUDED
  #endinput
#endif
#define _SPECIAL_ABILITY_DEFINITION_INCLUDED

Definition g_hSpecialAbilityDefinition;

void CreateSpecialAbilityDefinition()
{
	DefinitionCreator creator = new DefinitionCreator(BFF_SPECIAL_ABILITY);

	creator.DefineFunction("OnUse");

	creator.ForceTemplate();
	g_hSpecialAbilityDefinition = creator.Finalize();
}