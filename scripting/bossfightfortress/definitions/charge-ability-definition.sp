#if defined _CHARGE_ABILITY_DEFINITION_INCLUDED
  #endinput
#endif
#define _CHARGE_ABILITY_DEFINITION_INCLUDED

Definition g_hNormalChargeAbilityDefinition;
Definition g_hContinuousChargeAbilityDefinition;

void CreateChargeAbilityDefinition()
{
	// Normal charge ability (hold to charge, then release)
	DefinitionCreator creator = new DefinitionCreator(BFF_NORMAL_CHARGE_ABILITY);

	// OnChargeStart and OnPlayerRunCmd is available, if you so require, but is not needed
	// OnPlayerRunCmd is only called while charging!!
	//creator.DefineFunction("OnChargeStart");
	//creator.DefineFunction("OnPlayerRunCmd");
	creator.DefineFunction("OnChargeReleased");
	creator.SetBool("CanSaveFromDanger", false);
	creator.SetBool("IsAerialEscape", false);

	creator.ForceTemplate();
	creator.SetValidator(ChargeAbilityValidator);
	g_hNormalChargeAbilityDefinition = creator.Finalize();

	// Continuous charge ability (hold to use)
	creator = new DefinitionCreator(BFF_CONTINUOUS_CHARGE_ABILITY);

	// OnPlayerRunCmd is available, if you so require, but is not needed
	// OnPlayerRunCmd is only called while charging!!
	//creator.DefineFunction("OnPlayerRunCmd");
	creator.DefineFunction("OnChargeStart");
	creator.DefineFunction("OnChargeReleased");
	creator.SetBool("CanSaveFromDanger", false);
	creator.SetBool("IsAerialEscape", false);

	creator.ForceTemplate();
	creator.SetValidator(ChargeAbilityValidator);
	g_hContinuousChargeAbilityDefinition = creator.Finalize();
}

static bool ChargeAbilityValidator(Definition ability)
{
	if (ability.IsTemplate)
	{
		return true;
	}

	bool canSaveFromDanger = ability.GetBoolEx("CanSaveFromDanger");
	bool isAerialEscape = ability.GetBoolEx("IsAerialEscape");

	if (canSaveFromDanger && isAerialEscape)
	{
		int nameLength = ability.NameLength;
		char[] name = new char[nameLength + 1];
		ability.GetName(name, nameLength);

		LogError("Ability \"%s\" set both \"CanSaveFromDanger\" and \"IsAerialEscape\" to true, but they're mutually exclusive", name);
		return false;
	}
	return true;
}