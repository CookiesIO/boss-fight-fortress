#if defined _PHASE_ABILITY_DEFINITION_INCLUDED
  #endinput
#endif
#define _PHASE_ABILITY_DEFINITION_INCLUDED

Definition g_hPhaseAbilityDefinition;

void CreatePhaseAbilityDefinition()
{
	DefinitionCreator creator = new DefinitionCreator(BFF_PHASE_ABILITY);

	creator.DefineFunction("OnPhaseEnter");
	creator.DefineFunction("OnPhaseExit");

	creator.ForceTemplate();
	g_hPhaseAbilityDefinition = creator.Finalize();
}