#if defined _BOSS_DEFINITION_INCLUDED
  #endinput
#endif
#define _BOSS_DEFINITION_INCLUDED

void CreateBossDefinition()
{
	DefinitionCreator creator = new DefinitionCreator(BFF_BOSS_BASE);

	// Basic information
	creator.DefineString("DisplayName");
	creator.SetFunction("FormatName", DefaultFormatName); // DefaultFormatName is located in bffbaseclient due to limitations of methodmaps
	creator.DefineCell("Class");
	creator.DefineFunction("GetMaxHealth");

	// we don't define these, don't really feel like providing defaults either
	// they'll be available available by setting them anyway though!
	//creator.DefineFunction("OnGiveNamedItem");
	//creator.DefineFunction("EquipBoss");
	//creator.DefineFunction("SoundHook");
	//creator.DefineFunction("OnPlayerRunCmd");

	// Phase stuff, that we can do
	creator.SetCell("PhaseMode", PhaseMode_PercentageHealth);
	creator.SetCell("PhaseReliability", PhaseReliability_NeverSkip);
	creator.DefineFloatArray("PhasesAt");

	creator.DefineStringArray("Models", PLATFORM_MAX_PATH);
	creator.DefineStringArray("Materials", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnSpawn", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnWin", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnLoss", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnDeath", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillAny", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillSpy", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillPyro", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillScout", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillHeavy", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillMedic", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillSniper", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillDemoman", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillSoldier", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillEngineer", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnDestroyBuilding", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnKillingSpree", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnFirstBlood", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsOnBackstabbed", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundsCatchPhrase", PLATFORM_MAX_PATH);
	creator.DefineStringArray("SoundGroups", 64);
	creator.PushStringToArray("SoundGroups", "SoundsOnSpawn");
	creator.PushStringToArray("SoundGroups", "SoundsOnWin");
	creator.PushStringToArray("SoundGroups", "SoundsOnLoss");
	creator.PushStringToArray("SoundGroups", "SoundsOnDeath");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillAny");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillSpy");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillPyro");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillScout");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillHeavy");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillMedic");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillSniper");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillDemoman");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillSoldier");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillEngineer");
	creator.PushStringToArray("SoundGroups", "SoundsOnDestroyBuilding");
	creator.PushStringToArray("SoundGroups", "SoundsOnKillingSpree");
	creator.PushStringToArray("SoundGroups", "SoundsOnFirstBlood");
	creator.PushStringToArray("SoundGroups", "SoundsOnBackstabbed");
	creator.PushStringToArray("SoundGroups", "SoundsCatchPhrase");

	creator.SetBool("MuteVoice", false);

	creator.SetValidator(BossValidator);
	creator.ForceTemplate();
	g_hBossDefinition = creator.Finalize();

	// A generic boss, if you want one definition across multiple boss modes
	// this is the one you wish to extend.
	creator = new DefinitionCreator(BFF_BOSS_GENERIC, g_hBossDefinition);
	creator.ForceTemplate();
	creator.Finalize();
}

static bool BossValidator(Definition boss)
{
	if (boss.IsTemplate)
	{
		return true;
	}

	int nameLength = boss.NameLength;
	char[] name = new char[nameLength + 1];
	boss.GetName(name, nameLength);

	int length;
	boss.GetFloatArrayLength("PhaseAt", length);

	char property[64];
	for (int phase = 0; phase <= length; phase++)
	{
		// validate special abilities
		int abilitiesLength;
		Format(property, sizeof(property), "SpecialAbilities<%d>", phase);
		if (boss.GetDefinitionArrayLength(property, abilitiesLength))
		{
			Definition[] abilities = new Definition[abilitiesLength];
			boss.GetDefinitionArray(property, abilities, abilitiesLength);

			for (int j = 0; j < abilitiesLength; j++)
			{
				// must be descendants of Special Ability
				if (abilities[j].IsDescendantOf(g_hSpecialAbilityDefinition) == false)
				{
					LogError("Boss \"%s\" referenced definition in \"SpecialAbilities<%d>\" that is not a child of \""...BFF_SPECIAL_ABILITY..."\"", name, phase);
					return false;
				}
				else if (abilities[j].IsTemplate)
				{
					LogError("Boss \"%s\" referenced definition in \"SpecialAbilities<%d>\" that is only a template", name, phase);
					return false;
				}
			}
		}

		if (!ValidateChargeAbility(boss, "Primary", phase, name))
		{
			return false;
		}

		if (!ValidateChargeAbility(boss, "Secondary", phase, name))
		{
			return false;
		}

		Format(property, sizeof(property), "PhaseAbilities<%d>", phase);
		if (boss.GetDefinitionArrayLength(property, abilitiesLength))
		{
			Definition[] abilities = new Definition[abilitiesLength];
			boss.GetDefinitionArray(property, abilities, abilitiesLength);

			for (int j = 0; j < abilitiesLength; j++)
			{
				// must be descendants of Phase Ability
				if (abilities[j].IsDescendantOf(g_hPhaseAbilityDefinition) == false)
				{
					LogError("Boss \"%s\" referenced definition in \"PhaseAbilities<%d>\" that is not a child of \""...BFF_PHASE_ABILITY..."\"", name, phase);
					return false;
				}
				else if (abilities[j].IsTemplate)
				{
					LogError("Boss \"%s\" referenced definition in \"PhaseAbilities<%d>\" that is only a template", name, phase);
					return false;
				}
			}
		}
	}

	return true;
}

// Validate the charge abilities, which is uhh, a bit complicated
static bool ValidateChargeAbility(Definition boss, const char[] type, int phase, const char[] name)
{
	char property[64];
	bool onStartSet = false;
	bool onReleasedSet = false;
	bool hasInlineAbility = false;
	bool chargeModeSet = false;
	ChargeMode phaseChargeMode = ChargeMode_Invalid;

	// ChargeMode_Normal needs On{type}Released, but does not allow On{type}Start
	// ChargeMode_Continuous needs both
	Format(property, sizeof(property), "%sChargeMode<%d>", type, phase);
	chargeModeSet = boss.GetCell(property, phaseChargeMode);

	Format(property, sizeof(property), "On%sStart<%d>", type, phase);
	onStartSet = boss.HasFunction(property);

	Format(property, sizeof(property), "On%sReleased<%d>", type, phase);
	onReleasedSet = boss.HasFunction(property);

	bool isAnythingSet = chargeModeSet || onStartSet || onReleasedSet;
	if (isAnythingSet)
	{
		bool correctlySet = false;

		// Check if everything is set up correctly
		if (phaseChargeMode == ChargeMode_Continuous)
		{
			if (onStartSet == false || onReleasedSet == false)
			{
				LogError("Boss \"%s\" set %sChargeMode<%d>=Continuous and either On%sStart<%d> or On%sReleased<%d> was not set", name, type, phase, type, phase, type, phase);
				return false;
			}
			correctlySet = true;
		}
		// Normal doesn't allow for OnStart
		else if (phaseChargeMode == ChargeMode_Normal)
		{
			if (onStartSet == true || onReleasedSet == false)
			{
				LogError("Boss \"%s\" set %sChargeMode<%d>=Normal and either On%sStart<%d> is set or On%sReleased<%d> was not set", name, type, phase, type, phase, type, phase);
				return false;
			}
			correctlySet = true;		
		}

		if (correctlySet == false)
		{
			LogError("Boss \"%s\" set %sChargeMode<%d>, On%sStart<%d> and/or On%sReleased<%d> without setting all of them", name, type, phase, type, phase, type, phase);
			return false;
		}

		hasInlineAbility = true;
	}

	Format(property, sizeof(property), "%sCanSaveFromDanger<%d>", type, phase);
	bool canSaveFromDanger = boss.GetBoolEx(property, false);

	Format(property, sizeof(property), "%sIsAerialEscape<%d>", type, phase);
	bool isAerialEscape = boss.GetBoolEx(property, false);

	if (canSaveFromDanger && isAerialEscape)
	{
		LogError("Boss \"%s\" set both \"%sCanSaveFromDanger<%d>\" and \"%sIsAerialEscape<%d>\" to true, but they're mutually exclusive", name, type, phase, type, phase);
		return false;
	}

	Definition ability;
	Format(property, sizeof(property), "%sAbility<%d>", type, phase);
	if (boss.GetDefinition(property, ability))
	{
		if (hasInlineAbility)
		{
			LogError("Boss \"%s\" already has an ability set, but still set \"%Ability<%d>\"", name, type, phase);
			return false;
		}

		if (ability.IsTemplate)
		{
			LogError("Boss \"%s\" referenced a definition in \"%Ability<%d>\" that is only a template", name, type, phase);
			return false;
		}

		if (ability.IsDescendantOf(g_hNormalChargeAbilityDefinition, false))
		{
			phaseChargeMode = ChargeMode_Normal;
		}
		else if (ability.IsDescendantOf(g_hContinuousChargeAbilityDefinition, false))
		{
			phaseChargeMode = ChargeMode_Continuous;
		}

		if (phaseChargeMode == ChargeMode_Invalid)
		{
			LogError("Boss \"%s\" referenced a definition in \"%sAbility<%d>\" that is not a child of \""...BFF_NORMAL_CHARGE_ABILITY..."\" or \""...BFF_CONTINUOUS_CHARGE_ABILITY..."\"", name, type, phase);
			return false;
		}
	}
	return true;
}