#if defined _BOSSCLIENT_INCLUDED
  #endinput
#endif
#define _BOSSCLIENT_INCLUDED

#if !defined CHARGE_ACCURACY
// Accuracy of charge percentage to be shown in hud, should be one of the following:
// 1, 2, 5, 10, 20, 25, 50
#define CHARGE_ACCURACY 5
#endif

#define CHARGE_ACCURACY_FLOAT (CHARGE_ACCURACY/100.0)

static const float SoundsOnDestroyBuildingChance = 0.2;
static const float SoundsOnKillAnyChance = 0.1;
static const float SoundsOnKillChance = 0.35;
static const float SoundsOnBackstabbedChance = 0.5;

static const float EndangeredTime = 2.0;

#include "bossfightfortress/utils.sp"
#include "bossfightfortress/chargeability.sp"
#include "bossfightfortress/specialability.sp"

// Basic information required for bosses
static bool        g_bIsBoss[MAXPLAYERS+1]; // Faster lookup than querying and validating g_hBoss
static Definition  g_hBoss[MAXPLAYERS+1];
static TFClassType g_eBossClass[MAXPLAYERS+1];
static int         g_iBossMaxHealth[MAXPLAYERS+1];
static float       g_fBossMaxHealthMultiplier[MAXPLAYERS+1];
static bool        g_bBossHadMaxHealthMultiplierSet[MAXPLAYERS+1];
static bool        g_bBossIsEndangered[MAXPLAYERS+1];
static Handle      g_hBossEndangeredExpirationTimer[MAXPLAYERS+1];

// Not actually handles, but whatevs, they might turn into some. They're at least covered by methodmaps.
static ChargeAbility  g_hPrimaryAbility[MAXPLAYERS+1];
static ChargeAbility  g_hSecondaryAbility[MAXPLAYERS+1];
static SpecialAbility g_hSpecialAbility[MAXPLAYERS+1];

// Phase stuff, how the phase reacts and when to enter a new phase
static int              g_iBossPhase[MAXPLAYERS+1];
static PhaseMode        g_ePhaseMode[MAXPLAYERS+1];
static ArrayList        g_hPhasesAt[MAXPLAYERS+1];
static PhaseReliability g_ePhaseReliability[MAXPLAYERS+1];
static ArrayList        g_hPhaseAbilities[MAXPLAYERS+1];

// Hud stuff, using private forwards to avoid constantly looking up in definitions. Performance reasons.
static bool   g_bUpdateHudQueued[MAXPLAYERS+1];
static Handle g_hHudUpdateTimer[MAXPLAYERS+1];
static Handle g_hFormatName[MAXPLAYERS+1];

// Various tracking stuff, fun
static int g_iKillCount[MAXPLAYERS+1];
static int g_iDamageDealt[MAXPLAYERS+1];

static float g_fKillingSpreeEndTime[MAXPLAYERS+1];
static int   g_iKillingSpree[MAXPLAYERS+1];

// Forwards for when functions would just cost too much to do
static bool g_bHasOnPlayerRunCmdFwd[MAXPLAYERS+1];
static Handle g_hOnPlayerRunCmdFwd[MAXPLAYERS+1];

methodmap BffBaseClient
{
	public BffBaseClient(int client, bool userid = false)
	{
		if (userid)
		{
			client = GetClientOfUserId(client);
		}
		return view_as<BffBaseClient>(client);
	}

	/******************************************
	 *
	 *  Misc client/boss properties
	 *
	 ******************************************/

	property int ClientIndex
	{
		public get()
		{
			return view_as<int>(this);
		}
	}

	property BffClient BffClient
	{
		public get()
		{
			return view_as<BffClient>(this);
		}
	}

	property int UserId
	{
		public get()
		{
			return GetClientUserId(this.ClientIndex);
		}
	}

	property bool IsValid
	{
		public get()
		{
			int client = this.ClientIndex;
			return (client > 0 && client <= MaxClients && IsClientInGame(client));
		}
	}

	property bool IsBoss
	{
		public get()
		{
			return g_bIsBoss[this.ClientIndex];
		}
	}

	property Definition Boss
	{
		public get()
		{
			return g_hBoss[this.ClientIndex];
		}
	}

	property bool IsEndangered
	{
		public get()
		{
			return g_bBossIsEndangered[this.ClientIndex];
		}
		public set(bool value)
		{
			g_bBossIsEndangered[this.ClientIndex] = value;
		}
	}

	property Handle EndangeredExpirationTimer
	{
		public get()
		{
			return g_hBossEndangeredExpirationTimer[this.ClientIndex];
		}
		public set(Handle value)
		{
			g_hBossEndangeredExpirationTimer[this.ClientIndex] = value;
		}
	}

	property TFClassType BossClass
	{
		public get()
		{
			return g_eBossClass[this.ClientIndex];
		}
	}

	property TFClassType Class
	{
		public get()
		{
			return TF2_GetPlayerClass(this.ClientIndex);
		}
		public set(TFClassType tfclass)
		{
			TF2_SetPlayerClass(this.ClientIndex, tfclass);
		}
	}

	property TFTeam Team
	{
		public get()
		{
			return TF2_GetClientTeam(this.ClientIndex);
		}
		public set(TFTeam team)
		{
			TF2_ChangeClientTeam(this.ClientIndex, team);
		}
	}

	property bool IsPlaying
	{
		public get()
		{
			TFTeam team = this.Team;
			return (team == TFTeam_Blue || team == TFTeam_Red);
		}
	}

	property int Buttons
	{
		public get()
		{
			return GetClientButtons(this.ClientIndex);
		}
	}

	property int BossMaxHealth
	{
		public get()
		{
			return g_iBossMaxHealth[this.ClientIndex];
		}
		public set(int value)
		{
			if (value < 0)
			{
				value = 0;
			}
			g_iBossMaxHealth[this.ClientIndex] = value;
		}
	}

	property float BossMaxHealthMultiplier
	{
		public get()
		{
			return g_fBossMaxHealthMultiplier[this.ClientIndex];
		}
		public set(float value)
		{
			g_fBossMaxHealthMultiplier[this.ClientIndex] = value;
		}
	}

	property bool HadMaxHealthMultiplierSet
	{
		public get()
		{
			return g_bBossHadMaxHealthMultiplierSet[this.ClientIndex];
		}
		public set(bool value)
		{
			g_bBossHadMaxHealthMultiplierSet[this.ClientIndex] = value;
		}
	}

	property Handle FormatNameFwd
	{
		public get()
		{
			return g_hFormatName[this.ClientIndex];
		}
		public set(Handle value)
		{
			g_hFormatName[this.ClientIndex] = value;
		}
	}

	property Handle HudUpdateTimer
	{
		public get()
		{
			return g_hHudUpdateTimer[this.ClientIndex];
		}
		public set(Handle value)
		{
			g_hHudUpdateTimer[this.ClientIndex] = value;
		}
	}

	property bool UpdateHudQueued
	{
		public get()
		{
			return g_bUpdateHudQueued[this.ClientIndex];
		}
		public set(bool value)
		{
			g_bUpdateHudQueued[this.ClientIndex] = value;
		}
	}

	property bool HasOnPlayerRunCmdFwd
	{
		public get()
		{
			return g_bHasOnPlayerRunCmdFwd[this.ClientIndex];
		}
		public set(bool value)
		{
			g_bHasOnPlayerRunCmdFwd[this.ClientIndex] = value;
		}
	}

	property Handle OnPlayerRunCmdFwd
	{
		public get()
		{
			return g_hOnPlayerRunCmdFwd[this.ClientIndex];
		}
		public set(Handle value)
		{
			g_hOnPlayerRunCmdFwd[this.ClientIndex] = value;
		}
	}

	/******************************************
	 *
	 *  Primary ability properties
	 *
	 ******************************************/

	property ChargeAbility PrimaryAbility
	{
		public get()
		{
			return g_hPrimaryAbility[this.ClientIndex];
		}
		public set(ChargeAbility ability)
		{
			g_hPrimaryAbility[this.ClientIndex] = ability;
		}
	}

	property bool IsPrimaryAbilityUsable
	{
		public get()
		{
			return g_hPrimaryAbility[this.ClientIndex].IsUsable;
		}
	}

	/******************************************
	 *
	 *  Secondary ability properties
	 *
	 ******************************************/

	property ChargeAbility SecondaryAbility
	{
		public get()
		{
			return g_hSecondaryAbility[this.ClientIndex];
		}
		public set(ChargeAbility ability)
		{
			g_hSecondaryAbility[this.ClientIndex] = ability;
		}
	}

	property bool IsSecondaryAbilityUsable
	{
		public get()
		{
			return g_hSecondaryAbility[this.ClientIndex].IsUsable;
		}
	}

	/******************************************
	 *
	 *  Special ability properties
	 *
	 ******************************************/

	property SpecialAbility SpecialAbility
	{
		public get()
		{
			return g_hSpecialAbility[this.ClientIndex];
		}
		public set(SpecialAbility ability)
		{
			g_hSpecialAbility[this.ClientIndex] = ability;
		}
	}

	property bool IsSpecialAbilityUsable
	{
		public get()
		{
			return this.SpecialAbility.IsUsable;
		}
	}

	/******************************************
	 *
	 *  Phase properties
	 *
	 ******************************************/

	property int Phase
	{
		public get()
		{
			return g_iBossPhase[this.ClientIndex];
		}
		public set(int value)
		{
			g_iBossPhase[this.ClientIndex] = value;
		}
	}

	property PhaseMode PhaseMode
	{
		public get()
		{
			return g_ePhaseMode[this.ClientIndex];
		}
		public set(PhaseMode value)
		{
			g_ePhaseMode[this.ClientIndex] = value;
		}
	}

	property ArrayList PhasesAt
	{
		public get()
		{
			return g_hPhasesAt[this.ClientIndex];
		}
		public set(ArrayList value)
		{
			g_hPhasesAt[this.ClientIndex] = value;
		}
	}

	property PhaseReliability PhaseReliability
	{
		public get()
		{
			return g_ePhaseReliability[this.ClientIndex];
		}
		public set(PhaseReliability value)
		{
			g_ePhaseReliability[this.ClientIndex] = value;
		}
	}

	property ArrayList PhaseAbilities
	{
		public get()
		{
			return g_hPhaseAbilities[this.ClientIndex];
		}
		public set(ArrayList value)
		{
			g_hPhaseAbilities[this.ClientIndex] = value;
		}
	}

	/******************************************
	 *
	 *  Tracking properties
	 *
	 ******************************************/

	property int KillCount
	{
		public get()
		{
			return g_iKillCount[this.ClientIndex];
		}
		public set(int value)
		{
			g_iKillCount[this.ClientIndex] = value;
		}
	}

	property int DamageDealt
	{
		public get()
		{
			return g_iDamageDealt[this.ClientIndex];
		}
		public set(int value)
		{
			g_iDamageDealt[this.ClientIndex] = value;
		}
	}

	property int KillingSpree
	{
		public get()
		{
			return g_iKillingSpree[this.ClientIndex];
		}
		public set(int value)
		{
			g_iKillingSpree[this.ClientIndex] = value;
		}
	}

	property float KillingSpreeEndTime
	{
		public get()
		{
			return g_fKillingSpreeEndTime[this.ClientIndex];
		}
		public set(float value)
		{
			g_fKillingSpreeEndTime[this.ClientIndex] = value;
		}
	}

	/******************************************
	 *
	 *  Stuff that needs to be placed up-high, because reasons
	 *
	 ******************************************/

	public bool FindSound(const char[] sound, char file[PLATFORM_MAX_PATH])
	{
		if (this.IsBoss)
		{
			int length = 0;
			this.Boss.GetStringArrayLength(sound, length);
			if (length > 0)
			{
				this.Boss.GetArrayString(sound, GetRandomInt(0, length - 1), file, sizeof(file));
				return true;
			}
		}
		return false;
	}

	public bool PlaySound(const char[] sound, bool isGroup=true, bool emitGlobally=true)
	{
		// In BvsA we'd want globally emitted sounds
		// But in BvsB that'd be a fricking earrape
		static GameMode lastGameMode = null;
		static bool allowGlobalEmit = false;

		if (lastGameMode != g_hCurrentBossGameMode)
		{
			allowGlobalEmit = g_hCurrentBossGameMode.GetBoolEx("AllowGlobalSoundEmit");
			lastGameMode = g_hCurrentBossGameMode;
		}

		// Hard override, but it's neccesary if it's not allowed
		// Allowance in certain situations can be a-okay, if the game mode it self sets this true in the PlayingSound callback
		if (!allowGlobalEmit)
		{
			emitGlobally = false;
		}

		bool hasFile = false;
		char file[PLATFORM_MAX_PATH];

		if (this.IsBoss)
		{
			if (isGroup)
			{
				hasFile = this.FindSound(sound, file);
			}
			else
			{
				strcopy(file, sizeof(file), sound);
				hasFile = true;
			}
		}
		else if (!isGroup)
		{
			strcopy(file, sizeof(file), sound);
			hasFile = true;

			// Not allowed to emit this globally for non-bosses
			emitGlobally = false;
		}

		if (hasFile)
		{
			// The current boss game mode should be allowed to do light modifications here
			// Not allowed to override the sound being played, although that would be hackable and possible...
			// The main concern lies with some sounds playing atop others, such as Death + Lose, or Kill + Win
			// These rules might depend on the rules of the actual game mode, the game mode should then
			// have a way to block sounds that is deemed overlapping in a bad bad way
			if (g_hCurrentBossGameMode.StartFunction("PlayingSound"))
			{
				bool result = true;
				Call_PushCell(this.ClientIndex);
				Call_PushString(sound);
				Call_PushCell(isGroup);
				Call_PushCellRef(emitGlobally);
				Call_Finish(result);
				if (result == false)
				{
					return false;
				}
			}

			SoundHookShouldExpectSound();
			EmitSoundToAll(file, emitGlobally ? SOUND_FROM_PLAYER : this.ClientIndex);
			SoundHookShouldExpectSoundNoMore();
			return true;
		}
		return false;
	}

	/******************************************
	 *
	 *  Update functions
	 *
	 ******************************************/

	// Keep this up here, so it's readily available, and not in the hud section
	// We trigger the timer, so that it's reset, keeping potential hud draws down!
	public void UpdateHud()
	{
		TriggerTimer(this.HudUpdateTimer, true);
		this.UpdateHudQueued = false;
	}

	public void QueueUpdateHud()
	{
		if (this.IsBoss)
		{
			this.UpdateHudQueued = true;
		}
	}

	public bool UpdateSpecialAbility()
	{
		static lastChargePercent[MAXPLAYERS+1];

		if (this.IsSpecialAbilityUsable)
		{
			SpecialAbility ability = this.SpecialAbility;
			if (ability.UpdateCharge())
			{
				int lChargePercent = lastChargePercent[this.ClientIndex];
				int cChargePercent = PercentageToInt(ability.GetChargePercent(), false, true);
				int chargeDifference = cChargePercent - lChargePercent;

				if (chargeDifference != 0)
				{
					lastChargePercent[this.ClientIndex] = cChargePercent;
					return true;
				}
			}
		}
		return false;
	}

	public bool CheckChargePercent(ChargeAbility ability, int &lChargePercent)
	{
		bool roundUp = (ability.ChargeMode == ChargeMode_Continuous && ability.AbilityState != AbilityState_OnCooldown);
		int cChargePercent = PercentageToInt(ability.GetChargePercent(false), roundUp, true);
		int chargeDifference = cChargePercent - lChargePercent;

		if (chargeDifference != 0)
		{
			lChargePercent = cChargePercent;
			return true;
		}
		return false;
	}

	public bool UpdateChargeAbilities()
	{
		static int lastPrimaryChargePercent[MAXPLAYERS+1];
		static int lastSecondaryChargePercent[MAXPLAYERS+1];
		static bool lastPrimaryIsActive[MAXPLAYERS+1];
		static bool lastSecondaryIsActive[MAXPLAYERS+1];
		static int lastButtons[MAXPLAYERS+1];

		bool result = false;
		int buttons = this.Buttons;
		ChargeAbility ability;

		ability = this.PrimaryAbility;
		if (ability.UpdateCharge(buttons, lastButtons[this.ClientIndex]))
		{
			if (ability.IsActive != lastPrimaryIsActive[this.ClientIndex] ||
			    this.CheckChargePercent(ability, lastPrimaryChargePercent[this.ClientIndex]))
			{
				result = true;
				lastPrimaryIsActive[this.ClientIndex] = ability.IsActive;
			}
		}

		ability = this.SecondaryAbility;
		if (ability.UpdateCharge(buttons, lastButtons[this.ClientIndex]))
		{
			if (ability.IsActive != lastSecondaryIsActive[this.ClientIndex] ||
			    this.CheckChargePercent(ability, lastSecondaryChargePercent[this.ClientIndex]))
			{
				result = true;
				lastSecondaryIsActive[this.ClientIndex] = ability.IsActive;
			}
		}
		
		lastButtons[this.ClientIndex] = buttons;
		return result;
	}

	/**
	 *
	 * Phase magic, or torture, or both
	 *
	 **/

	// required for PhaseReliability_NeverSkip in order to never take too much damage
	public bool AdjustDamageForPhases(float &damage)
	{
		if (this.PhaseReliability == PhaseReliability_NeverSkip)
		{
			BffClient client = this.BffClient;
			int health = client.Health;
			int predictedHealth = health - RoundToCeil(damage);

			ArrayList phasesAt = this.PhasesAt;
			int phasesAtLength = phasesAt.Length;

			int nextPhase = this.Phase + 1;
			if (nextPhase < phasesAtLength)
			{
				int phaseAtHealth = this.PhasesAt.Get(nextPhase);
				if (predictedHealth <= phaseAtHealth)
				{
					damage = float(health - phaseAtHealth);
					return true;
				}
			}
		}
		return false;
	}

	public void ExitPhase()
	{
		int length = this.PhaseAbilities.Length;
		for (int i = 0; i < length; i++)
		{
			Definition phaseAbiltiy = this.PhaseAbilities.Get(i);
			phaseAbiltiy.StartFunction("OnPhaseExit");
			Call_PushCell(this.ClientIndex);
			Call_PushCell(phaseAbiltiy);
			Call_Finish();
		}
	}

	// These parameters are horrily named, but we need to differnciate between seeking for the first phase
	// and actually advancing to the next phase.
	public void EnterNextPhase(bool seekingFirstPhase, bool thisIsWantedPhase)
	{
		// Always exit phase prior to entering a new phase! Unless we're seeking for the first phase
		if (!seekingFirstPhase)
		{
			this.ExitPhase();
		}
		int phase = this.Phase + 1;

		int length = 0;
		char property[64];
		Format(property, sizeof(property), "PhaseAbilities<%d>", phase);
		if (this.Boss.GetDefinitionArrayLength(property, length))
		{
			// always clear if we have special abilities set
			this.PhaseAbilities.Clear();

			if (length != 0)
			{
				Definition[] abilities = new Definition[length];
				this.Boss.GetDefinitionArray(property, abilities, length);
				for (int j = 0; j < length; j++)
				{
					Definition phaseAbiltiy = abilities[j];
					this.PhaseAbilities.Push(phaseAbiltiy);

					if (!seekingFirstPhase || thisIsWantedPhase)
					{
						phaseAbiltiy.StartFunction("OnPhaseEnter");
						Call_PushCell(this.ClientIndex);
						Call_PushCell(phaseAbiltiy);
						Call_Finish();
					}
				}
			}
		}

		this.Phase = phase;
	}

	public void UpdatePhases(bool seekingFirstPhase=false)
	{
		BffClient client = this.BffClient;
		int health = client.Health;

		int newPhase = this.Phase;

		ArrayList phasesAt = this.PhasesAt;
		int phasesAtLength = phasesAt.Length;

		for (int i = this.Phase + 1; i < phasesAtLength; i++)
		{
			int phaseAtHealth = this.PhasesAt.Get(i);

			if (health <= phaseAtHealth)
			{
				newPhase = i;
			}
			else
			{
				break;
			}
		}

		if (newPhase != this.Phase)
		{
			this.SpecialAbility.AdvanceToPhase(newPhase);
			this.PrimaryAbility.AdvanceToPhase(newPhase);
			this.SecondaryAbility.AdvanceToPhase(newPhase);

			for (int i = this.Phase + 1; i <= newPhase; i++)
			{
				// if we are seeking the first phase, entering the "next phase" should not exit the previous phase, as there's none
				// if i == newPhase, then it is the wanted phase
				this.EnterNextPhase(seekingFirstPhase, i == newPhase);
			}

			char phaseSound[64];
			Format(phaseSound, sizeof(phaseSound), "SoundsOnPhase<%d>", this.Phase);
			this.PlaySound(phaseSound);
			
			this.UpdateHud();
		}
	}

	public void CalculatePhasesAt()
	{
		int length = 0;
		if (this.Boss.GetFloatArrayLength("PhasesAt", length))
		{
			float[] phaseAt = new float[length];
			this.Boss.GetFloatArray("PhasesAt", phaseAt, length);

			// default phase at phase 0, so push the default phase health (max health)
			this.PhasesAt.Push(this.BossMaxHealth);

			switch (this.PhaseMode)
			{
				case PhaseMode_FixedHealth: // Values in PhasesAt represents exact health figures for a phase
				{
					for (int i = 0; i < length; i++)
					{
						this.PhasesAt.Push(RoundToFloor(phaseAt[i]));
					}
				}
				case PhaseMode_PercentageHealth: // Values in PhasesAt represents percentage of max health for a phase
				{
					int maxHealth = this.BossMaxHealth;
					for (int i = 0; i < length; i++)
					{
						this.PhasesAt.Push(RoundToFloor(maxHealth * phaseAt[i]));
					}
				}
				case PhaseMode_DamageTaken: // Values in PhasesAt represents how much damage should be taken to enter next phase
				{
					int health = this.BossMaxHealth;
					for (int i = 0; i < length; i++)
					{
						health = RoundToFloor(health - phaseAt[i]);
						this.PhasesAt.Push(health);
					}
				}
			}

			this.UpdatePhases(true);
		}
	}

	/******************************************
	 *
	 *  Misc functions
	 *
	 ******************************************/

	public void SetHealthMultiplier(float multiplier)
	{
		if (g_eRoundState == BffRoundState_Preround || (g_eRoundState == BffRoundState_Active && this.HadMaxHealthMultiplierSet == false))
		{
			this.BossMaxHealthMultiplier = multiplier;
			this.HadMaxHealthMultiplierSet = true;

			int maxHealth;

			this.Boss.StartFunction("GetMaxHealth");
			Call_PushFloat(multiplier);
			Call_Finish(maxHealth);

			// Dunno why, but whatevs, better safe than sorry, or something, fishcaeks
			if (maxHealth <= 0)
			{
				maxHealth = 1;
			}

			this.BossMaxHealth = maxHealth;
			SetEntProp(this.ClientIndex, Prop_Send, "m_iHealth", maxHealth);

			this.CalculatePhasesAt();
			this.UpdateHud();
		}
	}

	/******************************************
	 *
	 *  Set/Clear boss functions
	 *
	 ******************************************/

	public void SetBoss(Definition boss)
	{
		if (this.IsBoss)
		{
			ClearBoss(this, boss.IsValidEx(false, g_hBossDefinition));
		}

		if (boss.IsValidEx(false, g_hBossDefinition))
		{
			PrecacheBossFiles(boss);

			int client = this.ClientIndex;
			g_bIsBoss[client] = true;
			g_hBoss[client] = boss;
			g_eBossClass[client] = boss.GetCellEx("Class");

			this.PrimaryAbility = ChargeAbility(this.ClientIndex, false);
			this.PrimaryAbility.Setup(boss);

			this.SecondaryAbility = ChargeAbility(this.ClientIndex, true);
			this.SecondaryAbility.Setup(boss);

			this.SpecialAbility = SpecialAbility(this.ClientIndex);
			this.SpecialAbility.Setup(boss);

			this.Phase = -1;
			this.PhasesAt = new ArrayList();
			this.PhaseAbilities = new ArrayList();

			this.PhaseMode = boss.GetCellEx("PhaseMode");
			this.PhaseReliability = boss.GetCellEx("PhaseReliability");

			this.FormatNameFwd = CreateForward(ET_Ignore, Param_Cell, Param_String, Param_Cell);
			boss.AddToForward("FormatName", this.FormatNameFwd);

			this.OnPlayerRunCmdFwd = CreateForward(ET_Single, Param_Cell, Param_CellByRef, Param_CellByRef, Param_Array, Param_Array, Param_CellByRef);
			this.HasOnPlayerRunCmdFwd = boss.AddToForward("OnPlayerRunCmd", this.OnPlayerRunCmdFwd);

			this.HudUpdateTimer = CreateTimer(5.0, HudUpdateTimer, this.ClientIndex, TIMER_REPEAT);

			// Uhh, yeah!
			if (this.BossMaxHealthMultiplier < 1.0)
			{
				this.BossMaxHealthMultiplier = 1.0;
			}

			this.SetHealthMultiplier(this.BossMaxHealthMultiplier);
			this.HadMaxHealthMultiplierSet = false;

			// We also need some hooks
			SDKHook(client, SDKHook_OnTakeDamageAlive, OnBossTakeDamageAlive);
			SDKHook(client, SDKHook_GetMaxHealth, GetMaxHealth);

			this.UpdateHud();
		}
	}

	public void ClearBoss(bool overriding=false)
	{
		if (this.IsBoss)
		{
			ClearHud(this);

			int client = this.ClientIndex;
			g_bIsBoss[client] = false;
			g_hBoss[client] = null;
			g_eBossClass[client] = TFClass_Unknown;

			this.BossMaxHealth = 0;
			if (overriding == false)
			{
				this.BossMaxHealthMultiplier = 1.0;
			}
			this.HadMaxHealthMultiplierSet = false;

			this.PrimaryAbility.Destroy();
			this.PrimaryAbility = InvalidChargeAbility;

			this.SecondaryAbility.Destroy();
			this.SecondaryAbility = InvalidChargeAbility;

			this.SpecialAbility.Destroy();
			this.SpecialAbility = InvalidSpecialAbility;

			delete this.PhasesAt;
			delete this.PhaseAbilities;
			delete this.FormatNameFwd;
			delete this.HudUpdateTimer;

			// We also better clear these hooks
			SDKUnhook(client, SDKHook_OnTakeDamageAlive, OnBossTakeDamageAlive);
			SDKUnhook(client, SDKHook_GetMaxHealth, GetMaxHealth);
		}
	}

	/******************************************
	 *
	 *  HUD functions
	 *
	 ******************************************/

	public void FormatName(char[] buffer, int bufferSize)
	{
		Call_StartForward(this.FormatNameFwd);
		Call_PushCell(this);
		Call_PushStringEx(buffer, bufferSize, SM_PARAM_STRING_UTF8|SM_PARAM_STRING_COPY, SM_PARAM_COPYBACK);
		Call_PushCell(bufferSize);
		Call_Finish();
	}

	public void DrawHud()
	{
		if (this.IsBoss)
		{
			// Message size of 255-36 taken from SourceMod's ShowHudText
			// This equates to 32 chars for name, and 61 for each ability (excluding nulls-turned-newlines)
			const int messageSize = 255 - 36;
			const int bufferSize = 62;
			const int messageParts = 4;

			int partCounter = 0;
			char message[messageSize];
			char buffers[messageParts][bufferSize];

			ChargeAbility primary = this.PrimaryAbility;
			ChargeAbility secondary = this.SecondaryAbility;

			this.FormatName(buffers[partCounter++], 33);

			if (this.IsSpecialAbilityUsable)
			{
				this.SpecialAbility.FormatAbility(buffers[partCounter++], bufferSize);
			}
			if (primary.IsUsable && primary.IsActive)
			{
				primary.FormatAbility(buffers[partCounter++], bufferSize);
			}
			if (secondary.IsUsable && secondary.IsActive)
			{
				secondary.FormatAbility(buffers[partCounter++], bufferSize);
			}

			ImplodeStrings(buffers, partCounter, "\n", message, messageSize);

			// Set display time to 10 seconds, which is double up on timer time, to reduce/eliminate flicker
			SetHudTextParams(0.02, -1.0, 10.0, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
			ShowSyncHudText(this.ClientIndex, g_hStatusHudSynchronizer, "%s", message);
		}
	}

	public void ClearHud()
	{
		if (this.IsBoss)
		{
			ClearSyncHud(this.ClientIndex, g_hStatusHudSynchronizer);
		}
	}

	/******************************************
	 *
	 *  Uhm ... events?
	 *
	 ******************************************/

	public Action OnPlayerRunCmd(int &buttons, int &impulse, float vel[3], float ang[3], int &weapon)
	{
		Action result = Plugin_Continue;
		Action tempResult = Plugin_Continue;

		if (this.HasOnPlayerRunCmdFwd)
		{
			Call_StartForward(this.OnPlayerRunCmdFwd);
			Call_PushCell(this.ClientIndex);
			Call_PushCellRef(buttons);
			Call_PushCellRef(impulse);
			Call_PushArrayEx(vel, sizeof(vel), SM_PARAM_COPYBACK);
			Call_PushArrayEx(ang, sizeof(ang), SM_PARAM_COPYBACK);
			Call_PushCellRef(weapon);
			Call_Finish(result);
		}

		tempResult = this.PrimaryAbility.OnPlayerRunCmd(buttons, impulse, vel, ang, weapon);
		result = (tempResult <= result ? result : tempResult);

		tempResult = this.SecondaryAbility.OnPlayerRunCmd(buttons, impulse, vel, ang, weapon);
		result = (tempResult <= result ? result : tempResult);

		return result;
	}

	public void RoundActive()
	{
		if (this.IsBoss)
		{
			this.SpecialAbility.AdvanceToPhase(0);
			this.PrimaryAbility.AdvanceToPhase(0);
			this.SecondaryAbility.AdvanceToPhase(0);
			if (this.IsSpecialAbilityUsable)
			{
				this.SpecialAbility.StartSpecialCooldown();
			}
			this.UpdateHud();
			this.PlaySound("SoundsOnSpawn", true, true);
		}
	}

	public void Spawned()
	{
		if (this.IsBoss)
		{
			if (this.Class != this.BossClass)
			{
				this.BffClient.SetTemporaryClass(this.BossClass);
			}

			if (g_eRoundState == BffRoundState_Active)
			{
				this.PlaySound("SoundsOnSpawn");
			}
		}
	}

	public void Died(BffBaseClient opponent, bool feignedDeath)
	{
		if (this.IsBoss)
		{
			this.PlaySound("SoundsOnDeath");
			//this.ClearBoss();
		}
	}

	public void Killed(BffBaseClient opponent, bool feignedDeath)
	{
		if (this.IsBoss)
		{
			bool hasPlayedSound = false;
			char file[PLATFORM_MAX_PATH];

			// Feigned death does not contribute to killing sprees
			if (!feignedDeath)
			{
				this.KillCount += 1;

				if (this.KillCount == 1 && this.FindSound("SoundsOnFirstBlood", file))
				{
					hasPlayedSound = this.PlaySound(file, false);
				}

				float gameTime = GetGameTime();
				if (gameTime < this.KillingSpreeEndTime)
				{
					this.KillingSpree += 1;
					if (this.KillingSpree == 3)
					{
						hasPlayedSound = this.PlaySound("SoundsOnKillingSpree");

						// just to make this more "dynamic", every 4-2 kills can be a killing spree message after the initial one!
						this.KillingSpree = GetRandomInt(-1, 1);
					}
				}
				else
				{
					this.KillingSpree = 1;
				}
				this.KillingSpreeEndTime = gameTime + 5.0;
			}

			// Should only play one of these if we haven't played a killing spree or first blood
			// These may also play during a feigned death
			if (hasPlayedSound == false && GetRandomFloat(0.0, 1.0) < SoundsOnKillChance)
			{
				static const char soundGroups[][] = {
					"",
					"SoundsOnKillScout",
					"SoundsOnKillSniper",
					"SoundsOnKillSoldier",
					"SoundsOnKillDemoman",
					"SoundsOnKillMedic",
					"SoundsOnKillHeavy",
					"SoundsOnKillPyro",
					"SoundsOnKillSpy",
					"SoundsOnKillEngineer",
				};

				// Class based kill-sound vs a boss? no bueno.
				if ((opponent.IsBoss || GetRandomFloat(0.0, 1.0) < SoundsOnKillAnyChance) && this.FindSound("SoundsOnKillAny", file))
				{
					this.PlaySound(file, false);
				}
				// If the random chance above failed, and no class-specific sound is found, try kill any as well
				else if (opponent.IsBoss == false && (this.FindSound(soundGroups[view_as<int>(opponent.Class)], file) || this.FindSound("SoundsOnKillAny", file)))
				{
					this.PlaySound(file, false);
				}
			}
		}
	}

	public void Hurt(int damage, int custom)
	{
		if (this.IsBoss)
		{
			SpecialAbility specialAbility = this.SpecialAbility;
			if (specialAbility.IsUsable && (specialAbility.ChargeMethod & ChargeMethod_Damage) == ChargeMethod_Damage)
			{
				specialAbility.ChargeDamageTaken += damage;
			}

			this.UpdatePhases();

			if (custom == TF_CUSTOM_BACKSTAB && GetRandomFloat(0.0, 1.0) < SoundsOnBackstabbedChance)
			{
				this.PlaySound("SoundsOnBackstabbed");
			}
		}
	}

	public void DestroyedBuilding()
	{
		if (this.IsBoss && GetRandomFloat(0.0, 1.0) < SoundsOnDestroyBuildingChance)
		{
			this.PlaySound("SoundsOnDestroyBuilding");
		}
	}

	public void Regenerated()
	{
		if (this.IsBoss)
		{
			// Comment from old BFF, so keeping the delay!
			// For almost ALL weapons it would've been fine to just call EquipBoss here, but the sapper - nooooo, delay it abit then it's kay
			RequestFrame(DelayedEquipBoss, this.UserId);
		}
	}

	public void Equip()
	{
		if (this.IsBoss)
		{
			char model[PLATFORM_MAX_PATH];
			if (this.Boss.GetString("CustomModel", model, sizeof(model)))
			{
				this.BffClient.SetModel(model);
			}
			if (this.Boss.StartFunction("EquipBoss"))
			{
				Call_PushCell(this.ClientIndex);
				Call_Finish();
			}
		}
	}

	public void Won()
	{
		if (this.IsBoss)
		{
			this.PlaySound("SoundsOnWin");
		}
	}

	public void Lost()
	{
		if (this.IsBoss)
		{
			this.PlaySound("SoundsOnLoss");
		}
	}

	/******************************************
	 *
	 *  Misc
	 *
	 ******************************************/

	// Putting RemoveWearables here in the base plugin, as it's likely to have to change
	// And thusly, it'll be fixed across all plugins!
	public void RemoveAllWearables()
	{
		static const char classnames[][] = 
		{
			"tf_wearable*",
			"tf_powerup_bottle"
		};

		for (int i = 0; i < sizeof(classnames); i++)
		{
			int ent = -1;
			while ((ent = FindEntityByClassname(ent, classnames[i])) != -1)
			{
				if (GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity") == this.ClientIndex)
				{
					TF2_RemoveWearable(this.ClientIndex, ent);
				}
			}
		}
	}
}

/******************************************
 *
 *  Helpers/wrappers for workarounds/stuff
 *
 ******************************************/

// ew..
static void ClearBoss(BffBaseClient client, bool overriding=false)
{
	client.ClearBoss(overriding);
}

static void ClearHud(BffBaseClient client)
{
	client.ClearHud();
}

/******************************************
 *
 *  Hooks, timers and frames
 *
 ******************************************/

// I know what I'm doing passing in the client index, I also know it'll always be valid
// as the timer is killed when the client gets his boss cleared: which happens when he disconnects
// IF HE DOES NOT get his boss cleared then, something is /WRONG/ anyway! So it's safe.
static Action HudUpdateTimer(Handle timer, any clientIndex)
{
	#pragma unused timer // Stupid compiler warnings
	BffBaseClient client = BffBaseClient(clientIndex);
	client.DrawHud();
}

static void DelayedEquipBoss(any userid)
{
	BffBaseClient client = BffBaseClient(userid, true);
	if (client.IsValid)
	{
		client.Equip();
	}
}

// This is also only applied for bosses, so no worries!
static Action GetMaxHealth(int clientIndex, int &maxHealth)
{
	BffBaseClient client = BffBaseClient(clientIndex);
	maxHealth = client.BossMaxHealth;
	return Plugin_Handled;
}

static Action OnBossTakeDamageAlive(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	#pragma unused damagePosition, damageForce
	BffBaseClient client = BffBaseClient(victim);

	// If we're in a trigger hurt, we should check if we're endangered
	// Depending on the configuration and the boss in question, we'll either
	// be teleported or allow the boss to take action themselves
	char classname[32];
	if (GetEdictClassname(attacker, classname, sizeof(classname)) && strcmp(classname, "trigger_hurt", false) == 0)
	{
		bool forceTeleport;
		if (IsTriggerDangerous(attacker, damage, forceTeleport))
		{
			bool shouldTeleport = forceTeleport;
			if (!shouldTeleport)
			{
				ChargeAbility primary = client.PrimaryAbility;
				ChargeAbility secondary = client.SecondaryAbility;
				if ((primary.IsUsable && primary.CanSaveFromDanger) ||
					(secondary.IsUsable && secondary.CanSaveFromDanger))
				{
					client.IsEndangered = true;
					delete client.EndangeredExpirationTimer;
					client.EndangeredExpirationTimer = CreateTimer(EndangeredTime, EndangeredExpirationTimer, client);
				}
				else
				{
					shouldTeleport = true;
				}
			}
			if (shouldTeleport)
			{
				g_hCurrentBossGameMode.StartFunction("TeleportAwayFromDanger");
				Call_PushCell(client);
				Call_Finish();
			}
		}
	}

	// phase reliability damage reduction takes priority
	if (client.PhaseReliability == PhaseReliability_NeverSkip)
	{
		if (client.AdjustDamageForPhases(damage))
		{
			return Plugin_Changed;
		}
	}

	if (damagecustom == TF_CUSTOM_BACKSTAB)
	{
		damagetype |= DMG_CRIT;
		damage = Pow(float(client.BossMaxHealth), 0.6);
		// A decent baseline, I'd say
		if (damage < 500.0)
		{
			damage = 500.0;
		}
		return Plugin_Changed;
	}
	else if ((damagetype & DMG_FALL) == DMG_FALL)
	{
		damage = Pow(float(client.BossMaxHealth), 0.6);
		return Plugin_Changed;
	}
	return Plugin_Continue;
}

static Action EndangeredExpirationTimer(Handle timer, any data)
{
	#pragma unused timer
	BffBaseClient client = BffBaseClient(data);
	client.IsEndangered = false;
	client.EndangeredExpirationTimer = null;
	return Plugin_Stop;
}

// Heavy-duty stuff! Used for charge/special ability.
public Action OnPlayerRunCmd(int clientIndex, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (g_eRoundState >= BffRoundState_Active)
	{
		BffBaseClient client = BffBaseClient(clientIndex);
		if (client.IsBoss)
		{
			client.OnPlayerRunCmd(buttons, impulse, vel, angles, weapon);

			bool shouldUpdateHud = client.UpdateHudQueued;
			shouldUpdateHud = client.UpdateSpecialAbility() || shouldUpdateHud;
			shouldUpdateHud = client.UpdateChargeAbilities() || shouldUpdateHud;

			if (shouldUpdateHud)
			{
				client.UpdateHud();
			}
		}
	}
}